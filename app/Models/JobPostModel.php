<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPostModel extends Model
{
    use HasFactory;
    protected $table = 'job_post';

    public function get_response(){
        return $this->hasOne(User::class,'id','created_by');
    }
    
    public function get_position(){
        return $this->hasOne(PositionModel::class,'id','position_id')->select(['id','name_en','name_kh']);
    }
    
    public function get_location(){
        return $this->hasOne(LocationModel::class,'id','location_id')->select(['id','name_en','name_kh']);
    }
}

