<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contactModel extends Model
{
    use HasFactory;
    protected $table = 'contactus';
    protected $primaryKey = 'contact_id';
    protected $fillable = ['contact_id','contactus_object','created_at','updated_at','status','flag'];
}
