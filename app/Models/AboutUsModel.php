<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutUsModel extends Model
{
    use HasFactory;
    protected $table = 'about_us';
    protected $primaryKey = 'about_id';
    protected $fillable = ['about_id','about_object','created_at','updated_at','status','flag'];
}
