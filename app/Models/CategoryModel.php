<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    use HasFactory;
    protected $table = 'category';

    public function get_response(){
        return $this->hasOne(User::class,'id');
    }
    
    public function get_product(){
        return $this->hasOne(ProductModel::class,'id','product_id')->select(['id','title_en']);
    }
}