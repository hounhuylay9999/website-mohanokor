<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnnualReportDetailModel extends Model
{
    use HasFactory;
    protected $table = 'annual_report_detail';
}