<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PositionApplyModel extends Model
{
    use HasFactory;
    protected $table = 'position_apply';
}