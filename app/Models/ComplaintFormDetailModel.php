<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplaintFormDetailModel extends Model
{
    use HasFactory;
    protected $table = 'complaint_form_detail';
}