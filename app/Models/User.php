<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasRoles;
    protected $fillable = [
        'name',
        'email',
        'profile',
        'gender',
        'email_verified_at',
        'password',
        'status',
        'entry_by',
        'update_by',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function get_roles(){
        return $this->belongsToMany(RoleModel::class,'model_has_roles','model_id','role_id');
    }

}
