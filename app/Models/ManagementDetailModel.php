<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManagementDetailModel extends Model
{
    use HasFactory;
    protected $table = 'management_detail';
}