<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExchangeRateRealModel extends Model
{
    use HasFactory;
    protected $table = 'exchange_rate_real';
    
}