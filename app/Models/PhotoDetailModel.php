<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhotoDetailModel extends Model
{
    use HasFactory;
    protected $table = 'photo_detail';
}