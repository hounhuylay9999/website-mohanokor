<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoreInformationModel extends Model
{
    use HasFactory;
    protected $table = 'more_information';
}