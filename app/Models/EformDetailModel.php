<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EformDetailModel extends Model
{
    use HasFactory;
    protected $table = 'e_form_detail';
}