<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductServiceModel extends Model
{
    use HasFactory;
    protected $table = 'product_service';

    public function get_response(){
        return $this->hasOne(User::class,'id', 'create_by');
    }
    
    public function get_product(){
        return $this->hasOne(ProductModel::class,'id','product_id')->select(['id','title_en']);
    }
    
    public function get_category(){
        return $this->hasOne(CategoryModel::class,'id','category_id')->select(['id','title_en']);
    }
}