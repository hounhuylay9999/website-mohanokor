<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchModel extends Model
{
    use HasFactory;
    protected $table = 'branch';

    public function get_response(){
        return $this->hasOne(User::class,'id');
    }

    public function get_province(){
        return $this->hasOne(ProductModel::class,'id','province_id')->select(['id','title_en']);
    }
}