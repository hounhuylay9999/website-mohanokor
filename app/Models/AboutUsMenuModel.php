<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutUsMenuModel extends Model
{
    use HasFactory;
    protected $table = 'about_menu';
    protected $primaryKey='id';
    
}