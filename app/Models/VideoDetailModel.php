<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoDetailModel extends Model
{
    use HasFactory;
    protected $table = 'video_detail';
}