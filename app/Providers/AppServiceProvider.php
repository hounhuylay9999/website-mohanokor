<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\CompanyModel;
use Auth;
use App\Models\GeneralModel;
use App;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer(['layouts.app'], function($view)
        {
            $company = CompanyModel::where('status',1)->first();
            $view->with(compact('company'));
        });

        view()->composer(['front-end.layouts.app'], function($view)
        {
            if(App::getLocale() == 'en'){
                $general = GeneralModel::where('lang','en')->first();
            }else{
                $general = GeneralModel::where('lang','kh')->first();
            }
            $view->with(compact('general'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(250);
    }
}