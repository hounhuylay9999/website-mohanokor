<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\PermissionModel;
class PermissionController extends Controller
{

    public function index()
    {
        return view('admin.permission.index');
    }

    public function getPermissionList(request $request){
        $length   = $request->get("pageLength");
        $role     = PermissionModel::where('status',1)->orderBy('id','desc')->take($length);
        return DataTables::of($role)
            ->addIndexColumn()
            ->addColumn('action',function ($role){
                $show = ''.$edit = ''.$delete = '';
                    $show = '<a  data-hint="View"  class="btn btn-square btn-sm btn-success hint--left hint--default" href=""><i class="fa fa-eye "></i></a> ';
                    $edit = '<a  data-hint="Modify" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('roles.edit', Crypt::encrypt($role->id)).'"><i class="fa fa-edit "></i></a> ';
                    $delete = '<a  data-hint="Delete"  data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$role->id.')"><i class="fa fa-trash"></i></a> ';
                return $show.$edit.$delete;
            })
            ->addColumn('date',function ($role){
                return $role->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        
    }
}
