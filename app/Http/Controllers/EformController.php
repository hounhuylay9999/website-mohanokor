<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\EformModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class EformController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:careers-list', ['only' => ['index']]);
        $this->middleware('permission:careers-create', ['only' => ['create','store']]);
        $this->middleware('permission:careers-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:careers-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $e_form = EformModel::where('status',1)->first();
        return view('back-end.e_form.index', compact('e_form'));
    }

    public function getEformList(request $request){
        $length   = $request->get("pageLength");
        $user     = EformModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('careers-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('e_form.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('careers-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.e_form.create');
    }

    public function store(request $request){
          $this->validate($request, [
      
        ]);

        DB::beginTransaction();
        try{
            $e_form = new EformModel();
            $e_form->title_en           = $request->title_en;
            $e_form->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $e_form->status            = 1;
            if($request->hasFile('e_form_thumnail')) {
                $file             = $request->e_form_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $e_form->e_form_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $e_form->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $e_form = EformModel::where('id',$id)->first();
        return view('back-end.e_form.modify',compact('e_form'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $e_form =  EformModel::where('id',$id)->first();
            $e_form->title_en           = $request->title_en;
            $e_form->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $e_form->status            = 1;
            if($request->hasFile('e_form_thumnail')) {
                $file             = $request->e_form_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $e_form->e_form_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $e_form->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $e_form =  EformModel::where('id',$id)->first();
            $e_form->status             = 0;
            $e_form->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}