<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HistoryDetailModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class HistoryDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $history_detail = HistoryDetailModel::where('status',1)->first();
        return view('back-end.history_detail.index', compact('history_detail'));
    }

    public function getHistoryDetailList(request $request){
        $length   = $request->get("pageLength");
        $user     = HistoryDetailModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('page-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('history_detail.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('page-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.history_detail.create');
    }

    public function store(request $request){

          $this->validate($request, [
    
        ]);

        DB::beginTransaction();
        try{
            $history_detail = new HistoryDetailModel();
            $history_detail->title_en           = $request->title_en;
            $history_detail->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $history_detail->short_desc_en      = $request->short_desc_en;
            $history_detail->short_desc_kh      = $request->short_desc_kh ? $request->short_desc_kh : $request->short_desc_en;
            $history_detail->status             = 1;
            if($request->hasFile('history_detail_thumnail')) {
                $file             = $request->history_detail_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $history_detail->history_detail_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $history_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $history_detail = HistoryDetailModel::where('id',$id)->first();
        return view('back-end.history_detail.modify',compact('history_detail'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $history_detail =  HistoryDetailModel::where('id',$id)->first();
            $history_detail->title_en           = $request->title_en;
            $history_detail->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $history_detail->short_desc_en      = $request->short_desc_en;
            $history_detail->short_desc_kh      = $request->short_desc_kh ? $request->short_desc_kh : $request->short_desc_en;
            $history_detail->status             = 1;
            if($request->hasFile('history_detail_thumnail')) {
                $file             = $request->history_detail_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $history_detail->history_detail_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $history_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $history_detail =  HistoryDetailModel::where('id',$id)->first();
            $history_detail->status             = 0;
            $history_detail->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}