<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ManagementDetailModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class ManagementDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $management_detail = ManagementDetailModel::where('status',1)->first();
        return view('back-end.management_detail.index', compact('management_detail'));
    }

    public function getManagementDetailList(request $request){
        $length   = $request->get("pageLength");
        $user     = ManagementDetailModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('page-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('management_detail.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('page-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.management_detail.create');
    }

    public function store(request $request){
          $this->validate($request, [
    
        ]);

        DB::beginTransaction();
        try{
            $management_detail = new ManagementDetailModel();
            $management_detail->title_en           = $request->title_en;
            $management_detail->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $management_detail->skill_en           = $request->skill_en;
            $management_detail->skill_kh           = $request->skill_kh ? $request->skill_kh : $request->skill_en;
            $management_detail->short_desc_en      = $request->short_desc_en;
            $management_detail->short_desc_kh      = $request->short_desc_kh ? $request->short_desc_kh : $request->short_desc_en;
            $management_detail->status             = 1;
            if($request->hasFile('management_detail_thumnail')) {
                $file             = $request->management_detail_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $management_detail->management_detail_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }

            $management_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $management_detail = ManagementDetailModel::where('id',$id)->first();
        return view('back-end.management_detail.modify',compact('management_detail'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $management_detail =  ManagementDetailModel::where('id',$id)->first();
            $management_detail->title_en           = $request->title_en;
            $management_detail->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $management_detail->skill_en           = $request->skill_en;
            $management_detail->skill_kh           = $request->skill_kh ? $request->skill_kh : $request->skill_en;
            $management_detail->short_desc_en      = $request->short_desc_en;
            $management_detail->short_desc_kh      = $request->short_desc_kh ? $request->short_desc_kh : $request->short_desc_en;
            $management_detail->status             = 1;
            if($request->hasFile('management_detail_thumnail')) {
                $file             = $request->management_detail_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $management_detail->management_detail_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }

            $management_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $management_detail =  ManagementDetailModel::where('id',$id)->first();
            $management_detail->status             = 0;
            $management_detail->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}