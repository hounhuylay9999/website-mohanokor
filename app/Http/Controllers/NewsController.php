<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $news = NewsModel::where('status',1)->first();
        return view('back-end.news.index', compact('news'));
    }

    public function getNewsList(request $request){
        $length   = $request->get("pageLength");
        $user     = NewsModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('page-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('news.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('page-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.news.create');
    }

    public function store(request $request){

          $this->validate($request, [
      
        ]);

        DB::beginTransaction();
        try{
            $news = new NewsModel();
            $news->title_en           = $request->title_en;
            $news->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $news->status             = 1;
            if($request->hasFile('news_thumnail')) {
                $file             = $request->news_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $news->news_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $news->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $news = NewsModel::where('id',$id)->first();
        return view('back-end.news.modify',compact('news'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $news =  NewsModel::where('id',$id)->first();
            $news->title_en           = $request->title_en;
            $news->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $news->status             = 1;
            if($request->hasFile('news_thumnail')) {
                $file             = $request->news_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $news->news_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $news->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $news =  NewsModel::where('id',$id)->first();
            $news->status             = 0;
            $news->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}