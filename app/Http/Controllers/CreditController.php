<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CreditModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class CreditController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $credit = CreditModel::where('status',1)->first();
        return view('back-end.credit.index', compact('credit'));
    }

    public function getCreditList(request $request){
        $length   = $request->get("pageLength");
        $user     = CreditModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('page-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('credit.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('page-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.credit.create');
    }

    public function store(request $request){
          $this->validate($request, [
      
        ]);

        DB::beginTransaction();
        try{
            $credit = new CreditModel();
            $credit->title_en           = $request->title_en;
            $credit->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $credit->short_title_en     = $request->short_title_en;
            $credit->short_title_kh     = $request->short_title_kh ? $request->short_title_kh : $request->short_title_en;
            $credit->short_desc_en      = $request->short_desc_en;
            $credit->short_desc_kh      = $request->short_desc_kh ? $request->short_desc_kh : $request->short_desc_en;
            $credit->status             = 1;
            if($request->hasFile('credit_thumnail')) {
                $file             = $request->credit_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $credit->credit_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $credit->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $credit = CreditModel::where('id',$id)->first();
        return view('back-end.credit.modify',compact('credit'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $credit =  CreditModel::where('id',$id)->first();
            $credit->title_en           = $request->title_en;
            $credit->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $credit->short_title_en     = $request->short_title_en;
            $credit->short_title_kh     = $request->short_title_kh ? $request->short_title_kh : $request->short_title_en;
            $credit->short_desc_en      = $request->short_desc_en;
            $credit->short_desc_kh      = $request->short_desc_kh ? $request->short_desc_kh : $request->short_desc_en;
            $credit->status             = 1;
            if($request->hasFile('credit_thumnail')) {
                $file             = $request->credit_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $credit->credit_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $credit->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $credit =  CreditModel::where('id',$id)->first();
            $credit->status             = 0;
            $credit->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}