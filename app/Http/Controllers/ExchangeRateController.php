<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\ExchangeRateModel;
use App\Models\ExchangeRateRealModel;
use DB;
use Auth;
use Carbon\Carbon;
use User;
class ExchangeRateController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:home-list', ['only' => ['index']]);
        $this->middleware('permission:home-create', ['only' => ['create','store']]);
        $this->middleware('permission:home-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:home-delete', ['only' => ['destroy']]);
    }

    public function index()
    {   
        $exchange_rate = ExchangeRateRealModel::where('status',1)->first();
        return view('back-end.exchange_rate.index', compact('exchange_rate'));
    }

    public function getExchangeRateList(request $request){
        $length   = $request->get("pageLength");
        $user     = ExchangeRateRealModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return datatables()::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('home-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('exchange_rate.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('home-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.exchange_rate.create');
    }

    public function store(request $request){
          $this->validate($request, [
                
        ]);

        DB::beginTransaction();
        try{
            
            $exchange_rate_history =  ExchangeRateRealModel::where('title_en',$request->title_en)->first();
            if($exchange_rate_history !=NULL){
                $exchange_rate1 = new ExchangeRateModel();
                $exchange_rate1->title_en           = $exchange_rate_history->title_en;
                $exchange_rate1->title_kh           = $exchange_rate_history->title_kh ? $exchange_rate_history->title_kh : $exchange_rate_history->title_en;
                $exchange_rate1->buy                = $exchange_rate_history->buy;
                $exchange_rate1->sell               = $exchange_rate_history->sell;
                $exchange_rate1->date_post          = $exchange_rate_history->date_post;
                $exchange_rate1->status             = 1;
                $exchange_rate1->created_by         = Auth::user()->id;
                $exchange_rate1->save();
            }
            if($exchange_rate_history !=NULL){
                $exchange_rate_remove =  ExchangeRateRealModel::where('title_en',$request->title_en)->first();
                $exchange_rate_remove->delete();
                DB::commit();
            }
            $exchange_rate = new ExchangeRateRealModel();
            $exchange_rate->title_en           = $request->title_en;
            $exchange_rate->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $exchange_rate->buy                = $request->buy;
            $exchange_rate->sell               = $request->sell;
            $exchange_rate->date_post          = $request->date_post;
            $exchange_rate->status             = 1;
            $exchange_rate->created_by         = Auth::user()->id;
            $exchange_rate->save();
            DB::commit();    

        }catch(\Exception $e){
            DB::rollback();
             return back()->with('warning','Something Went Wrong!');
        }
         return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $exchange_rate = ExchangeRateRealModel::where('id',$id)->first();
        return view('back-end.exchange_rate.modify',compact('exchange_rate'));
    }

    public function update(request $request){

        $this->validate($request, [
        ]);

        DB::beginTransaction();
        try{
            $exchange_rate_history =  ExchangeRateRealModel::where('title_en',$request->title_en)->first();
            if($exchange_rate_history !=NULL){
                $exchange_rate1 = new ExchangeRateModel();
                $exchange_rate1->title_en           = $exchange_rate_history->title_en;
                $exchange_rate1->title_kh           = $exchange_rate_history->title_kh ? $exchange_rate_history->title_kh : $exchange_rate_history->title_en;
                $exchange_rate1->buy                = $exchange_rate_history->buy;
                $exchange_rate1->sell               = $exchange_rate_history->sell;
                $exchange_rate1->date_post          = $exchange_rate_history->date_post;
                $exchange_rate1->status             = 1;
                $exchange_rate1->created_by         = Auth::user()->id;
                $exchange_rate1->save();
            }
            if($exchange_rate_history !=NULL){
                $exchange_rate_remove =  ExchangeRateRealModel::where('title_en',$request->title_en)->first();
                $exchange_rate_remove->delete();
                DB::commit();
            }
            $exchange_rate = new ExchangeRateRealModel();
            $exchange_rate->title_en           = $request->title_en;
            $exchange_rate->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $exchange_rate->buy                = $request->buy;
            $exchange_rate->sell               = $request->sell;
            $exchange_rate->date_post          = $request->date_post;
            $exchange_rate->status             = 1;
            $exchange_rate->created_by         = Auth::user()->id;
            $exchange_rate->save();
            DB::commit();  

        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $exchange_rate =  ExchangeRateRealModel::where('id',$id)->first();
            $exchange_rate->status             = 0;
            $exchange_rate->save();

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}