<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PositionModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class PositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:careers-list', ['only' => ['index']]);
        $this->middleware('permission:careers-create', ['only' => ['create','store']]);
        $this->middleware('permission:careers-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:careers-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $position = PositionModel::where('status',1)->first();
        return view('back-end.position.index', compact('position'));
    }

    public function getPositionList(request $request){
        $length   = $request->get("pageLength");
        $user     = PositionModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('careers-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('position.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('careers-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.position.create');
    }

    public function store(request $request){
          $this->validate($request, [
      
        ]);

        DB::beginTransaction();
        try{
            $position = new PositionModel();
            $position->name_en           = $request->name_en;
            $position->name_kh           = $request->name_kh ? $request->name_kh : $request->name_en;
            $position->status            = 1;
            $position->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $position = PositionModel::where('id',$id)->first();
        return view('back-end.position.modify',compact('position'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $position =  PositionModel::where('id',$id)->first();
            $position->name_en           = $request->name_en;
            $position->name_kh           = $request->name_kh ? $request->name_kh : $request->name_en;
            $position->status            = 1;
            $position->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $position =  PositionModel::where('id',$id)->first();
            $position->status             = 0;
            $position->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}