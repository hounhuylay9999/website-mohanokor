<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App;
use Auth;
use Image;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use App\Models\AboutUsModel;
use File;
class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        return redirect()->route('about.create');
    }

    public function listAboutResource(){
        $about_list = AboutUsModel::where('flag',1)->get();

        $Data = array();
        foreach($about_list as $row){

            $about_object = json_decode($row->about_object);

            $d = [
                "about_id"                  => $row->about_id,
                "about_title_kh"            => $about_object->about_title_kh, 
                "company_overview_kh"       => $about_object->company_overview_kh,
                "company_overview_eng"      => $about_object->company_overview_eng,
                "ourvission_kh"             => $about_object->ourvission_kh,
                "ourvission_eng"            => $about_object->ourvission_eng,
                "ourmission_kh"             => $about_object->ourmission_kh,
                "ourmission_eng"            => $about_object->ourmission_eng,
                "core_value_title_kh"       => $about_object->core_value_title_kh,
                "core_value_description_kh" => $about_object->core_value_description_kh,
                "core_value_title_en"       => @$about_object->core_value_title_en,
                "core_value_description_en" => @$about_object->core_value_description_en,
                'about_cover_image'         => $about_object->about_cover_image,
                'our_mission_image'         => $about_object->about_cover_image,
                "created_at"                => $row->created_at,
                "updated_at"                => $row->updated_at,
                "status"                    => $row->status,
                "flag"                      => $row->flag,

            ];
            array_push($Data,$d);
        }

        return DataTables::of($Data)
        ->addIndexColumn()
        ->addColumn('action',function ($Data){
            $edit = ''.$delete = '';
            if(auth::user()->can('page-edit')):
            $edit   = '<a data-hint="Modify" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('about.edit',$Data['about_id']).'"><i class="fa fa-edit "></i></a> ';
            endif;
            if(auth::user()->can('page-edit')):
            $delete = '<a data-hint="Delete"  data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$Data['about_id'].')"><i class="fa fa-trash"></i></a> ';
            endif;
            return $edit.$delete;
        })
        ->addColumn('date',function ($Data){
            return $Data['created_at']->format('F d, Y');
        })
        ->rawColumns(['action','date'])
        ->make(true);
    }

    public function create()
    {
        $about_list = AboutUsModel::where('flag',1)->first();
        return view('back-end.about.create',compact('about_list'));
    }

    public function store(Request $request)
    {
        $is_about_page_exists = AboutUsModel::where('flag',1)->count();
        if($is_about_page_exists>0){
            $this->validate($request, [

                'about_title_kh'                => 'required',
                'about_title_en'                => 'required',
                'company_overview_kh'           => 'required',
                'company_overview_eng'          => 'required',
                'ourvission_kh'                 => 'required',
                'ourvission_eng'                => 'required',
                'ourmission_kh'                 => 'required',
                'ourmission_eng'                => 'required',
                'core_value_title_kh'           => 'required',
                'core_value_description_kh'     => 'required',
                'core_value_title_en'           => 'required',
                'core_value_description_en'     => 'required',               
                                      
            ]);
        } else {
            
            $this->validate($request, [
                'about_title_kh'                => 'required',
                'about_title_en'                => 'required',
                'company_overview_kh'           => 'required',
                'company_overview_eng'          => 'required',
                'ourvission_kh'                 => 'required',
                'ourvission_eng'                => 'required',
                'ourmission_kh'                 => 'required',
                'ourmission_eng'                => 'required',
                'core_value_title_kh'           => 'required',
                'core_value_description_kh'     => 'required',
                'core_value_title_en'           => 'required',
                'core_value_description_en'     => 'required',
                'company_images'                => 'required',
                'our_mission_images'            => 'required',                    
            ]);
            
           
        }
         
        DB::beginTransaction();
        try {
            $destinationPath = public_path('/upload/thumbnail/');
            $renameFile_company = null;
            $renameFile_ourmission = null;
            $company_images = $request->file('company_images');
            if($company_images!=null){
                $fileName_company = $company_images->getClientOriginalName();
                $renameFile_company = time() . '_' . date('d-M-Y') . '_' . $fileName_company;
                $img1 = Image::make($company_images->getRealPath()); // Get real path when user upload
                $company_images->move(public_path('/upload/thumbnail/'), $renameFile_company);
            }

            $our_mission_images  = $request->file('our_mission_images');
            if($our_mission_images!=null){  
                            
                $fileName_ourmission = $our_mission_images->getClientOriginalName();
                $renameFile_ourmission = time() . '_' . date('d-M-Y') . '_' . $fileName_ourmission;
                $img = Image::make($our_mission_images->getRealPath()); // Get real path when user upload
                $our_mission_images->move(public_path('/upload/thumbnail/'), $renameFile_ourmission);
            }

            $is_about_page_exists = AboutUsModel::where('flag',1)->count();
            if($is_about_page_exists>0){
                $id = AboutUsModel::where('flag',1)->first();
                $aboutObject = json_decode($id->about_object);
                if($renameFile_company==null){$renameFile_company = $aboutObject->company_overview_images;}
                else{
                    if (File::exists((public_path('/upload/thumbnail/'.@$aboutObject->company_overview_images)))) {
                       File::delete(public_path('/upload/thumbnail/'.@$aboutObject->company_overview_images));
                    }
                }
                if($renameFile_ourmission==null){$renameFile_ourmission = $aboutObject->our_mission_image;}
                else{
                    if (File::exists((public_path('/upload/thumbnail/'.@$aboutObject->our_mission_image)))) {
                        File::delete(public_path('/upload/thumbnail/'.@$aboutObject->our_mission_image));
                    }
                }
                $Core_value_object = array(
                    'core_value_title_kh'           => \json_encode($request->core_value_title_kh),
                    'core_value_description_kh'     => \json_encode($request->core_value_description_kh),
                    'core_value_title_en'           => \json_encode($request->core_value_title_en),
                    'core_value_description_en'     => \json_encode($request->core_value_description_en),
                );
                $About_object = array(
                    'about_title_kh'                => $request->about_title_kh,
                    'about_title_en'                => $request->about_title_en,
                    'company_overview_kh'           => $request->company_overview_kh,
                    'company_overview_eng'          => $request->company_overview_eng,
                    'ourvission_kh'                 => $request->ourvission_kh,
                    'ourvission_eng'                => $request->ourvission_eng,
                    'ourmission_kh'                 => $request->ourmission_kh,
                    'ourmission_eng'                => $request->ourmission_eng,
                    'core_value'                    => $Core_value_object,          
                    'company_overview_images'       => $renameFile_company,
                    'our_mission_image'             => $renameFile_ourmission,                    
                );
                
                $aboutus = AboutUsModel::findOrFail($id->about_id);
                $aboutus->update_by     = Auth()->user()->id; 
                $aboutus->updated_at     = Carbon::now();
                $aboutus->about_object   = \json_encode($About_object);               
                $aboutus->save();
               
                
            }else{
                 $Core_value_object = array(
                    'core_value_title_kh'           => \json_encode($request->core_value_title_kh),
                    'core_value_description_kh'     => \json_encode($request->core_value_description_kh),
                    'core_value_title_en'           => \json_encode($request->core_value_title_en),
                    'core_value_description_en'     => \json_encode($request->core_value_description_en),
                );
                $About_object = array(
                    'about_title_kh'                => $request->about_title_kh,
                    'about_title_en'                => $request->about_title_en,
                    'company_overview_kh'           => $request->company_overview_kh,
                    'company_overview_eng'          => $request->company_overview_eng,

                    'ourvission_kh'                 => $request->ourvission_kh,
                    'ourvission_eng'                => $request->ourvission_eng,
                    'ourmission_kh'                 => $request->ourmission_kh,
                    'ourmission_eng'                => $request->ourmission_eng,
                    'core_value'                    => $Core_value_object,
                    'company_overview_images'       => $renameFile_company,
                    'our_mission_image'             => $renameFile_ourmission,           
                );

                $AboutUs = AboutUsModel::create(
                [
                    "about_object"       => json_encode($About_object),
                    "created_at"         => Carbon::now(),           
                    "status"             => 1,
                ]); 

            }
            DB::commit();
            return redirect()->route('about.create')
            ->with('success','About us page create success!');
            
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        return redirect()->route('about.create');
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        AboutUsModel::where('about_id',$id)->update(['flag'=>0,'update_by'=>auth::user()->id]);
        return redirect()->route('about.index')->with('success','About us page deleted success!');
    }
}