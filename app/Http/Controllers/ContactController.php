<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\contactModel;
use Yajra\DataTables\DataTables;
use Spatie\Permission\Models\Role;
use App;
use Image;
use Auth;
use DB;
use File;
use Carbon\Carbon;
use App\Models\PermissionMenuModel;
use App\Models\PermissionSubMenuModel;
class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        return redirect()->route('contact.create');
    }

    public function create()
    {
        $contact_list = contactModel::where('flag',1)->first();
        return view('back-end.contact.create',compact('contact_list'));
    }

    public function listContactResource(){
        $contact_list = contactModel::where('flag',1)->get();
        $Data = array();
        foreach($contact_list as $row){
            $contactus_object = json_decode($row->contactus_object);
            $d = [
                "contact_id"              => $row->contact_id,
                "business_name_khmer"     => $contactus_object->business_name_khmer,
                "business_name_english"   => $contactus_object->business_name_english,
                "business_type_kh"        => $contactus_object->business_type_kh,
                "business_type_en"        => $contactus_object->business_type_en,
                "physical_address_kh"     => $contactus_object->physical_address_kh,
                "physical_address_en"     => $contactus_object->physical_address_en,
                "email_address"           => $contactus_object->email_address,
                "google_map"              => $contactus_object->google_map,
                "company_definition_kh"    => $contactus_object->company_definition_kh,
                "company_definition_en"    => $contactus_object->company_definition_en,  
                "contact_conver_image"    => $contactus_object->contact_conver_image,                 
                "created_at"              => $row->created_at,
                "updated_at"              => $row->updated_at,
                "status"                  => $row->status,
                "flag"                    => $row->flag,
            ];
            array_push($Data,$d);
        }

        return DataTables::of($Data)
        ->addIndexColumn()
        ->addColumn('action',function ($Data){
            $edit = ''.$delete = '';
            if(auth::user()->can('page-edit')):
            $edit   = '<a data-hint="Modify" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('contact.create').'"><i class="fa fa-edit "></i></a> ';
            endif;
            if(auth::user()->can('page-delete')):
             $delete = '<a data-hint="Delete"  data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$Data['contact_id'].')"><i class="fa fa-trash"></i></a> ';
            endif;
            return $edit.$delete;
        })
        ->addColumn('date',function ($Data){
            return $Data['created_at']->format('F d, Y');
        })
        ->rawColumns(['action','date'])
        ->make(true);
    }

    public function store(Request $request)
    {
        $is_countact_exists = contactModel::where('flag',1)->count();
        if($is_countact_exists>0){

            $this->validate($request, [
                'business_name_khmer'         => 'required',
                'business_name_english'       => 'required',
                'business_type_kh'            => 'required',
                'business_type_en'            => 'required',
                'physical_address_kh'         => 'required',
                'physical_address_en'         => 'required',
                'email_address'               => 'required',
                'facebook_page'               => 'required',
                'google_map'                  => 'required',
                'company_definition_kh'       => 'required',
                'company_definition_en'       => 'required',                      
            ]);

        }else{
            $this->validate($request, [
                'business_name_khmer'         => 'required',
                'business_name_english'       => 'required',
                'business_type_kh'            => 'required',
                'business_type_en'            => 'required',
                'physical_address_kh'         => 'required',
                'physical_address_en'         => 'required',
                'email_address'               => 'required',
                'facebook_page'               => 'required',
                'google_map'                  => 'required',
                'company_definition_kh'       => 'required',
                'company_definition_en'       => 'required',
                'conver_images'               => 'required',
                'telephone_no'                => 'required',      
            ]);
        }
        DB::beginTransaction();
        try {
            $destinationPath = public_path('/upload/thumbnail/');
            $conver_images  = $request->file('conver_images');
            $renameFile = null;
            if($conver_images!=null){

                $fileName = $conver_images->getClientOriginalName();
                $renameFile = time() . '_' . date('d-M-Y') . '_' . $fileName;
                $img = Image::make($conver_images->getRealPath()); // Get real path when user upload
                $conver_images->move(public_path('/upload/'), $renameFile);
            }
            if($is_countact_exists>0){
                $contactus_update = contactModel::where('flag',1)->first();
                $contactusObject  = json_decode($contactus_update->contactus_object);
                if($renameFile==null){$renameFile = $contactusObject->contact_conver_image;}
                else{
                    if (File::exists((public_path('/upload/'.@$contactusObject->contact_conver_image)))) {
                       File::delete(public_path('/upload/'.@$contactusObject->contact_conver_image));
                    }
                }
                $contact_object = array(
                    "business_name_khmer"     => $request->business_name_khmer,
                    "business_name_english"   => $request->business_name_english,
                    "business_type_kh"        => $request->business_type_kh,
                    "business_type_en"        => $request->business_type_en,
                    "physical_address_kh"     => $request->physical_address_kh,
                    "physical_address_en"     => $request->physical_address_en,
                    "email_address"           => $request->email_address,
                    "facebook_page"           => $request->facebook_page,
                    "google_map"              => $request->google_map,
                    "company_definition_kh"   => $request->company_definition_kh,
                    "company_definition_en"   => $request->company_definition_en,  
                    "contact_conver_image"    => $renameFile,
                    "telephone_no"            => $request->telephone_no, 
                );

                $contactusObjects = contactModel::findOrFail($contactus_update->contact_id);
                $contactusObjects->update_by     = Auth()->user()->id; 
                $contactusObjects->updated_at     = Carbon::now();
                $contactusObjects->contactus_object   = \json_encode($contact_object);               
                $contactusObjects->save();

            }else{
                $contact_object = array(
                    "business_name_khmer"     => $request->business_name_khmer,
                    "business_name_english"   => $request->business_name_english,
                    "business_type_kh"        => $request->business_type_kh,
                    "business_type_en"        => $request->business_type_en,
                    "physical_address_kh"     => $request->physical_address_kh,
                    "physical_address_en"     => $request->physical_address_en,
                    "email_address"           => $request->email_address,
                    "facebook_page"           => $request->facebook_page,
                    "google_map"              => $request->google_map,
                    "company_definition_kh"   => $request->company_definition_kh,
                    "company_definition_en"   => $request->company_definition_en,  
                    "contact_conver_image"    => $renameFile,
                    "telephone_no"            => $request->telephone_no,
                );
                
                $contactus = contactModel::create(
                [
                    "contactus_object"   => json_encode($contact_object),
                    "created_at"         => Carbon::now(),           
                    "status"             => 1,
                ]); 

            }
            DB::commit();
            return redirect()->route('contact.create')
            ->with('success','page contact us create success!');
            
        } catch (\Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        
    }
}