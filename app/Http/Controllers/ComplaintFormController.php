<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\ComplaintFormModel;
use App\Models\User;
use DB;
use Auth;
use Carbon\Carbon;
use Image;
class ComplaintFormController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('permission:contact_us-list', ['only' => ['index']]);
        $this->middleware('permission:contact_us-create', ['only' => ['create','store']]);
        $this->middleware('permission:contact_us-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:contact_us-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    { 
        return view('back-end.complaint_form.index');
    }

    public function getComplaintFormList(request $request){
        $length   = $request->get("pageLength");
        $user  = ComplaintFormModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);

        return datatables()::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('contact_us-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('complaint_form.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('contact_us-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
            
    }
    
    public function create()
    {
        $complaint_form = ComplaintFormModel::where('status',1)->get();
        return view('back-end.complaint_form.create', compact('complaint_form'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
                
        ]);

        DB::beginTransaction();
        try {
            $complaint_form = new ComplaintFormModel();
            $complaint_form->title_en                     = $request->title_en;
            $complaint_form->title_kh                     = $request->title_kh;
            $complaint_form->sort_text_en                 = $request->sort_text_en;
            $complaint_form->sort_text_kh                 = $request->sort_text_kh;
            $complaint_form->full_text_en                 = $request->full_text_en;
            $complaint_form->full_text_kh                 = $request->full_text_kh;
            $complaint_form->status                       = 1;
            $complaint_form->created_by                    = Auth::user()->id;
            
            if($request->hasFile('complaint_form_thumnail')) {
                $file             = $request->complaint_form_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $complaint_form->complaint_form_thumnail       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $complaint_form->save();
            DB::commit();
            }catch(\Exception $e){
                DB::rollback();
                return back()->with('warning','Something Went Wrong!');
            }
            return back()->with('success',trans('message.save_successfully'));

    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $complaint_form = ComplaintFormModel::where('id', $id)->first();
        return view('back-end.complaint_form.modify',compact('complaint_form'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
        
        ]);
       
        DB::beginTransaction();
        try {
            $complaint_form =  ComplaintFormModel::where('id',$id)->first();
            $complaint_form->title_en                     = $request->title_en;
            $complaint_form->title_kh                     = $request->title_kh;
            $complaint_form->sort_text_en                 = $request->sort_text_en;
            $complaint_form->sort_text_kh                 = $request->sort_text_kh;
            $complaint_form->full_text_en                 = $request->full_text_en;
            $complaint_form->full_text_kh                 = $request->full_text_kh;
            $complaint_form->status                       = 1;
            $complaint_form->created_by                    = Auth::user()->id;
            
            if($request->hasFile('complaint_form_thumnail')) {
                $file             = $request->complaint_form_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $complaint_form->complaint_form_thumnail       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $complaint_form->save();
            
            
            DB::commit();
            }catch(\Exception $e){
                DB::rollback();
                return back()->with('warning','Something Went Wrong!');
                
            }
            return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $complaint_form =  ComplaintFormModel::where('id',$id)->first();
            $complaint_form->status           = 0;
            $complaint_form->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }

}