<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BranchesNetworkModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class BranchesNetworkController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:contact_us-list', ['only' => ['index']]);
        $this->middleware('permission:contact_us-create', ['only' => ['create','store']]);
        $this->middleware('permission:contact_us-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:contact_us-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $branches_network = BranchesNetworkModel::where('status',1)->first();
        return view('back-end.branches_network.index', compact('branches_network'));
    }

    public function getBranchesNetworkList(request $request){
        $length   = $request->get("pageLength");
        $user     = BranchesNetworkModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('contact_us-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('branches_network.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('contact_us-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.branches_network.create');
    }

    public function store(request $request){
          $this->validate($request, [
      
        ]);
        DB::beginTransaction();
        try{
            $branches_network = new BranchesNetworkModel();
            $branches_network->title_en           = $request->title_en;
            $branches_network->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $branches_network->status             = 1;
            $branches_network->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $branches_network = BranchesNetworkModel::where('id',$id)->first();
        return view('back-end.branches_network.modify',compact('branches_network'));
    }

    public function update(request $request,$id){
        $this->validate($request, [
        ]);

        DB::beginTransaction();
        try{
            $branches_network =  BranchesNetworkModel::where('id',$id)->first();
            $branches_network->title_en           = $request->title_en;
            $branches_network->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $branches_network->status             = 1;
            $branches_network->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $branches_network =  BranchesNetworkModel::where('id',$id)->first();
            $branches_network->status             = 0;
            $branches_network->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}