<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\RoleModel;
use Spatie\Permission\Models\Role;
use Hash;
use Carbon\Carbon;
use DB;
use App\Models\PermissionMenuModel;
use App\Models\PermissionSubMenuModel;
class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:role-list', ['only' => ['index']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        return view('admin.role.index');
    }

    public function getRoleList(request $request){
        $length   = $request->get("pageLength");
        $role     = RoleModel::where('status',1)->orderBy('id','desc')->take($length);
        return DataTables::of($role)
            ->addIndexColumn()
            ->addColumn('action',function ($role){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('role-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('roles.edit', Crypt::encrypt($role->id)).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('role-delete')):
                    $delete = '<a  data-hint="'.trans('message.delete').'"  data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$role->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $show.$edit.$delete;
            })
            ->addColumn('date',function ($role){
                return $role->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {
        $permission = PermissionMenuModel::with('getPermissionByGroup')
            ->where('status',1)
            ->get();
        $sub_group = PermissionSubMenuModel::where('status',1)->get();
        return view('admin.role.create',compact('permission','sub_group'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'roles'       => 'required|unique:roles,name',
            'slug'        => 'required|unique:roles,slug',
            'permission'  => 'required',
        ]);
        $role = Role::create(['name' => $request->input('roles'),'slug' => $request->input('roles')]);
        $role->syncPermissions($request->input('permission'));
        
        return redirect()->route('roles.index')
            ->with('success',trans('message.save_successfully'));
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $id              = Crypt::decrypt($id);
        $role            = Role::find($id);
        $permission = PermissionMenuModel::with('getPermissionByGroup')->where('status',1)->get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();

        $sub_group = PermissionSubMenuModel::where('status',1)->get();

        return view('admin.role.modify',compact('role','permission','rolePermissions','sub_group'));
    }

    public function update(Request $request, $id)
    {
        $id         = Crypt::decrypt($id);
        $this->validate($request, [
            'roles'       => 'required|unique:roles,name,'.$id,
            'slug'        => 'required|unique:roles,slug,'.$id,
            'permission'  => 'required',
        ]);
        $role       = Role::find($id);
        $role->name = $request->input('roles');
        $role->slug = $request->input('slug');
        $role->save();
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('roles.index')
            ->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id)
    {
        RoleModel::where('id',$id)->update(['status'=>0]);
         return redirect()->route('roles.index')->with('success',trans('message.delete_successfully'));
    }
}
