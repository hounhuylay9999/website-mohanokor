<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\PartnerModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class PartnerController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:home-list', ['only' => ['index']]);
        $this->middleware('permission:home-create', ['only' => ['create','store']]);
        $this->middleware('permission:home-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:home-delete', ['only' => ['destroy']]);
    }
    public function index()
    {   $partner = PartnerModel::where('status',1)->first();
        return view('back-end.partner.index', compact('partner'));
    }

    public function getGetPartnerList(request $request){
        $length   = $request->get("pageLength");
        $user     = PartnerModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('home-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('partner.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('home-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.partner.create');
    }

    public function store(request $request){
          $this->validate($request, [
   
        ]);

        DB::beginTransaction();
        try{
            $partner = new PartnerModel();
            $partner->title_en           = $request->title_en;
            $partner->title_kh           = $request->title_kh? $request->title_kh : $request->title_en;
            $partner->status             = 1;
            if($request->hasFile('partner_thumnail')) {
                $file             = $request->partner_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $partner->partner_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $partner->save();

        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $partner = PartnerModel::where('id',$id)->first();
        return view('back-end.partner.modify',compact('partner'));
    }

    public function update(request $request,$id){
        $this->validate($request, [    

        ]);

        DB::beginTransaction();
        try{
            $partner =  PartnerModel::where('id',$id)->first();
            $partner->title_en           = $request->title_en;
            $partner->title_kh           = $request->title_kh? $request->title_kh : $request->title_en;
            $partner->status             = 1;
            if($request->hasFile('partner_thumnail')) {
                $file             = $request->partner_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $partner->partner_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $partner->save();

        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $partner =  PartnerModel::where('id',$id)->first();
            $partner->status             = 0;
            $partner->save();
            var_dump($partner);
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}