<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AnnualReportDetailModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class AnnualReportDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $annual_report_detail = AnnualReportDetailModel::where('status',1)->first();
        return view('back-end.annual_report_detail.index', compact('annual_report_detail'));
    }

    public function getAnnualReportDetailList(request $request){
        $length   = $request->get("pageLength");
        $user     = AnnualReportDetailModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('page-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('annual_report_detail.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('page-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.annual_report_detail.create');
    }

    public function store(request $request){
          $this->validate($request, [
     
        ]);

        DB::beginTransaction();
        try{
            $annual_report_detail = new AnnualReportDetailModel();
            $annual_report_detail->title_en           = $request->title_en;
            $annual_report_detail->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $annual_report_detail->status             = 1;
            if($request->hasFile('annual_report_detail_thumnail')) {
                $file             = $request->annual_report_detail_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $annual_report_detail->annual_report_detail_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }

            if($request->hasFile('photo')) {
                $file             = $request->photo;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $annual_report_detail->photo   = $name;
                $file->move(public_path('/upload'), $name);
            }

            $annual_report_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $annual_report_detail = AnnualReportDetailModel::where('id',$id)->first();
        return view('back-end.annual_report_detail.modify',compact('annual_report_detail'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);
        DB::beginTransaction();
        try{
            $annual_report_detail =  AnnualReportDetailModel::where('id',$id)->first();
            $annual_report_detail->title_en           = $request->title_en;
            $annual_report_detail->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $annual_report_detail->status             = 1;
            if($request->hasFile('annual_report_detail_thumnail')) {
                $file             = $request->annual_report_detail_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $annual_report_detail->annual_report_detail_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            if($request->hasFile('photo')) {
                $file             = $request->photo;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $annual_report_detail->photo   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $annual_report_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $annual_report_detail =  AnnualReportDetailModel::where('id',$id)->first();
            $annual_report_detail->status             = 0;
            $annual_report_detail->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}