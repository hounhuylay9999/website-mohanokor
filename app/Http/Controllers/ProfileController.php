<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CompanyModel;;
use Auth;
use Carbon\Carbon;
use Hash;
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:company-edit', ['only' => ['index']]);
        $this->middleware('permission:company-create', ['only' => ['create','store']]);
        $this->middleware('permission:company-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:company-delete', ['only' => ['destroy']]);
    }

    public function yourProfile(){
        $userLogin    = auth::user()->id;
        $profile      = User::where('id',$userLogin)->first();
        return view('admin.profile.your-profile',compact('profile'));
    }

    public function yourProfileUpdate(request $request,$id){
        $id = auth::user()->id;
        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required|email|unique:users,email,'.$id,
        ]);
        if(\Request::input('old_password')) {
            $this->validate($request, [
                'old_password'     => 'required',
                'password'         => 'required|string|min:6|different:old_password',
                'confirm_password' => 'required_with:password|same:password|string|min:6'
            ]);
        }

        $input = User::find($id);
        $input->name = $request->name;
        if($request->hasFile('profile')) {
            $file             = $request->profile;
            $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name             = $timestamp. '-' .$file->getClientOriginalName();
            $input->profile   = $name;
            $file->move(public_path('/upload'), $name);
        }
        
        if(\Request::input('old_password')) {
            if (Hash::check($request->get('old_password'), Auth::user()->password)) {
                $user = User::find(Auth::user()->id);
                $user->password = bcrypt($request->get('password'));
                if ($user->save()) {
                    return back()->with('success',trans('message.updated_successfully'));
                }
            } else {
                return back()->with('error',trans('message.wrong_old_password'));
            }
        }
        $input->save();
        return back()->with('success',trans('message.updated_successfully'));

    }

    public function companyProfile(){
        $profile      = CompanyModel::where('status',1)->first();
        return view('admin.profile.company-profile',compact('profile'));
    }

    public function companyProfileUpdate(request $request,$id){
       
        $this->validate($request, [
            'name_en'      => 'required',
            'name_kh'      => 'required',
            'phone'        => 'required',
            'address'      => 'required',
            'email'        => 'required|email',
        ]);
        $input = CompanyModel::find($id);
        $input->name_en = $request->name_en;
        $input->name_kh = $request->name_kh;
        $input->phone   = $request->phone;
        $input->address = $request->address;
        $input->email   = $request->email;
        if($request->hasFile('profile')) {
            $file             = $request->profile;
            $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name             = $timestamp. '-' .$file->getClientOriginalName();
            $input->profile   = $name;
            $file->move(public_path('/upload'), $name);
        }
        $input->save();
        return back()->with('success',trans('message.updated_successfully'));

    }
}
