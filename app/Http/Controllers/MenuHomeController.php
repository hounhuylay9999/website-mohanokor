<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\MenuHomeModel;
use DB;
use Auth;
use Carbon\Carbon;
use Image;
class MenuHomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $menuhome = MenuHomeModel::where('status',1)->first();
        return view('back-end.menuhome.index', compact('menuhome'));
    }

    public function create()
    {
        
    }

   
    public function store(Request $request)
    {
       DB::beginTransaction();
       try {
            $menuhome =  MenuHomeModel::where('id',1)->first();
            $menuhome->title_en             = $request->title_en;
            $menuhome->title_kh             = $request->title_kh ? $request->title_kh : $request->title_en;
            $menuhome->home_url_en          = $request->home_url_en;
            $menuhome->home_url_kh          = $request->home_url_kh;
            if($request->hasFile('img')) {
                $file             = $request->img;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $menuhome->img       = $name;
                $file->move(public_path('/upload'), $name);
            }
            $menuhome->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
      
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        
    }
}