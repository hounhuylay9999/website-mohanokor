<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\GeneralModel;
use DB;
use Carbon\Carbon;
class GeneralController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('permission:general-list', ['only' => ['index']]);
        $this->middleware('permission:general-create', ['only' => ['create','store']]);
        $this->middleware('permission:general-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:general-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $general_en = GeneralModel::where('lang','en')->first();
        $general_kh = GeneralModel::where('lang','kh')->first();
        return view('back-end.general',compact('general_en','general_kh'));
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $general_en = GeneralModel::where('lang','en')->first();
            $general_en->lang = 'en';
            $general_en->header_phone       = $request->phone_number_en;
            $general_en->header_email       = $request->email_en;
            $general_en->header_fb_url      = $request->facebook_url_en;
            $general_en->header_yt_url      = $request->youtube_url_en;
            
            $general_en->footer_head        = $request->head_ofiice_title_en;
            $general_en->footer_head_desc   = $request->head_office_desc_en;
            $general_en->footer_branch      = $request->branch_title_en;
            $general_en->footer_branch_desc = $request->branch_desc_en;
            $general_en->footer_call        = $request->call_us_title_en;
            $general_en->footer_call_1      = $request->call_us_number_1_en;
            $general_en->footer_call_2      = $request->call_us_number_2_en;
            $general_en->footer_email       = $request->email_us_title_en;
            $general_en->footer_email_url   = $request->email_link_en;
            $general_en->footer_web_url     = $request->website_url_en;

            $general_en->footer_copyright   = $request->copyright_en;
            $general_en->footer_title       = $request->footer_title_en;

            $general_en->meta_keyword       = $request->meta_keyword_en;
            $general_en->meta_description   = $request->meta_description_en;

            $general_kh = GeneralModel::where('lang','kh')->first();
            $general_kh->lang = 'kh';
            $general_kh->header_phone       = $request->phone_number_kh ? $request->phone_number_kh : $request->phone_number_en;
            $general_kh->header_email       = $request->email_kh ? $request->email_kh : $request->email_en;
            $general_kh->header_fb_url      = $request->facebook_url_kh ? $request->facebook_url_kh : $request->facebook_url_en;
            $general_kh->header_yt_url      = $request->youtube_url_kh ? $request->youtube_url_kh : $request->youtube_url_en;
            
            $general_kh->footer_head        = $request->head_ofiice_title_kh ? $request->head_ofiice_title_kh : $request->head_ofiice_title_en;
            $general_kh->footer_head_desc   = $request->head_office_desc_kh ? $request->head_office_desc_kh : $request->head_office_desc_en;
            $general_kh->footer_branch      = $request->branch_title_kh ? $request->branch_title_kh : $request->branch_title_en;
            $general_kh->footer_branch_desc = $request->branch_desc_kh ? $request->branch_desc_kh : $request->branch_desc_en;
            $general_kh->footer_call        = $request->call_us_title_kh ? $request->call_us_title_kh : $request->call_us_title_en;
            $general_kh->footer_call_1      = $request->call_us_number_1_kh ? $request->call_us_number_1_kh : $request->call_us_number_1_en;
            $general_kh->footer_call_2      = $request->call_us_number_2_kh ? $request->call_us_number_2_kh : $request->call_us_number_2_en;
            $general_kh->footer_email       = $request->email_us_title_kh ? $request->email_us_title_kh : $request->email_us_title_en;
            $general_kh->footer_email_url   = $request->email_link_kh ? $request->email_link_kh : $request->email_link_en;
            $general_kh->footer_web_url     = $request->website_url_kh ? $request->website_url_kh : $request->website_url_en;

            $general_kh->footer_copyright   = $request->copyright_kh ? $request->copyright_kh : $request->copyright_en;
            $general_kh->footer_title       = $request->footer_title_kh ? $request->footer_title_kh : $request->footer_title_en;

            $general_kh->meta_keyword       = $request->meta_keyword_kh;
            $general_kh->meta_description   = $request->meta_description_kh;

            if($request->hasFile('header_logo') || $request->hasFile('header_logo_kh')) {
                $file             = $request->header_logo ? $request->header_logo : $request->header_logo_kh;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->header_logo   = $name;
                $general_kh->header_logo   = $name;
                $file->move(public_path('/upload'), $name);
            }
            
            if($request->hasFile('favicon')) {
                $file             = $request->favicon;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->favicon   = $name;
                $general_kh->favicon   = $name;
                $file->move(public_path('/upload'), $name);
            }

            if($request->hasFile('footer_logo')) {
                $file             = $request->footer_logo;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->footer_logo   = $name;
                $general_kh->footer_logo   = $name;
                $file->move(public_path('/upload'), $name);
            }

            if($request->hasFile('product_cover')) {
                $file             = $request->product_cover;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->product_cover   = $name;
                $general_kh->product_cover   = $name;
                $file->move(public_path('/upload'), $name);
            }

            if($request->hasFile('about_cover')) {
                $file             = $request->about_cover;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->about_cover   = $name;
                $general_kh->about_cover   = $name;
                $file->move(public_path('/upload'), $name);
            }

            if($request->hasFile('team_cover')) {
                $file             = $request->team_cover;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->team_cover   = $name;
                $general_kh->team_cover   = $name;
                $file->move(public_path('/upload'), $name);
            }

            if($request->hasFile('news_cover')) {
                $file             = $request->news_cover;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->news_cover   = $name;
                $general_kh->news_cover   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $general_en->save();
            $general_kh->save();
            
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong With Customer!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        
    }
}