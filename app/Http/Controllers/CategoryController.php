<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CategoryModel;
use App\Models\ProductModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $category = CategoryModel::where('status',1)->first();
        return view('back-end.category.index', compact('category'));
    }

    public function getCategoryList(request $request){
        $length   = $request->get("pageLength");
        $user  = CategoryModel::with('get_response')
        ->select('category.*',DB::raw('product.title_en as product_title'))
        ->join('product', 'product.id', 'category.product_id')
        ->where('category.status',1)
        ->orderBy('category.id','desc')
        ->get();

        return datatables()::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('page-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('category.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('page-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
            
    }

    public function create()
    {   $product = ProductModel::where('status',1)->get();
        return view('back-end.category.create', compact('product'));
    }

    public function store(request $request){
          $this->validate($request, [
       
        ]);

        DB::beginTransaction();
        try{
            $category = new CategoryModel();
            $category->title_en           = $request->title_en;
            $category->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $category->product_id         = $request->product_id;
            $category->status             = 1;
            $category->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $product = ProductModel::where('status',1)->get();
        $category = CategoryModel::where('id',$id)->first();
        return view('back-end.category.modify',compact('category','product'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $category =  CategoryModel::where('id',$id)->first();
            $category->title_en           = $request->title_en;
            $category->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $category->product_id         = $request->product_id;
            $category->status             = 1;
            $category->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $category =  CategoryModel::where('id',$id)->first();
            $category->status             = 0;
            $category->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}