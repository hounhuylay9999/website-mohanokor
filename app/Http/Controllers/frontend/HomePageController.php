<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HerobannerModel;
use App\Models\PhotoDetailModel;
use App\Models\VideoDetailModel;
use App;
use App\Models\PhotoModel;
use App\Models\ProductServiceModel;
use App\Models\VideoModel;
use App\Models\MissionModel;
use App\Models\OrganizationModel;
use App\Models\ComplaintFormModel;
use App\Models\PartnerModel;
use App\Models\MenuHomeModel;
use App\Models\ManagementModel;
use App\Models\HistoryModel;
use App\Models\HistoryDetailModel;
use App\Models\EformModel;
use App\Models\NewsDetailModel;
use App\Models\NewsModel;
use App\Models\JobTitleModel;
use App\Models\JobPostModel;
use App\Models\ProductModel;
use App\Models\CategoryModel;
use App\Models\MoreInformationModel;
use App\Models\BranchModel;
use App\Models\ExchangeRateRealModel;
use App\Models\PositionApplyModel;
use App\Models\BranchesNetworkModel;
use App\Models\AnnualReportModel;
use App\Models\AnnualReportDetailModel;
use App\Models\ProvinceModel;
use App\Models\ManagementDetailModel;
use DB;

class HomePageController extends Controller
{
    public function HomePage()
    {
    
    $news = NewsModel::where('status',1)->first();
    $partner = PartnerModel::where('status',1)->get();
    $herobanner     = HerobannerModel::where('status',1)
    ->orderBy('id','desc')->get();

    $service_product = ProductServiceModel::select([
        'title_'.App::getLocale().' as title',
        'sort_text_'.App::getLocale().' as sort_text',
        'full_text_'.App::getLocale().' as full_text',
        'product_id',
        'category_id',
        'product_service_thumnail_en',
        'product_service_thumnail_kh',
        
    ])->where('status',1)->get();

    $more_information = MoreInformationModel::select([
        'title_'.App::getLocale().' as title',
        'more_information_thumnail'
    ])->where('status',1)->get();

    $menuhome = MenuHomeModel::select([
        'title_'.App::getLocale().' as title',
        'img'
    ])->where('status',1)->get();

    $news_detail = NewsDetailModel::select([
        'title_'.App::getLocale().' as title',
        'short_desc_'.App::getLocale().' as short_desc',
        'news_detail_thumnail',
        'event_date',
        'id',
    ])->where('status',1)->get();

    $exchange_rate = ExchangeRateRealModel::select([
    'title_'.App::getLocale().' as title',
    'buy',
    'sell',
    'date_post'
    ])->where('status',1)->get();

    return view('front-end.page.home',compact('herobanner', 'partner', 'menuhome', 'news', 'service_product', 'more_information', 'news_detail', 'exchange_rate'));
    }
    
    public function History(){
        $history = HistoryModel::select([
            'title_'.App::getLocale().' as title',
            'history_thumnail'
        ])->where('status',1)->first();
        $history_detail = HistoryDetailModel::select([
            'title_'.App::getLocale().' as title',
            'short_desc_'.App::getLocale().' as short_desc'
        ])->where('status',1)->get();
      return view('front-end.page.history',compact('history_detail','history'));
    }

    public function Getmenu()
    {
        $menuhome = MenuHomeModel::where('status',1)->first();
        return  $menuhome;
    }

    public function Organization(){
        $organization = OrganizationModel::select([
            'title_'.App::getLocale().' as title',
            'organization_thumnail_en',
            'organization_thumnail_kh'
        ])->where('status',1)->first();
        return view('front-end.page.organization', compact('organization'));
    }

    public function Management(){
        $management = ManagementModel::select([
            'title_'.App::getLocale().' as title',
            'management_thumnail_en',
            'management_thumnail_kh',
        ])->where('status',1)->first();
        return view('front-end.page.management', compact('management'));
    }

    public function Photo(){
        $photo = PhotoModel::select([
            'title_'.App::getLocale().' as title',
            'photo_thumnail'
        ])->where('status',1)->first();
        $photo_detail = PhotoDetailModel::select([
            'title_'.App::getLocale().' as title',
            'short_desc_'.App::getLocale().' as short_desc',
            'type',
            'photo_detail_thumnail'
        ])->where('status',1)->get();
        return view('front-end.page.photo', compact('photo', 'photo_detail'));
    }

    public function Video(){
        $video = VideoModel::select([
            'title_'.App::getLocale().' as title'
        ])->where('status',1)->first();
        $video_detail = VideoDetailModel::select([
            'title_'.App::getLocale().' as title',
            'short_desc_'.App::getLocale().' as short_desc',
            'type',
            'url'
        ])->where('status',1)->get();
        return view('front-end.page.video', compact('video','video_detail'));
    }
    
    public function News_Event(){
        $news = NewsModel::select([
            'title_'.App::getLocale().' as title'
        ])->where('status',1)->first();
        $news_detail = NewsDetailModel::select([
            'title_'.App::getLocale().' as title',
            'short_desc_'.App::getLocale().' as short_desc',
            'news_detail_thumnail',
            'event_date',
            'id',
        ])->where('status',1)->get();
        return view('front-end.page.news', compact('news', 'news_detail'));
    }

    public function JobPost(){
        $job_title = JobTitleModel::select([
            'title_'.App::getLocale().' as title',
            'job_title_thumnail'
        ])->where('status',1)->first();

        $job_post = JobPostModel::
            select('job_post.*','position.name_en','position.name_kh',DB::raw('location.name_en as location_name_en, location.name_kh as location_name_kh'))
            ->join('position', 'position.id', 'job_post.position_id')
            ->join('location', 'location.id', 'job_post.location_id')
            ->where('job_post.status',1)
            ->orderBy('job_post.id','desc')
            ->get();
        return view('front-end.career.job_post', compact('job_title', 'job_post'));
    }

    public function Complaint(){
       
        $complaint_form = ComplaintFormModel::select([
            'title_'.App::getLocale().' as title',
            'sort_text_'.App::getLocale().' as sort_text',
            'full_text_'.App::getLocale().' as full_text',
            'complaint_form_thumnail'
        ])->where('status',1)->first();
        return view('front-end.contact.complant_form', compact('complaint_form'));
    }

    public function product_show(){
        $service_product = ProductServiceModel::select([
            'title_'.App::getLocale().' as title',
            'sort_text_'.App::getLocale().' as sort_text',
            'full_text_'.App::getLocale().' as full_text',
            'product_service_thumnail_en',
            'product_service_thumnail_kh',
            
        ])->where('status',1)->where('category_id',$_GET['type'])->first();

        $product = ProductModel::select([
            'title_'.App::getLocale().' as title'
            
        ])->where('id',$_GET['main'])->first();
        $category = CategoryModel::select([
            'title_'.App::getLocale().' as title',
            'id',
            'product_id'
            
        ])->where('product_id',$_GET['main'])->get();
        return view('front-end.product.index', compact('service_product','product','category'));
    }

    public function Mission(){
        $mission = MissionModel::select([
            'vision_title_'.App::getLocale().' as vision_title',
            'vision_des_'.App::getLocale().' as vision_des',
            'mission_title_'.App::getLocale().' as mission_title',
            'mission_des_'.App::getLocale().' as mission_des',
            'core_title_'.App::getLocale().' as core_title',
            'core_des_'.App::getLocale().' as core_des',
            'trust_title_'.App::getLocale().' as trust_title',
            'trust_des_'.App::getLocale().' as trust_des',
            'exellence_title_'.App::getLocale().' as exellence_title',
            'exellence_des_'.App::getLocale().' as exellence_des',
            'accountability_title_'.App::getLocale().' as accountability_title',
            'accountability_des_'.App::getLocale().' as accountability_des',
            'morality_title_'.App::getLocale().' as morality_title',
            'morality_des_'.App::getLocale().' as morality_des',
            
        ])->where('status',1)->first();
        return view('front-end.page.mission', compact('mission'));
    }

    public function Eform(){
        $position_apply = PositionApplyModel::select([
            'name_'.App::getLocale().' as name',
        ])->where('status',1)->get();
        $e_form = EformModel::select([
            'title_'.App::getLocale().' as title',
            'e_form_thumnail'
        ])->where('status',1)->first();
        return view('front-end.career.e_form', compact('position_apply','e_form'));
    }

    public function NewFeature(){
        $news_detail = NewsDetailModel::select([
            'title_'.App::getLocale().' as title',
            'short_desc_'.App::getLocale().' as short_desc',
            'news_detail_thumnail',
            'event_date',
            'id'
        ])->where('id',$_GET['detail'])->get();
        return view('front-end.page.news_feature',compact('news_detail'));
    }

    public function BranchesNetwork(){
        $branches_network = BranchModel::select([
            'province_id',
            'title_'.App::getLocale().' as title',
            'address_'.App::getLocale().' as address',
            'phone_'.App::getLocale().' as phone',
            'e_mail'
        ])->where('status',1)->get();

        $branches_network_title = BranchesNetworkModel::select([
            'title_'.App::getLocale().' as title',
        ])->where('status',1)->first();

        $province = ProvinceModel::select([
            'id',
            // 'title_'.App::getLocale().' as title',
        ])->where('status',1)->get();
        
        return view('front-end.contact.branches_network',compact('branches_network','branches_network_title','province'));
    }
    
    public function AnnualReport(){
        $annual_report = AnnualReportModel::select([
            'title_'.App::getLocale().' as title',
        ])->where('status',1)->first();

        $annual_report_detail = AnnualReportDetailModel::select([
            'title_'.App::getLocale().' as title',
            'annual_report_detail_thumnail',
            'photo'
        ])->where('status',1)->first();
        return view('front-end.page.annual_report',compact('annual_report', 'annual_report_detail'));
    }

    public function Management_Role(){
        $management_role = ManagementDetailModel::select([
            'title_'.App::getLocale().' as title',
            'short_desc_'.App::getLocale().' as short_desc',
            'skill_'.App::getLocale().' as skill',
            'management_detail_thumnail',
            'id'
        ])->where('status',1)->where('id',$_GET['details'])->get();
        return view('front-end.page.management_role',compact('management_role'));
    }


}