<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\AboutUsMenuModel;
use DB;
use Auth;
use Carbon\Carbon;
use Image;
class AboutUsController extends Controller
{
  
    public function __construct()
    {
       
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $aboutus = AboutUsMenuModel::where('status',1)->first();
        return view('back-end.aboutus.index', compact('aboutus'));
    }

    public function create()
    {
        
    }

   
    public function store(Request $request)
    {
       DB::beginTransaction();
       try {
            
            $aboutus = AboutUsMenuModel::where('id',1)->first();
            $aboutus->h_title_en = $request->h_title_en;
            $aboutus->h_title_kh = $request->h_title_kh;
            $aboutus->h_desc_en = $request->h_desc_en;
            $aboutus->h_desc_kh = $request->h_desc_kh;
           
            if($request->hasFile('h_thumbnail')) {
                $file             = $request->h_thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $aboutus->h_thumbnail       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $aboutus->v_title_en = $request->v_title_en;
            $aboutus->v_title_kh = $request->v_title_kh;
            $aboutus->v_desc_en = $request->v_desc_en;
            $aboutus->v_desc_kh = $request->v_desc_kh;

            if($request->hasFile('v_thumbnail')) {
                $file             = $request->v_thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $aboutus->v_thumbnail       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $aboutus->o_title_en = $request->o_title_en;
            $aboutus->o_title_kh = $request->o_title_kh;
            $aboutus->o_desc_en = $request->o_desc_en;
            $aboutus->o_desc_kh = $request->o_desc_kh;

            if($request->hasFile('o_thumbnail')) {
                $file             = $request->o_thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $aboutus->o_thumbnail       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $aboutus->m_title_en = $request->m_title_en;
            $aboutus->m_title_kh = $request->m_title_kh;
            $aboutus->m_desc_en = $request->m_desc_en;
            $aboutus->m_desc_kh = $request->m_desc_kh;

            if($request->hasFile('m_thumbnail')) {
                $file             = $request->m_thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $aboutus->m_thumbnail       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $aboutus->p_title_en = $request->p_title_en;
            $aboutus->p_title_kh = $request->p_title_kh;
            $aboutus->p_desc_en     = $request->p_desc_en;
            $aboutus->p_desc_kh     = $request->p_desc_kh;

            if($request->hasFile('p_thumbnail')) {
                $file             = $request->p_thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $aboutus->p_thumbnail       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $aboutus->vdo_title_en = $request->vdo_title_en;
            $aboutus->vdo_title_kh = $request->vdo_title_kh;
            $aboutus->vdo_desc_en     = $request->vdo_desc_en;
            $aboutus->vdo_desc_kh     = $request->vdo_desc_kh;

            if($request->hasFile('vdo_location')) {
                $file             = $request->vdo_location;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $aboutus->vdo_location       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $aboutus->n_title_en = $request->n_title_en;
            $aboutus->n_title_kh = $request->n_title_kh;
            $aboutus->n_desc_en     = $request->n_desc_en;
            $aboutus->n_desc_kh     = $request->n_desc_kh;

            if($request->hasFile('n_thumbnail')) {
                $file             = $request->n_thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $aboutus->n_thumbnail       = $name;
                $file->move(public_path('/upload'), $name);
            }

            $aboutus->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
      
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        
    }
}