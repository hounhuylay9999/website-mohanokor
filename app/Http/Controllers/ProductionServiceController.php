<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\ProductServiceModel;
use App\Models\ProductModel;
use App\Models\CategoryModel;
use App\Models\User;
use DB;
use Auth;
use Carbon\Carbon;
use Image;
class ProductionServiceController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {
       
        return view('back-end.product_service.index');
    }

    public function getProductServiceList(request $request){
        $length   = $request->get("pageLength");
        $user  = ProductServiceModel::with('get_response')
        ->select('product_service.*','product.title_en',DB::raw('category.title_en as category_title'))
        ->join('product', 'product.id', 'product_service.product_id')
        ->join('category', 'category.id', 'product_service.category_id')
        ->where('product_service.status',1)
        ->orderBy('product_service.id','desc')
        ->get();

        return datatables()::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('page-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('product_service.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('page-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
            
    }
    
    public function create()
    {
        $product = ProductModel::where('status',1)->get();
        $category = CategoryModel::where('status',1)->get();
        $product_service = ProductServiceModel::where('status',1)->get();
        return view('back-end.product_service.create', compact('product', 'category'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
      
        ]);

        DB::beginTransaction();
        try {
            $product_service = new ProductServiceModel();
            $product_service->product_id                   = $request->product_id;
            $product_service->category_id                  = $request->category_id;
            $product_service->title_en                     = $request->title_en;
            $product_service->title_kh                     = $request->title_kh;
            $product_service->sort_text_en                 = $request->sort_text_en;
            $product_service->sort_text_kh                 = $request->sort_text_kh;
            $product_service->full_text_en                 = $request->full_text_en;
            $product_service->full_text_kh                 = $request->full_text_kh;
            $product_service->status                       = 1;
            $product_service->create_by                    = Auth::user()->id;
            
            if($request->hasFile('product_service_thumnail_en')) {
                $file             = $request->product_service_thumnail_en;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $product_service->product_service_thumnail_en       = $name;
                $file->move(public_path('/upload'), $name);
            }
            if($request->hasFile('product_service_thumnail_kh')) {
                $file             = $request->product_service_thumnail_kh;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $product_service->product_service_thumnail_kh       = $name;
                $file->move(public_path('/upload'), $name);
            }
            $product_service->save();
            DB::commit();
            }catch(\Exception $e){
                DB::rollback();
                return back()->with('warning','Something Went Wrong!');
            }
            return back()->with('success',trans('message.save_successfully'));

    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $product = ProductModel::where('status',1)->get();
        $category = CategoryModel::where('status',1)->get();
        $product_service = ProductServiceModel::where('id', $id)->first();
        return view('back-end.product_service.modify',compact('product_service', 'product', 'category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            
        ]);
       
        DB::beginTransaction();
        try {
            $product_service =  ProductServiceModel::where('id',$id)->first();
            $product_service->product_id                   = $request->product_id;
            $product_service->category_id                  = $request->category_id;
            $product_service->title_en                     = $request->title_en;
            $product_service->title_kh                     = $request->title_kh;
            $product_service->sort_text_en                 = $request->sort_text_en;
            $product_service->sort_text_kh                 = $request->sort_text_kh;
            $product_service->full_text_en                 = $request->full_text_en;
            $product_service->full_text_kh                 = $request->full_text_kh;
            $product_service->status                       = 1;
            $product_service->create_by                    = Auth::user()->id;
            
            if($request->hasFile('product_service_thumnail_en')) {
                $file             = $request->product_service_thumnail_en;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $product_service->product_service_thumnail_en       = $name;
                $file->move(public_path('/upload'), $name);
            }
            if($request->hasFile('product_service_thumnail_kh')) {
                $file             = $request->product_service_thumnail_kh;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $product_service->product_service_thumnail_kh       = $name;
                $file->move(public_path('/upload'), $name);
            }
            $product_service->save(); 
            DB::commit();
            }catch(\Exception $e){
                DB::rollback();
                return back()->with('warning','Something Went Wrong!');
                
            }
            return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $product_service =  ProductServiceModel::where('id',$id)->first();
            $product_service->status           = 0;
            $product_service->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }

}