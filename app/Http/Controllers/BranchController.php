<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BranchModel;
use App\Models\ProvinceModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class BranchController extends Controller
{
    public function __construct()
    {
        //$this->middleware('permission:user-list');
        $this->middleware('permission:contact_us-list', ['only' => ['index']]);
        $this->middleware('permission:contact_us-create', ['only' => ['create','store']]);
        $this->middleware('permission:contact_us-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:contact_us-delete', ['only' => ['destroy']]);
    }
    //
    public function index()
    {   
        $branch = BranchModel::where('status',1)->first();
        return view('back-end.branch.index', compact('branch'));
    }

    
    /**
     * getBannerList
     */
    public function getBranchList(request $request){
        $length   = $request->get("pageLength");
        $user  = BranchModel::with('get_response')
        ->select('branch.*',DB::raw('province.title_en as province_title'))
        ->join('province', 'province.id', 'branch.province_id')
        ->where('branch.status',1)
        ->orderBy('branch.id','desc')
        ->get();
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('contact_us-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('branch.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('contact_us-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    
    //
    public function create()
    {   $province = ProvinceModel::where('status',1)->get();
        return view('back-end.branch.create', compact('province'));
    }

    public function store(request $request){
          //dd($request->all());
          $this->validate($request, [
            // 'title_en'               => 'required',
            // 'banner'                 => 'required',       
        ]);

        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $branch = new BranchModel();
            $branch->province_id        = $request->province_id;
            $branch->title_en           = $request->title_en;
            $branch->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $branch->address_en         = $request->address_en;
            $branch->address_kh         = $request->address_kh ? $request->address_kh : $request->address_en;
            $branch->phone_en           = $request->phone_en;
            $branch->phone_kh           = $request->phone_kh ? $request->phone_kh : $request->phone_en;
            $branch->e_mail             = $request->e_mail;
            $branch->status             = 1;

            //save
            $branch->save();
            // var_dump($branch->title_en);
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $province = ProvinceModel::where('status',1)->get();
        $branch = BranchModel::where('id',$id)->first();
        return view('back-end.branch.modify',compact('branch','province'));
    }

    public function update(request $request,$id){
           //dd($request->all());
        $this->validate($request, [
            // 'title_en'               => 'required',
        ]);

        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $branch =  BranchModel::where('id',$id)->first();
            $branch->province_id        = $request->province_id;
            $branch->title_en           = $request->title_en;
            $branch->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $branch->address_en         = $request->address_en;
            $branch->address_kh         = $request->address_kh ? $request->address_kh : $request->address_en;
            $branch->phone_en           = $request->phone_en;
            $branch->phone_kh           = $request->phone_kh ? $request->phone_kh : $request->phone_en;
            $branch->e_mail             = $request->e_mail;
            $branch->status             = 1;

            //save
            $branch->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $branch =  BranchModel::where('id',$id)->first();
            $branch->status             = 0;
            $branch->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}