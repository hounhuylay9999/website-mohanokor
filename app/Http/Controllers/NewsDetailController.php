<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsDetailModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class NewsDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $news_detail = NewsDetailModel::where('status',1)->first();
        return view('back-end.news_detail.index', compact('news_detail'));
    }

    public function getNewsDetailList(request $request){
        $length   = $request->get("pageLength");
        $user     = NewsDetailModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('page-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('news_detail.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('page-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.news_detail.create');
    }

    public function store(request $request){
          $this->validate($request, [
     
        ]);

        DB::beginTransaction();
        try{
            $news_detail = new NewsDetailModel();
            $news_detail->title_en           = $request->title_en;
            $news_detail->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $news_detail->short_desc_en      = $request->short_desc_en;
            $news_detail->short_desc_kh      = $request->short_desc_kh ? $request->short_desc_kh : $request->short_desc_en;
            $news_detail->event_date         = $request->event_date;

            $news_detail->status             = 1;
            if($request->hasFile('news_detail_thumnail')) {
                $file             = $request->news_detail_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $news_detail->news_detail_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $news_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $news_detail = NewsDetailModel::where('id',$id)->first();
        return view('back-end.news_detail.modify',compact('news_detail'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $news_detail =  NewsDetailModel::where('id',$id)->first();
            $news_detail->title_en           = $request->title_en;
            $news_detail->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $news_detail->short_desc_en      = $request->short_desc_en;
            $news_detail->short_desc_kh      = $request->short_desc_kh ? $request->short_desc_kh : $request->short_desc_en;
            $news_detail->event_date         = $request->event_date;
            $news_detail->status             = 1;
            if($request->hasFile('news_detail_thumnail')) {
                $file             = $request->news_detail_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $news_detail->news_detail_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $news_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $news_detail =  NewsDetailModel::where('id',$id)->first();
            $news_detail->status             = 0;
            $news_detail->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}