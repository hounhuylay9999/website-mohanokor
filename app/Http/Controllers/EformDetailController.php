<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EformDetailModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class EformDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }

    public function store(request $request){
          $this->validate($request, [
                   
        ]);
        DB::beginTransaction();
        try{
            $e_form_detail = new EformDetailModel();
            $e_form_detail->position_apply_id    = $request->position_apply_id;
            $e_form_detail->first_name           = $request->first_name;
            $e_form_detail->last_name            = $request->last_name;
            $e_form_detail->e_mail               = $request->e_mail;
            $e_form_detail->phone                = $request->phone;
            $e_form_detail->status               = 1;
            if($request->hasFile('file')) {
                $file             = $request->file;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $e_form_detail->file   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $e_form_detail->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

}