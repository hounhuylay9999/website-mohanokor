<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JobTitleModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class JobTitleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:careers-list', ['only' => ['index']]);
        $this->middleware('permission:careers-create', ['only' => ['create','store']]);
        $this->middleware('permission:careers-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:careers-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $job_title = JobTitleModel::where('status',1)->first();
        return view('back-end.job_title.index', compact('job_title'));
    }

    public function getJobTitleList(request $request){
        $length   = $request->get("pageLength");
        $user     = JobTitleModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);
       
        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('careers-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('job_title.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('careers-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    public function create()
    {   
        return view('back-end.job_title.create');
    }

    public function store(request $request){
          $this->validate($request, [
               
        ]);

        DB::beginTransaction();
        try{
            $job_title = new JobTitleModel();
            $job_title->title_en           = $request->title_en;
            $job_title->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $job_title->status            = 1;
            if($request->hasFile('job_title_thumnail')) {
                $file             = $request->job_title_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $job_title  ->  job_title_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $job_title->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $job_title = JobTitleModel::where('id',$id)->first();
        return view('back-end.job_title.modify',compact('job_title'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
            $job_title =  JobTitleModel::where('id',$id)->first();
            $job_title->title_en           = $request->title_en;
            $job_title->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;
            $job_title->status            = 1;
            if($request->hasFile('job_title_thumnail')) {
                $file             = $request->job_title_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $job_title  ->  job_title_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $job_title->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $job_title =  JobTitleModel::where('id',$id)->first();
            $job_title->status             = 0;
            $job_title->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}