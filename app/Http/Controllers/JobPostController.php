<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\JobPostModel;
use App\Models\LocationModel;
use App\Models\PositionModel;
// use App\Models\User;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class JobPostController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:careers-list', ['only' => ['index']]);
        $this->middleware('permission:careers-create', ['only' => ['create','store']]);
        $this->middleware('permission:careers-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:careers-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {   
        $job_post = JobPostModel::where('status',1)->first();
        return view('back-end.job_post.index', compact('job_post'));
    }

    public function getJobPostList(request $request){
        $length   = $request->get("pageLength");
        $user     = JobPostModel::with('get_response')
        ->select('job_post.*','position.name_en',DB::raw('location.name_en as location_name'))
        ->join('position', 'position.id', 'job_post.position_id')
        ->join('location', 'location.id', 'job_post.location_id')
        ->where('job_post.status',1)
        ->orderBy('job_post.id','desc')
        ->get();

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('careers-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('job_post.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                endif;
                if(auth::user()->can('careers-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                endif;
                return $edit.$delete;
            })
            
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }
    
    public function create()
    {   
        
        $location = LocationModel::where('status',1)->get();
        $position = PositionModel::where('status',1)->get();
        $job_post = JobPostModel::where('status',1)->get();
        return view('back-end.job_post.create', compact('location','position'));
    }

    public function store(request $request){
      
          $this->validate($request, [
            'position_id'               => 'required',
            'location_id'               => 'required',       
            'date_line'                 => 'required',       
        ]);

        DB::beginTransaction();
        try{
            
            $job_post = new JobPostModel();
            $job_post->position_id           = $request->position_id;
            $job_post->location_id           = $request->location_id;
            $job_post->date_line             = $request->date_line;
            $job_post->status                = 1;
            $job_post->created_by            = Auth::user()->id;
            if($request->hasFile('job_post_thumnail')) {
                $file             = $request->job_post_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $job_post->job_post_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
        
            $job_post->save();
            var_dump($request->position_id);
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $job_post = JobPostModel::where('id',$id)->first();
        $location = LocationModel::where('status',1)->get();
        $position = PositionModel::where('status',1)->get();
        return view('back-end.job_post.modify',compact('job_post','location','position'));
    }

    public function update(request $request,$id){
        $this->validate($request, [

        ]);

        DB::beginTransaction();
        try{
          
            $job_post =  JobPostModel::where('id',$id)->first();
            $job_post->position_id           = $request->position_id;
            $job_post->location_id           = $request->location_id;
            $job_post->date_line             = $request->date_line;
            $job_post->status                = 1;
            $job_post->created_by            = Auth::user()->id;
            if($request->hasFile('job_post_thumnail')) {
                $file             = $request->job_post_thumnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $job_post->job_post_thumnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $job_post->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $job_post =  JobPostModel::where('id',$id)->first();
            $job_post->status             = 0;
            $job_post->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}