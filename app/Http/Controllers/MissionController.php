<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\MissionModel;
use DB;
use Auth;
use Carbon\Carbon;
use Image;
class MissionController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('permission:page-list', ['only' => ['index']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $mission = MissionModel::where('status',1)->first();
        return view('back-end.mission.index', compact('mission'));
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
       DB::beginTransaction();
       try {
            $mission =  MissionModel::where('id',1)->first();
            $mission->vision_title_en = $request->vision_title_en;
            $mission->vision_title_kh = $request->vision_title_kh;
            $mission->vision_des_en = $request->vision_des_en;
            $mission->vision_des_kh = $request->vision_des_kh;

            $mission->mission_title_en = $request->mission_title_en;
            $mission->mission_title_kh = $request->mission_title_kh;
            $mission->mission_des_en = $request->mission_des_en;
            $mission->mission_des_kh = $request->mission_des_kh;

            $mission->core_title_en = $request->core_title_en;
            $mission->core_title_kh = $request->core_title_kh;
            $mission->core_des_en = $request->core_des_en;
            $mission->core_des_kh = $request->core_des_kh;

            $mission->trust_title_en = $request->trust_title_en;
            $mission->trust_title_kh = $request->trust_title_kh;
            $mission->trust_des_en = $request->trust_des_en;
            $mission->trust_des_kh = $request->trust_des_kh;

            $mission->exellence_title_en = $request->exellence_title_en;
            $mission->exellence_title_kh = $request->exellence_title_kh;
            $mission->exellence_des_en = $request->exellence_des_en;
            $mission->exellence_des_kh = $request->exellence_des_kh;

            $mission->accountability_title_en = $request->accountability_title_en;
            $mission->accountability_title_kh = $request->accountability_title_kh;
            $mission->accountability_des_en = $request->accountability_des_en;
            $mission->accountability_des_kh = $request->accountability_des_kh;

            $mission->morality_title_en = $request->morality_title_en;
            $mission->morality_title_kh = $request->morality_title_kh;
            $mission->morality_des_en = $request->morality_des_en;
            $mission->morality_des_kh = $request->morality_des_kh;
           
            $mission->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
      
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    
    public function destroy($id)
    {
        
    }
}