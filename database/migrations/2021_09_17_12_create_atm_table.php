<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtmTable extends Migration
{

    public function up()
    {
        Schema::create('atm', function (Blueprint $table) {
            $table->id();
            $table->string('title_en');
            $table->string('title_kh');
            $table->string('short_title_en');
            $table->string('short_title_kh');
            $table->string('short_desc_en');
            $table->string('short_desc_kh');
            $table->text('atm_thumnail');
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('atm');
    }
}