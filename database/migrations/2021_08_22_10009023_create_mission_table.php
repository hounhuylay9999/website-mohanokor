<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionTable extends Migration
{
    public function up()
    {
        Schema::create('mission', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vision_title_en')->nullable();
            $table->string('vision_title_kh')->nullable();
            $table->text('vision_des_en',250)->nullable();
            $table->text('vision_des_kh',250)->nullable();
            $table->string('mission_title_en')->nullable();
            $table->string('mission_title_kh')->nullable();
            $table->text('mission_des_en',250)->nullable();
            $table->text('mission_des_kh',250)->nullable();
            $table->string('core_title_en')->nullable();
            $table->string('core_title_kh')->nullable();
            $table->text('core_des_en',250)->nullable();
            $table->text('core_des_kh',250)->nullable();
            $table->string('trust_title_en')->nullable();
            $table->string('trust_title_kh')->nullable();
            $table->text('trust_des_en',250)->nullable();
            $table->text('trust_des_kh',250)->nullable();
            $table->string('exellence_title_en')->nullable();
            $table->string('exellence_title_kh')->nullable();
            $table->text('exellence_des_en',250)->nullable();
            $table->text('exellence_des_kh',250)->nullable();
            $table->string('accountability_title_en')->nullable();
            $table->string('accountability_title_kh')->nullable();
            $table->text('accountability_des_en',250)->nullable();
            $table->text('accountability_des_kh',250)->nullable();
            $table->string('morality_title_en')->nullable();
            $table->string('morality_title_kh')->nullable();
            $table->text('morality_des_en',250)->nullable();
            $table->text('morality_des_kh',250)->nullable();
            $table->integer('update_by')->nullable();
            $table->integer('status')->default(1);           
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mission');
    }
}