<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintFormDetailTable extends Migration
{

    public function up()
    {
        Schema::create('complaint_form_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name')->nullable();
            $table->text('address')->nullable();
            $table->text('phone')->nullable();
            $table->text('e_mail')->nullable();
            $table->text('txt1')->nullable();
            $table->text('txt2')->nullable();
            $table->text('file')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('complaint_form_detail');
    }
}
