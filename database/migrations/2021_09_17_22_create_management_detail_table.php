<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagementDetailTable extends Migration
{
    public function up()
    {
        Schema::create('management_detail', function (Blueprint $table) {
            $table->id();
            $table->string('title_en');
            $table->string('title_kh');
            $table->text('skill_en');
            $table->text('skill_kh');
            $table->text('short_desc_en');
            $table->text('short_desc_kh');
            $table->text('management_detail_thumnail');
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('management_detail');
    }
}