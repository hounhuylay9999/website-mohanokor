<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductServiceTable extends Migration
{
    public function up()
    {
        Schema::create('product_service', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_id')->nullable();
            $table->string('category_id')->nullable();
            $table->string('title_en')->nullable();
            $table->string('title_kh')->nullable();
            $table->text('sort_text_en')->nullable();
            $table->text('sort_text_kh')->nullable();
            $table->text('full_text_en')->nullable();
            $table->text('full_text_kh')->nullable();
            $table->text('product_service_thumnail_en')->nullable();
            $table->text('product_service_thumnail_kh')->nullable();
            $table->integer('create_by')->nullable();
            $table->integer('update_by')->nullable();
            $table->integer('status')->nullable();          
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_service');
    }
}
