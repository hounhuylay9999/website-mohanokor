<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('email',250)->unique();
            $table->string('profile',250)->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password',500);
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('entry_by')->nullable();
            $table->tinyInteger('update_by')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
