<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoDetailTable extends Migration
{
    public function up()
    {
        Schema::create('video_detail', function (Blueprint $table) {
            $table->id();
            $table->string('title_en');
            $table->string('title_kh');
            $table->string('short_desc_en');
            $table->string('short_desc_kh');
            $table->string('type');
            $table->text('url');
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('video_detail');
    }
}