<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeRateRealTable extends Migration
{

    public function up()
    {
        Schema::create('exchange_rate_real', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en')->nullable();
            $table->string('title_kh')->nullable();
            $table->text('buy')->nullable();
            $table->text('sell')->nullable();
            $table->text('date_post')->nullable();
            $table->integer('update_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('status')->default(1);           
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('exchange_rate_real');
    }
}