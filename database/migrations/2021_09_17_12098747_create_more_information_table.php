<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoreInformationTable extends Migration
{
    public function up()
    {
        Schema::create('more_information', function (Blueprint $table) {
            $table->id();
            $table->string('title_en');
            $table->string('title_kh');
            $table->text('more_information_thumnail');
            $table->integer('status');
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('more_information');
    }
}