<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostTable extends Migration
{

    public function up()
    {
        Schema::create('job_post', function (Blueprint $table) {
            $table->id();
            $table->string('position_id');
            $table->string('location_id');
            $table->date('date_line');
            $table->text('job_post_thumnail');
            $table->integer('status');
            $table->Integer('created_by')->nullable();
            $table->Integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('job_post');
    }
}