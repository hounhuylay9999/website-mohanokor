<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintFormTable extends Migration
{

    public function up()
    {
        Schema::create('complaint_form', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en')->nullable();
            $table->string('title_kh')->nullable();
            $table->text('sort_text_en')->nullable();
            $table->text('sort_text_kh')->nullable();
            $table->text('full_text_en')->nullable();
            $table->text('full_text_kh')->nullable();
            $table->text('complaint_form_thumnail')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('update_by')->nullable();
            $table->integer('status')->nullable();          
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('complaint_form');
    }
}
