<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuHomeTable extends Migration
{

    public function up()
    {
        Schema::create('menuhome', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en')->nullable();
            $table->string('title_kh')->nullable();
            $table->string('home_url_en',250)->nullable();
            $table->string('home_url_kh',250)->nullable();
            $table->text('img')->nullable();
            $table->integer('update_by')->nullable();
            $table->integer('status')->default(1);           
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('menuhome');
    }
}