<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutMenuTable extends Migration
{

    public function up()
    {
        Schema::create('about_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('h_title_en')->nullable();
            $table->string('h_title_kh')->nullable();
            $table->text('h_desc_en')->nullable();
            $table->text('h_desc_kh')->nullable();
            $table->text('h_thumbnail')->nullable();

            $table->string('v_title_en')->nullable();
            $table->string('v_title_kh')->nullable();
            $table->text('v_desc_en')->nullable();
            $table->text('v_desc_kh')->nullable();
            $table->text('v_thumbnail')->nullable();

            $table->string('o_title_en')->nullable();
            $table->string('o_title_kh')->nullable();
            $table->text('o_desc_en')->nullable();
            $table->text('o_desc_kh')->nullable();
            $table->text('o_thumbnail')->nullable();

            $table->string('m_title_en')->nullable();
            $table->string('m_title_kh')->nullable();
            $table->text('m_desc_en')->nullable();
            $table->text('m_desc_kh')->nullable();
            $table->text('m_thumbnail')->nullable();

            $table->string('p_title_en')->nullable();
            $table->string('p_title_kh')->nullable();
            $table->text('p_desc_en')->nullable();
            $table->text('p_desc_kh')->nullable();
            $table->text('p_thumbnail')->nullable();

            $table->string('vdo_title_en')->nullable();
            $table->string('vdo_title_kh')->nullable();
            $table->text('vdo_desc_en')->nullable();
            $table->text('vdo_desc_kh')->nullable();
            $table->text('vdo_location')->nullable();

            $table->string('n_title_en')->nullable();
            $table->string('n_title_kh')->nullable();
            $table->text('n_desc_en')->nullable();
            $table->text('n_desc_kh')->nullable();
            $table->text('n_thumbnail')->nullable();
           
            $table->integer('update_by')->nullable();
            $table->integer('status')->default(1);           
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('about_menu');
    }
}