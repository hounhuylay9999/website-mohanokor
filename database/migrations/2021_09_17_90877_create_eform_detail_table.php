<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEformDetailTable extends Migration
{
    public function up()
    {
        Schema::create('e_form_detail', function (Blueprint $table) {
            $table->id();
            $table->string('position_apply_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->text('e_mail');
            $table->text('phone');
            $table->text('file');
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('e_form_detail');
    }
}