<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralTable extends Migration
{

    public function up()
    {
        Schema::create('general', function (Blueprint $table) {
            $table->id();
            $table->string('header_logo',250)->nullable();
            $table->string('favicon',250)->nullable();
            $table->string('header_phone',250)->nullable();
            $table->string('header_email',250)->nullable();
            $table->string('header_fb_url',250)->nullable();
            $table->string('header_yt_url',250)->nullable();
           

            $table->string('footer_head',250)->nullable();
            $table->string('footer_head_desc',250)->nullable();
            
            $table->string('footer_branch',250)->nullable();
            $table->string('footer_branch_desc',250)->nullable();

            $table->string('footer_call',250)->nullable();
            $table->string('footer_call_1',250)->nullable();
            $table->string('footer_call_2',250)->nullable();

            $table->string('footer_email',250)->nullable();
            $table->string('footer_email_url',250)->nullable();
            $table->string('footer_web_url',250)->nullable();

            $table->string('footer_copyright',250)->nullable();
            $table->string('footer_title',250)->nullable();
            $table->string('footer_logo',250)->nullable();
            $table->string('product_cover',250)->nullable();
            $table->string('about_cover',250)->nullable();
            $table->string('team_cover',250)->nullable();
            $table->string('news_cover',250)->nullable();

            $table->text('meta_keyword')->nullable();
            $table->text('meta_description')->nullable();
            
            $table->string('lang',9)->nullable();
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('general');
    }
}