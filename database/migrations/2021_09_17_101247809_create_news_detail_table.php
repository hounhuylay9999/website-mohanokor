<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsDetailTable extends Migration
{
    public function up()
    {
        Schema::create('news_detail', function (Blueprint $table) {
            $table->id();
            $table->string('title_en');
            $table->string('title_kh');
            $table->text('short_desc_en');
            $table->text('short_desc_kh');
            $table->text('event_date');
            $table->text('news_detail_thumnail');
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('news_detail');
    }
}