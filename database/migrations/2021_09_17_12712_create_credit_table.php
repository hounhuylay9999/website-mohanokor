<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditTable extends Migration
{

    public function up()
    {
        Schema::create('credit', function (Blueprint $table) {
            $table->id();
            $table->string('title_en');
            $table->string('title_kh');
            $table->string('short_title_en');
            $table->string('short_title_kh');
            $table->string('short_desc_en');
            $table->string('short_desc_kh');
            $table->text('credit_thumnail');
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('credit');
    }
}