<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_profile')->insert([
            array(
                'name_en'       => 'Core Back End',
                'name_kh'       => 'Core Back End',
                'email'         => 'admin@admin.com',
                'company_object'=> '{"CompanyCode": "100"}',
                'phone'         => '093322910',
                'address'       => '#80A, st 25 Borey Chomka Dong New World, Phnom Penh, Cambodia.',
                'profile'       => 'delivery.png',
                'created_at' => Carbon::now(),
            ),
         ]);
    }
}