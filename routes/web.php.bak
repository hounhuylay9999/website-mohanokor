<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale){
      Session::put('locale', $locale);
      return redirect()->back();
});

//front-end
Route::GET('/', [App\Http\Controllers\frontend\HomePageController::class, 'HomePage'])->name('homepage.index');
Route::GET('/straight-tube', [App\Http\Controllers\frontend\HomePageController::class, 'StraightTube'])->name('straight-tube.index');
Route::GET('/pancake-coil', [App\Http\Controllers\frontend\HomePageController::class, 'PancakeCoil'])->name('pancake-coil.index');
Route::GET('/copper-fitting', [App\Http\Controllers\frontend\HomePageController::class, 'CopperFitting'])->name('copper-fitting.index');
Route::GET('/production-strength', [App\Http\Controllers\frontend\HomePageController::class, 'ProductionStrength'])->name('production-strength.index');
Route::GET('/media', [App\Http\Controllers\frontend\HomePageController::class, 'Media'])->name('media.index');
Route::GET('/media/detail/{uuid}', [App\Http\Controllers\frontend\HomePageController::class, 'MediaDetail'])->name('mediaDetail.index');

Route::GET('/about', [App\Http\Controllers\frontend\HomePageController::class, 'About'])->name('about.index');
Route::GET('/contact', [App\Http\Controllers\frontend\HomePageController::class, 'Contact'])->name('contact.index');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['prefix'=>'admin','middleware' => ['auth']], function() {
    //Manage Users
    Route::GET('/users.html', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
    Route::GET('users/ajax/list', [App\Http\Controllers\UserController::class, 'getUserList'])->name('users.getUserList');
    Route::GET('/users/addnew.html', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
    Route::POST('/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');
    Route::DELETE('/users/delete/{id}.html', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');
    Route::GET('/users/modify/{id}.html', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
    Route::PATCH('/users/updated/{id}.html', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');

     //Manage Roles
     Route::GET('/role.html', [App\Http\Controllers\RoleController::class, 'index'])->name('roles.index');
     Route::GET('role/ajax/list', [App\Http\Controllers\RoleController::class, 'getRoleList'])->name('roles.getRoleList');
     Route::GET('/role/addnew.html', [App\Http\Controllers\RoleController::class, 'create'])->name('roles.create');
     Route::POST('/role/store', [App\Http\Controllers\RoleController::class, 'store'])->name('roles.store');
     Route::DELETE('/role/delete/{id}.html', [App\Http\Controllers\RoleController::class, 'destroy'])->name('roles.destroy');
     Route::GET('/role/modify/{id}.html', [App\Http\Controllers\RoleController::class, 'edit'])->name('roles.edit');
     Route::PATCH('/role/updated/{id}.html', [App\Http\Controllers\RoleController::class, 'update'])->name('roles.update');

      //Manage Profile
      Route::GET('your-profile.html', [App\Http\Controllers\ProfileController::class, 'yourProfile'])->name('profile.yourProfile');
      Route::POST('your-profile/modify/{id}.html', [App\Http\Controllers\ProfileController::class, 'yourProfileUpdate'])->name('profile.yourProfileUpdate');
      Route::GET('company-profile.html', [App\Http\Controllers\ProfileController::class, 'companyProfile'])->name('profile.companyProfile');
      Route::POST('company-profile/modify/{id}.html', [App\Http\Controllers\ProfileController::class, 'companyProfileUpdate'])->name('profile.companyProfileUpdate');

      //Manage Permission
      Route::GET('/permission.html', [App\Http\Controllers\PermissionController::class, 'index'])->name('permission.index');
      Route::GET('permission/ajax/list', [App\Http\Controllers\PermissionController::class, 'getPermissionList'])->name('permission.getPermissionList');
   
});


Route::group(['prefix'=>'tle','middleware' => ['auth']], function() {
      //General
      Route::GET('/general.html', [App\Http\Controllers\GeneralController::class, 'index'])->name('general.index');
      Route::POST('/general/modify.html', [App\Http\Controllers\GeneralController::class, 'store'])->name('general.store');

       //Straight Tube
       Route::GET('/straight-tube.html', [App\Http\Controllers\StraightTubeController::class, 'index'])->name('straightTube.index');
       Route::POST('/straight-tube/modify', [App\Http\Controllers\StraightTubeController::class, 'store'])->name('straightTube.store');
       Route::POST('/straight-tube/features', [App\Http\Controllers\StraightTubeController::class, 'features'])->name('straightTube.features');
       Route::GET('/straight-tube/features/delete', [App\Http\Controllers\StraightTubeController::class, 'deleteFeature'])->name('straightTubeFeatures.deleteFeature');

       
       /**pancake-coil */
       Route::GET('/pancake-coil.html', [App\Http\Controllers\PancakeCoilController::class, 'index'])->name('pancakeCoil.index');
       Route::GET('/pancake-coil/create', [App\Http\Controllers\PancakeCoilController::class, 'index'])->name('pancakeCoil.create');
       Route::POST('/pancake-coil/modify.html', [App\Http\Controllers\PancakeCoilController::class, 'store'])->name('pancakeCoil.store');
       Route::POST('/pancake-coil/features', [App\Http\Controllers\PancakeCoilController::class, 'features'])->name('pancakeCoil.features');
       Route::GET('/pancake-coil/features/delete', [App\Http\Controllers\PancakeCoilController::class, 'deleteFeature'])->name('pancakeCoil.deleteFeature');

       //Contact
      Route::GET('/contact.html', [App\Http\Controllers\ContactController::class, 'index'])->name('contact.index');
      Route::GET('/create', [App\Http\Controllers\ContactController::class, 'create'])->name('contact.create');
      Route::POST('contact/store', [App\Http\Controllers\ContactController::class, 'store'])->name('contact.store');
      Route::GET('/listContactResource',[App\Http\Controllers\ContactController::class, 'listContactResource'])->name('contact.listContactResource');

     
       /**copper-fitting */
       Route::GET('/copperfitting.html', [App\Http\Controllers\CopperFittingController::class, 'index'])->name('copperfitting.index');
       Route::GET('/copperfitting/create', [App\Http\Controllers\CopperFittingController::class, 'index'])->name('copperfitting.create');
       Route::POST('/copperfitting/store', [App\Http\Controllers\CopperFittingController::class, 'store'])->name('copperfitting.store');
       Route::POST('/copperfitting/features', [App\Http\Controllers\CopperFittingController::class, 'features'])->name('copperfitting.features');
       Route::GET('/copperfitting/features/delete', [App\Http\Controllers\CopperFittingController::class, 'deleteFeature'])->name('copperfitting.deleteFeature');

     
       /**Production Strength*/
       Route::GET('/productionstrength.html', [App\Http\Controllers\productionstrengthController::class, 'index'])->name('productionstrength.index');
       Route::GET('/productionstrength/create', [App\Http\Controllers\productionstrengthController::class, 'index'])->name('productionstrength.create');
       Route::POST('/productionstrength/store', [App\Http\Controllers\productionstrengthController::class, 'store'])->name('productionstrength.store');
       Route::GET('/listProductResource',[App\Http\Controllers\productionstrengthController::class, 'listProductResource'])->name('productionstrength.listProductResource');
       Route::DELETE('/productionstrength/delete/{id}', [App\Http\Controllers\productionstrengthController::class, 'destroy'])->name('productionstrength.destroy');
       Route::GET('/productionstrength/edit/{id}', [App\Http\Controllers\productionstrengthController::class, 'edit'])->name('productionstrength.edit');
       Route::PATCH('/productionstrength/update/{id}', [App\Http\Controllers\productionstrengthController::class, 'update'])->name('productionstrength.update');
       
       
       
       /**media */
       Route::GET('/medias.html', [App\Http\Controllers\MediaController::class, 'index'])->name('medias.index');
       Route::GET('/medias/create', [App\Http\Controllers\MediaController::class, 'create'])->name('media.create');
       Route::POST('/medias/store', [App\Http\Controllers\MediaController::class, 'store'])->name('media.store');
       Route::GET('/medias/list', [App\Http\Controllers\MediaController::class, 'medias_list'])->name('medias.list');
       Route::GET('/medias/edit/{id}', [App\Http\Controllers\MediaController::class, 'edit'])->name('medias.edit');
       Route::PATCH('/medias/update/{id}', [App\Http\Controllers\MediaController::class, 'update'])->name('medias.update');
       Route::DELETE('/medias/delete/{id}', [App\Http\Controllers\MediaController::class, 'destroy'])->name('medias.destroy');

 
      //Hero banner
      Route::GET('/herobanner.html', [App\Http\Controllers\HerobannerController::class, 'index'])->name('herobanner.index');
      Route::GET('/herobanner/create.html', [App\Http\Controllers\HerobannerController::class, 'create'])->name('herobanner.create');
      Route::POST('/herobanner/store.html', [App\Http\Controllers\HerobannerController::class, 'store'])->name('herobanner.store');
      Route::GET('/herobanner/ajxlist.html', [App\Http\Controllers\HerobannerController::class, 'getBannerList'])->name('herobanner.getBannerList');
      Route::GET('/herobanner/edit/{id}.html', [App\Http\Controllers\HerobannerController::class, 'edit'])->name('herobanner.edit');
      Route::PATCH('/herobanner/update/{id}.html', [App\Http\Controllers\HerobannerController::class, 'update'])->name('herobanner.update');
      Route::DELETE('/herobanner/delete/{id}', [App\Http\Controllers\HerobannerController::class, 'destroy'])->name('herobanner.destroy');
});