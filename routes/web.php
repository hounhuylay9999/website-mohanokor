<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale){
      Session::put('locale', $locale);
      return redirect()->back();
});

//front-end
Route::GET('/', [App\Http\Controllers\frontend\HomePageController::class, 'HomePage'])->name('homepage.index');

Route::GET('/organizations', [App\Http\Controllers\frontend\HomePageController::class, 'Organization'])->name('organizations.index');
Route::GET('/managements', [App\Http\Controllers\frontend\HomePageController::class, 'Management'])->name('managements.index');
Route::GET('/historys', [App\Http\Controllers\frontend\HomePageController::class, 'History'])->name('historys.index');
Route::GET('/photos', [App\Http\Controllers\frontend\HomePageController::class, 'Photo'])->name('photos.index');
Route::GET('/annual_reports', [App\Http\Controllers\frontend\HomePageController::class, 'AnnualReport'])->name('annual_reports.index');
Route::GET('/videos', [App\Http\Controllers\frontend\HomePageController::class, 'video'])->name('videos.index');
Route::GET('/news_event', [App\Http\Controllers\frontend\HomePageController::class, 'News_Event'])->name('news_event.index');
Route::GET('/job_posts', [App\Http\Controllers\frontend\HomePageController::class, 'JobPost'])->name('job_posts.index');
Route::GET('/e_forms', [App\Http\Controllers\frontend\HomePageController::class, 'Eform'])->name('e_forms.index');
Route::GET('/complaints', [App\Http\Controllers\frontend\HomePageController::class, 'Complaint'])->name('complaints.index');
Route::GET('/product_show', [App\Http\Controllers\frontend\HomePageController::class, 'product_show'])->name('product_show.index');
Route::GET('/missions', [App\Http\Controllers\frontend\HomePageController::class, 'Mission'])->name('missions.index');
Route::GET('/news_feature', [App\Http\Controllers\frontend\HomePageController::class, 'NewFeature'])->name('news_feature.index');
Route::GET('/branches_networks', [App\Http\Controllers\frontend\HomePageController::class, 'BranchesNetwork'])->name('branches_networks.index');
Route::GET('/management_role', [App\Http\Controllers\frontend\HomePageController::class, 'Management_Role'])->name('management_role.index');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

      Route::group(['prefix'=>'admin','middleware' => ['auth']], function() {
      //Manage Users
      Route::GET('/users.html', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
      Route::GET('users/ajax/list', [App\Http\Controllers\UserController::class, 'getUserList'])->name('users.getUserList');
      Route::GET('/users/addnew.html', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
      Route::POST('/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');
      Route::DELETE('/users/delete/{id}.html', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');
      Route::GET('/users/modify/{id}.html', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
      Route::PATCH('/users/updated/{id}.html', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');

      //Manage Roles
      Route::GET('/role.html', [App\Http\Controllers\RoleController::class, 'index'])->name('roles.index');
      Route::GET('role/ajax/list', [App\Http\Controllers\RoleController::class, 'getRoleList'])->name('roles.getRoleList');
      Route::GET('/role/addnew.html', [App\Http\Controllers\RoleController::class, 'create'])->name('roles.create');
      Route::POST('/role/store', [App\Http\Controllers\RoleController::class, 'store'])->name('roles.store');
      Route::DELETE('/role/delete/{id}.html', [App\Http\Controllers\RoleController::class, 'destroy'])->name('roles.destroy');
      Route::GET('/role/modify/{id}.html', [App\Http\Controllers\RoleController::class, 'edit'])->name('roles.edit');
      Route::PATCH('/role/updated/{id}.html', [App\Http\Controllers\RoleController::class, 'update'])->name('roles.update');

      //Manage Profile
      Route::GET('your-profile.html', [App\Http\Controllers\ProfileController::class, 'yourProfile'])->name('profile.yourProfile');
      Route::POST('your-profile/modify/{id}.html', [App\Http\Controllers\ProfileController::class, 'yourProfileUpdate'])->name('profile.yourProfileUpdate');
      Route::GET('company-profile.html', [App\Http\Controllers\ProfileController::class, 'companyProfile'])->name('profile.companyProfile');
      Route::POST('company-profile/modify/{id}.html', [App\Http\Controllers\ProfileController::class, 'companyProfileUpdate'])->name('profile.companyProfileUpdate');

      //Manage Permission
      Route::GET('/permission.html', [App\Http\Controllers\PermissionController::class, 'index'])->name('permission.index');
      Route::GET('permission/ajax/list', [App\Http\Controllers\PermissionController::class, 'getPermissionList'])->name('permission.getPermissionList');

      //General
      Route::GET('/general.html', [App\Http\Controllers\GeneralController::class, 'index'])->name('general.index');
      Route::POST('/general/modify.html', [App\Http\Controllers\GeneralController::class, 'store'])->name('general.store');
   
});

Route::group(['prefix'=>'mohanokor','middleware' => ['auth']], function() {
      
      //homepage
      Route::GET('/menuhome.html', [App\Http\Controllers\MenuHomeController::class, 'index'])->name('menuhome.index');
      Route::GET('/menuhome/create', [App\Http\Controllers\MenuHomeController::class, 'index'])->name('menuhome.create');
      Route::POST('/menuhome/modify.html', [App\Http\Controllers\MenuHomeController::class, 'store'])->name('menuhome.store');
      Route::POST('/menuhome/features', [App\Http\Controllers\MenuHomeController::class, 'features'])->name('menuhome.features');
      Route::GET('/menuhome/features/delete', [App\Http\Controllers\MenuHomeController::class, 'deleteFeature'])->name('menuhome.deleteFeature');

      //aboutus
      Route::GET('/aboutus.html', [App\Http\Controllers\AboutUsController::class, 'index'])->name('aboutus.index');
      Route::GET('/aboutus/create', [App\Http\Controllers\AboutUsController::class, 'index'])->name('aboutus.create');
      Route::POST('/aboutus/modify.html', [App\Http\Controllers\AboutUsController::class, 'store'])->name('aboutus.store');
      Route::POST('/aboutus/features', [App\Http\Controllers\AboutUsController::class, 'features'])->name('aboutus.features');
      Route::GET('/aboutus/features/delete', [App\Http\Controllers\AboutUsController::class, 'deleteFeature'])->name('aboutus.deleteFeature');

      //Hero banner
      Route::GET('/herobanner.html', [App\Http\Controllers\HerobannerController::class, 'index'])->name('herobanner.index');
      Route::GET('/herobanner/create.html', [App\Http\Controllers\HerobannerController::class, 'create'])->name('herobanner.create');
      Route::POST('/herobanner/store.html', [App\Http\Controllers\HerobannerController::class, 'store'])->name('herobanner.store');
      Route::GET('/herobanner/ajxlist.html', [App\Http\Controllers\HerobannerController::class, 'getBannerList'])->name('herobanner.getBannerList');
      Route::GET('/herobanner/edit/{id}.html', [App\Http\Controllers\HerobannerController::class, 'edit'])->name('herobanner.edit');
      Route::PATCH('/herobanner/update/{id}.html', [App\Http\Controllers\HerobannerController::class, 'update'])->name('herobanner.update');
      Route::DELETE('/herobanner/delete/{id}', [App\Http\Controllers\HerobannerController::class, 'destroy'])->name('herobanner.destroy');

      //Partner
      Route::GET('/partner.html', [App\Http\Controllers\PartnerController::class, 'index'])->name('partner.index');
      Route::GET('/partner/create.html', [App\Http\Controllers\PartnerController::class, 'create'])->name('partner.create');
      Route::POST('/partner/store.html', [App\Http\Controllers\PartnerController::class, 'store'])->name('partner.store');
      Route::GET('/partner/ajxlist.html', [App\Http\Controllers\PartnerController::class, 'getGetPartnerList'])->name('partner.getGetPartnerList');
      Route::GET('/partner/edit/{id}.html', [App\Http\Controllers\PartnerController::class, 'edit'])->name('partner.edit');
      Route::PATCH('/partner/update/{id}.html', [App\Http\Controllers\PartnerController::class, 'update'])->name('partner.update');
      Route::DELETE('/partner/delete/{id}', [App\Http\Controllers\PartnerController::class, 'destroy'])->name('partner.destroy');

      //history detail
      Route::GET('/history_detail.html', [App\Http\Controllers\HistoryDetailController::class, 'index'])->name('history_detail.index');
      Route::GET('/history_detail/create.html', [App\Http\Controllers\HistoryDetailController::class, 'create'])->name('history_detail.create');
      Route::POST('/history_detail/store.html', [App\Http\Controllers\HistoryDetailController::class, 'store'])->name('history_detail.store');
      Route::GET('/history_detail/ajxlist.html', [App\Http\Controllers\HistoryDetailController::class, 'getHistoryDetailList'])->name('history_detail.getHistoryDetailList');
      Route::GET('/history_detail/edit/{id}.html', [App\Http\Controllers\HistoryDetailController::class, 'edit'])->name('history_detail.edit');
      Route::PATCH('/history_detail/update/{id}.html', [App\Http\Controllers\HistoryDetailController::class, 'update'])->name('history_detail.update');
      Route::DELETE('/history_detail/delete/{id}', [App\Http\Controllers\HistoryDetailController::class, 'destroy'])->name('history_detail.destroy');

      //history
      Route::GET('/history.html', [App\Http\Controllers\HistoryController::class, 'index'])->name('history.index');
      Route::GET('/history/create.html', [App\Http\Controllers\HistoryController::class, 'create'])->name('history.create');
      Route::POST('/history/store.html', [App\Http\Controllers\HistoryController::class, 'store'])->name('history.store');
      Route::GET('/history/ajxlist.html', [App\Http\Controllers\HistoryController::class, 'getHistoryList'])->name('history.getHistoryList');
      Route::GET('/history/edit/{id}.html', [App\Http\Controllers\HistoryController::class, 'edit'])->name('history.edit');
      Route::PATCH('/history/update/{id}.html', [App\Http\Controllers\HistoryController::class, 'update'])->name('history.update');
      Route::DELETE('/history/delete/{id}', [App\Http\Controllers\HistoryController::class, 'destroy'])->name('history.destroy');

      //organization
      Route::GET('/organization.html', [App\Http\Controllers\OrganizationController::class, 'index'])->name('organization.index');
      Route::GET('/organization/create.html', [App\Http\Controllers\OrganizationController::class, 'create'])->name('organization.create');
      Route::POST('/organization/store.html', [App\Http\Controllers\OrganizationController::class, 'store'])->name('organization.store');
      Route::GET('/organization/ajxlist.html', [App\Http\Controllers\OrganizationController::class, 'getOrganizationList'])->name('organization.getOrganizationList');
      Route::GET('/organization/edit/{id}.html', [App\Http\Controllers\OrganizationController::class, 'edit'])->name('organization.edit');
      Route::PATCH('/organization/update/{id}.html', [App\Http\Controllers\OrganizationController::class, 'update'])->name('organization.update');
      Route::DELETE('/organization/delete/{id}', [App\Http\Controllers\OrganizationController::class, 'destroy'])->name('organization.destroy');

      //organization detail
      Route::GET('/organization_detail.html', [App\Http\Controllers\OrganizationDetailController::class, 'index'])->name('organization_detail.index');
      Route::GET('/organization_detail/create.html', [App\Http\Controllers\OrganizationDetailController::class, 'create'])->name('organization_detail.create');
      Route::POST('/organization_detail/store.html', [App\Http\Controllers\OrganizationDetailController::class, 'store'])->name('organization_detail.store');
      Route::GET('/organization_detail/ajxlist.html', [App\Http\Controllers\OrganizationDetailController::class, 'getOrganizationDetailList'])->name('organization_detail.getOrganizationDetailList');
      Route::GET('/organization_detail/edit/{id}.html', [App\Http\Controllers\OrganizationDetailController::class, 'edit'])->name('organization_detail.edit');
      Route::PATCH('/organization_detail/update/{id}.html', [App\Http\Controllers\OrganizationDetailController::class, 'update'])->name('organization_detail.update');
      Route::DELETE('/organization_detail/delete/{id}', [App\Http\Controllers\OrganizationDetailController::class, 'destroy'])->name('organization_detail.destroy');

      //management
      Route::GET('/management.html', [App\Http\Controllers\ManagementController::class, 'index'])->name('management.index');
      Route::GET('/management/create.html', [App\Http\Controllers\ManagementController::class, 'create'])->name('management.create');
      Route::POST('/management/store.html', [App\Http\Controllers\ManagementController::class, 'store'])->name('management.store');
      Route::GET('/management/ajxlist.html', [App\Http\Controllers\ManagementController::class, 'getManagementList'])->name('management.getManagementList');
      Route::GET('/management/edit/{id}.html', [App\Http\Controllers\ManagementController::class, 'edit'])->name('management.edit');
      Route::PATCH('/management/update/{id}.html', [App\Http\Controllers\ManagementController::class, 'update'])->name('management.update');
      Route::DELETE('/management/delete/{id}', [App\Http\Controllers\ManagementController::class, 'destroy'])->name('management.destroy');

      //management_detail
      Route::GET('/management_detail.html', [App\Http\Controllers\ManagementDetailController::class, 'index'])->name('management_detail.index');
      Route::GET('/management_detail/create.html', [App\Http\Controllers\ManagementDetailController::class, 'create'])->name('management_detail.create');
      Route::POST('/management_detail/store.html', [App\Http\Controllers\ManagementDetailController::class, 'store'])->name('management_detail.store');
      Route::GET('/management_detail/ajxlist.html', [App\Http\Controllers\ManagementDetailController::class, 'getManagementDetailList'])->name('management_detail.getManagementDetailList');
      Route::GET('/management_detail/edit/{id}.html', [App\Http\Controllers\ManagementDetailController::class, 'edit'])->name('management_detail.edit');
      Route::PATCH('/management_detail/update/{id}.html', [App\Http\Controllers\ManagementDetailController::class, 'update'])->name('management_detail.update');
      Route::DELETE('/management_detail/delete/{id}', [App\Http\Controllers\ManagementDetailController::class, 'destroy'])->name('management_detail.destroy');

      //photo
      Route::GET('/photo.html', [App\Http\Controllers\PhotoController::class, 'index'])->name('photo.index');
      Route::GET('/photo/create.html', [App\Http\Controllers\PhotoController::class, 'create'])->name('photo.create');
      Route::POST('/photo/store.html', [App\Http\Controllers\PhotoController::class, 'store'])->name('photo.store');
      Route::GET('/photo/ajxlist.html', [App\Http\Controllers\PhotoController::class, 'getPhotoList'])->name('photo.getPhotoList');
      Route::GET('/photo/edit/{id}.html', [App\Http\Controllers\PhotoController::class, 'edit'])->name('photo.edit');
      Route::PATCH('/photo/update/{id}.html', [App\Http\Controllers\PhotoController::class, 'update'])->name('photo.update');
      Route::DELETE('/photo/delete/{id}', [App\Http\Controllers\PhotoController::class, 'destroy'])->name('photo.destroy');

      //photo detail
      Route::GET('/photo_detail.html', [App\Http\Controllers\PhotoDetailController::class, 'index'])->name('photo_detail.index');
      Route::GET('/photo_detail/create.html', [App\Http\Controllers\PhotoDetailController::class, 'create'])->name('photo_detail.create');
      Route::POST('/photo_detail/store.html', [App\Http\Controllers\PhotoDetailController::class, 'store'])->name('photo_detail.store');
      Route::GET('/photo_detail/ajxlist.html', [App\Http\Controllers\PhotoDetailController::class, 'getPhotoDetailList'])->name('photo_detail.getPhotoDetailList');
      Route::GET('/photo_detail/edit/{id}.html', [App\Http\Controllers\PhotoDetailController::class, 'edit'])->name('photo_detail.edit');
      Route::PATCH('/photo_detail/update/{id}.html', [App\Http\Controllers\PhotoDetailController::class, 'update'])->name('photo_detail.update');
      Route::DELETE('/photo_detail/delete/{id}', [App\Http\Controllers\PhotoDetailController::class, 'destroy'])->name('photo_detail.destroy');
      
      
      //video
      Route::GET('/video.html', [App\Http\Controllers\VideoController::class, 'index'])->name('video.index');
      Route::GET('/video/create.html', [App\Http\Controllers\VideoController::class, 'create'])->name('video.create');
      Route::POST('/video/store.html', [App\Http\Controllers\VideoController::class, 'store'])->name('video.store');
      Route::GET('/video/ajxlist.html', [App\Http\Controllers\VideoController::class, 'getVideoList'])->name('video.getVideoList');
      Route::GET('/video/edit/{id}.html', [App\Http\Controllers\VideoController::class, 'edit'])->name('video.edit');
      Route::PATCH('/video/update/{id}.html', [App\Http\Controllers\VideoController::class, 'update'])->name('video.update');
      Route::DELETE('/video/delete/{id}', [App\Http\Controllers\VideoController::class, 'destroy'])->name('video.destroy');

      //video detail
      Route::GET('/video_detail.html', [App\Http\Controllers\VideoDetailController::class, 'index'])->name('video_detail.index');
      Route::GET('/video_detail/create.html', [App\Http\Controllers\VideoDetailController::class, 'create'])->name('video_detail.create');
      Route::POST('/video_detail/store.html', [App\Http\Controllers\VideoDetailController::class, 'store'])->name('video_detail.store');
      Route::GET('/video_detail/ajxlist.html', [App\Http\Controllers\VideoDetailController::class, 'getVideoDetailList'])->name('video_detail.getVideoDetailList');
      Route::GET('/video_detail/edit/{id}.html', [App\Http\Controllers\VideoDetailController::class, 'edit'])->name('video_detail.edit');
      Route::PATCH('/video_detail/update/{id}.html', [App\Http\Controllers\VideoDetailController::class, 'update'])->name('video_detail.update');
      Route::DELETE('/video_detail/delete/{id}', [App\Http\Controllers\VideoDetailController::class, 'destroy'])->name('video_detail.destroy');

      //news
      Route::GET('/news.html', [App\Http\Controllers\NewsController::class, 'index'])->name('news.index');
      Route::GET('/news/create.html', [App\Http\Controllers\NewsController::class, 'create'])->name('news.create');
      Route::POST('/news/store.html', [App\Http\Controllers\NewsController::class, 'store'])->name('news.store');
      Route::GET('/news/ajxlist.html', [App\Http\Controllers\NewsController::class, 'getNewsList'])->name('news.getNewsList');
      Route::GET('/news/edit/{id}.html', [App\Http\Controllers\NewsController::class, 'edit'])->name('news.edit');
      Route::PATCH('/news/update/{id}.html', [App\Http\Controllers\NewsController::class, 'update'])->name('news.update');
      Route::DELETE('/news/delete/{id}', [App\Http\Controllers\NewsController::class, 'destroy'])->name('news.destroy');

      //news
      Route::GET('/news_detail.html', [App\Http\Controllers\NewsDetailController::class, 'index'])->name('news_detail.index');
      Route::GET('/news_detail/create.html', [App\Http\Controllers\NewsDetailController::class, 'create'])->name('news_detail.create');
      Route::POST('/news_detail/store.html', [App\Http\Controllers\NewsDetailController::class, 'store'])->name('news_detail.store');
      Route::GET('/news_detail/ajxlist.html', [App\Http\Controllers\NewsDetailController::class, 'getNewsDetailList'])->name('news_detail.getNewsDetailList');
      Route::GET('/news_detail/edit/{id}.html', [App\Http\Controllers\NewsDetailController::class, 'edit'])->name('news_detail.edit');
      Route::PATCH('/news_detail/update/{id}.html', [App\Http\Controllers\NewsDetailController::class, 'update'])->name('news_detail.update');
      Route::DELETE('/news_detail/delete/{id}', [App\Http\Controllers\NewsDetailController::class, 'destroy'])->name('news_detail.destroy');

      //credit
      Route::GET('/credit.html', [App\Http\Controllers\CreditController::class, 'index'])->name('credit.index');
      Route::GET('/credit/create.html', [App\Http\Controllers\CreditController::class, 'create'])->name('credit.create');
      Route::POST('/credit/store.html', [App\Http\Controllers\CreditController::class, 'store'])->name('credit.store');
      Route::GET('/credit/ajxlist.html', [App\Http\Controllers\CreditController::class, 'getCreditList'])->name('credit.getCreditList');
      Route::GET('/credit/edit/{id}.html', [App\Http\Controllers\CreditController::class, 'edit'])->name('credit.edit');
      Route::PATCH('/credit/update/{id}.html', [App\Http\Controllers\CreditController::class, 'update'])->name('credit.update');
      Route::DELETE('/credit/delete/{id}', [App\Http\Controllers\CreditController::class, 'destroy'])->name('credit.destroy');

      //deposit
      Route::GET('/deposit.html', [App\Http\Controllers\DepositController::class, 'index'])->name('deposit.index');
      Route::GET('/deposit/create.html', [App\Http\Controllers\DepositController::class, 'create'])->name('deposit.create');
      Route::POST('/deposit/store.html', [App\Http\Controllers\DepositController::class, 'store'])->name('deposit.store');
      Route::GET('/deposit/ajxlist.html', [App\Http\Controllers\DepositController::class, 'getDepositList'])->name('deposit.getDepositList');
      Route::GET('/deposit/edit/{id}.html', [App\Http\Controllers\DepositController::class, 'edit'])->name('deposit.edit');
      Route::PATCH('/deposit/update/{id}.html', [App\Http\Controllers\DepositController::class, 'update'])->name('deposit.update');
      Route::DELETE('/deposit/delete/{id}', [App\Http\Controllers\DepositController::class, 'destroy'])->name('deposit.destroy');

      //atm
      Route::GET('/atm.html', [App\Http\Controllers\AtmController::class, 'index'])->name('atm.index');
      Route::GET('/atm/create.html', [App\Http\Controllers\AtmController::class, 'create'])->name('atm.create');
      Route::POST('/atm/store.html', [App\Http\Controllers\AtmController::class, 'store'])->name('atm.store');
      Route::GET('/atm/ajxlist.html', [App\Http\Controllers\AtmController::class, 'getAtmList'])->name('atm.getAtmList');
      Route::GET('/atm/edit/{id}.html', [App\Http\Controllers\AtmController::class, 'edit'])->name('atm.edit');
      Route::PATCH('/atm/update/{id}.html', [App\Http\Controllers\AtmController::class, 'update'])->name('atm.update');
      Route::DELETE('/atm/delete/{id}', [App\Http\Controllers\AtmController::class, 'destroy'])->name('atm.destroy');

      //mobile
      Route::GET('/mobile.html', [App\Http\Controllers\MobileController::class, 'index'])->name('mobile.index');
      Route::GET('/mobile/create.html', [App\Http\Controllers\MobileController::class, 'create'])->name('mobile.create');
      Route::POST('/mobile/store.html', [App\Http\Controllers\MobileController::class, 'store'])->name('mobile.store');
      Route::GET('/mobile/ajxlist.html', [App\Http\Controllers\MobileController::class, 'getMobileList'])->name('mobile.getMobileList');
      Route::GET('/mobile/edit/{id}.html', [App\Http\Controllers\MobileController::class, 'edit'])->name('mobile.edit');
      Route::PATCH('/mobile/update/{id}.html', [App\Http\Controllers\MobileController::class, 'update'])->name('mobile.update');
      Route::DELETE('/mobile/delete/{id}', [App\Http\Controllers\MobileController::class, 'destroy'])->name('mobile.destroy');

      //payroll
      Route::GET('/payroll.html', [App\Http\Controllers\PayrollController::class, 'index'])->name('payroll.index');
      Route::GET('/payroll/create.html', [App\Http\Controllers\PayrollController::class, 'create'])->name('payroll.create');
      Route::POST('/payroll/store.html', [App\Http\Controllers\PayrollController::class, 'store'])->name('payroll.store');
      Route::GET('/payroll/ajxlist.html', [App\Http\Controllers\PayrollController::class, 'getPayrollList'])->name('payroll.getPayrollList');
      Route::GET('/payroll/edit/{id}.html', [App\Http\Controllers\PayrollController::class, 'edit'])->name('payroll.edit');
      Route::PATCH('/payroll/update/{id}.html', [App\Http\Controllers\PayrollController::class, 'update'])->name('payroll.update');
      Route::DELETE('/payroll/delete/{id}', [App\Http\Controllers\PayrollController::class, 'destroy'])->name('payroll.destroy');

      //branch network
      Route::GET('/branch.html', [App\Http\Controllers\BranchController::class, 'index'])->name('branch.index');
      Route::GET('/branch/create.html', [App\Http\Controllers\BranchController::class, 'create'])->name('branch.create');
      Route::POST('/branch/store.html', [App\Http\Controllers\BranchController::class, 'store'])->name('branch.store');
      Route::GET('/branch/ajxlist.html', [App\Http\Controllers\BranchController::class, 'getBranchList'])->name('branch.getBranchList');
      Route::GET('/branch/edit/{id}.html', [App\Http\Controllers\BranchController::class, 'edit'])->name('branch.edit');
      Route::PATCH('/branch/update/{id}.html', [App\Http\Controllers\BranchController::class, 'update'])->name('branch.update');
      Route::DELETE('/branch/delete/{id}', [App\Http\Controllers\BranchController::class, 'destroy'])->name('branch.destroy');

      //position
      Route::GET('/position.html', [App\Http\Controllers\PositionController::class, 'index'])->name('position.index');
      Route::GET('/position/create.html', [App\Http\Controllers\PositionController::class, 'create'])->name('position.create');
      Route::POST('/position/store.html', [App\Http\Controllers\PositionController::class, 'store'])->name('position.store');
      Route::GET('/position/ajxlist.html', [App\Http\Controllers\PositionController::class, 'getPositionList'])->name('position.getPositionList');
      Route::GET('/position/edit/{id}.html', [App\Http\Controllers\PositionController::class, 'edit'])->name('position.edit');
      Route::PATCH('/position/update/{id}.html', [App\Http\Controllers\PositionController::class, 'update'])->name('position.update');
      Route::DELETE('/position/delete/{id}', [App\Http\Controllers\PositionController::class, 'destroy'])->name('position.destroy');

      //location
      Route::GET('/location.html', [App\Http\Controllers\LocationController::class, 'index'])->name('location.index');
      Route::GET('/location/create.html', [App\Http\Controllers\LocationController::class, 'create'])->name('location.create');
      Route::POST('/location/store.html', [App\Http\Controllers\LocationController::class, 'store'])->name('location.store');
      Route::GET('/location/ajxlist.html', [App\Http\Controllers\LocationController::class, 'getLocationList'])->name('location.getLocationList');
      Route::GET('/location/edit/{id}.html', [App\Http\Controllers\LocationController::class, 'edit'])->name('location.edit');
      Route::PATCH('/location/update/{id}.html', [App\Http\Controllers\LocationController::class, 'update'])->name('location.update');
      Route::DELETE('/location/delete/{id}', [App\Http\Controllers\LocationController::class, 'destroy'])->name('location.destroy');

      //form job post
      Route::GET('/job_post.html', [App\Http\Controllers\JobPostController::class, 'index'])->name('job_post.index');
      Route::GET('/job_post/create.html', [App\Http\Controllers\JobPostController::class, 'create'])->name('job_post.create');
      Route::POST('/job_post/store.html', [App\Http\Controllers\JobPostController::class, 'store'])->name('job_post.store');
      Route::GET('/job_post/ajxlist.html', [App\Http\Controllers\JobPostController::class, 'getJobPostList'])->name('job_post.getJobPostList');
      Route::GET('/job_post/edit/{id}.html', [App\Http\Controllers\JobPostController::class, 'edit'])->name('job_post.edit');
      Route::PATCH('/job_post/update/{id}.html', [App\Http\Controllers\JobPostController::class, 'update'])->name('job_post.update');
      Route::DELETE('/job_post/delete/{id}', [App\Http\Controllers\JobPostController::class, 'destroy'])->name('job_post.destroy');

      //job title
      Route::GET('/job_title.html', [App\Http\Controllers\JobTitleController::class, 'index'])->name('job_title.index');
      Route::GET('/job_title/create.html', [App\Http\Controllers\JobTitleController::class, 'create'])->name('job_title.create');
      Route::POST('/job_title/store.html', [App\Http\Controllers\JobTitleController::class, 'store'])->name('job_title.store');
      Route::GET('/job_title/ajxlist.html', [App\Http\Controllers\JobTitleController::class, 'getJobTitleList'])->name('job_title.getJobTitleList');
      Route::GET('/job_title/edit/{id}.html', [App\Http\Controllers\JobTitleController::class, 'edit'])->name('job_title.edit');
      Route::PATCH('/job_title/update/{id}.html', [App\Http\Controllers\JobTitleController::class, 'update'])->name('job_title.update');
      Route::DELETE('/job_title/delete/{id}', [App\Http\Controllers\JobTitleController::class, 'destroy'])->name('job_title.destroy');

      //E-form
      Route::GET('/e_form.html', [App\Http\Controllers\EformController::class, 'index'])->name('e_form.index');
      Route::GET('/e_form/create.html', [App\Http\Controllers\EformController::class, 'create'])->name('e_form.create');
      Route::POST('/e_form/store.html', [App\Http\Controllers\EformController::class, 'store'])->name('e_form.store');
      Route::GET('/e_form/ajxlist.html', [App\Http\Controllers\EformController::class, 'getEformList'])->name('e_form.getEformList');
      Route::GET('/e_form/edit/{id}.html', [App\Http\Controllers\EformController::class, 'edit'])->name('e_form.edit');
      Route::PATCH('/e_form/update/{id}.html', [App\Http\Controllers\EformController::class, 'update'])->name('e_form.update');
      Route::DELETE('/e_form/delete/{id}', [App\Http\Controllers\EformController::class, 'destroy'])->name('e_form.destroy');

      //position apply
      Route::GET('/position_apply.html', [App\Http\Controllers\PositionApplyController::class, 'index'])->name('position_apply.index');
      Route::GET('/position_apply/create.html', [App\Http\Controllers\PositionApplyController::class, 'create'])->name('position_apply.create');
      Route::POST('/position_apply/store.html', [App\Http\Controllers\PositionApplyController::class, 'store'])->name('position_apply.store');
      Route::GET('/position_apply/ajxlist.html', [App\Http\Controllers\PositionApplyController::class, 'getPositionApplyList'])->name('position_apply.getPositionApplyList');
      Route::GET('/position_apply/edit/{id}.html', [App\Http\Controllers\PositionApplyController::class, 'edit'])->name('position_apply.edit');
      Route::PATCH('/position_apply/update/{id}.html', [App\Http\Controllers\PositionApplyController::class, 'update'])->name('position_apply.update');
      Route::DELETE('/position_apply/delete/{id}', [App\Http\Controllers\PositionApplyController::class, 'destroy'])->name('position_apply.destroy');

      //mission
      Route::GET('/mission.html', [App\Http\Controllers\MissionController::class, 'index'])->name('mission.index');
      Route::GET('/mission/create.html', [App\Http\Controllers\MissionController::class, 'create'])->name('mission.create');
      Route::POST('/mission/store.html', [App\Http\Controllers\MissionController::class, 'store'])->name('mission.store');
      Route::GET('/mission/ajxlist.html', [App\Http\Controllers\MissionController::class, 'getMissionList'])->name('mission.getMissionList');
      Route::GET('/mission/edit/{id}.html', [App\Http\Controllers\MissionController::class, 'edit'])->name('mission.edit');
      Route::PATCH('/mission/update/{id}.html', [App\Http\Controllers\MissionController::class, 'update'])->name('mission.update');
      Route::DELETE('/mission/delete/{id}', [App\Http\Controllers\MissionController::class, 'destroy'])->name('mission.destroy');

      //product service
      Route::GET('/product_service.html', [App\Http\Controllers\ProductionServiceController::class, 'index'])->name('product_service.index');
      Route::GET('/product_service/create.html', [App\Http\Controllers\ProductionServiceController::class, 'create'])->name('product_service.create');
      Route::POST('/product_service/store.html', [App\Http\Controllers\ProductionServiceController::class, 'store'])->name('product_service.store');
      Route::GET('/product_service/ajxlist.html', [App\Http\Controllers\ProductionServiceController::class, 'getProductServiceList'])->name('product_service.getProductServiceList');
      Route::GET('/product_service/edit/{id}.html', [App\Http\Controllers\ProductionServiceController::class, 'edit'])->name('product_service.edit');
      Route::PATCH('/product_service/update/{id}.html', [App\Http\Controllers\ProductionServiceController::class, 'update'])->name('product_service.update');
      Route::DELETE('/product_service/delete/{id}', [App\Http\Controllers\ProductionServiceController::class, 'destroy'])->name('product_service.destroy');

      //product
      Route::GET('/product.html', [App\Http\Controllers\ProductController::class, 'index'])->name('product.index');
      Route::GET('/product/create.html', [App\Http\Controllers\ProductController::class, 'create'])->name('product.create');
      Route::POST('/product/store.html', [App\Http\Controllers\ProductController::class, 'store'])->name('product.store');
      Route::GET('/product/ajxlist.html', [App\Http\Controllers\ProductController::class, 'getProductList'])->name('product.getProductList');
      Route::GET('/product/edit/{id}.html', [App\Http\Controllers\ProductController::class, 'edit'])->name('product.edit');
      Route::PATCH('/product/update/{id}.html', [App\Http\Controllers\ProductController::class, 'update'])->name('product.update');
      Route::DELETE('/product/delete/{id}', [App\Http\Controllers\ProductController::class, 'destroy'])->name('product.destroy');

      //category
      Route::GET('/category.html', [App\Http\Controllers\CategoryController::class, 'index'])->name('category.index');
      Route::GET('/category/create.html', [App\Http\Controllers\CategoryController::class, 'create'])->name('category.create');
      Route::POST('/category/store.html', [App\Http\Controllers\CategoryController::class, 'store'])->name('category.store');
      Route::GET('/category/ajxlist.html', [App\Http\Controllers\CategoryController::class, 'getCategoryList'])->name('category.getCategoryList');
      Route::GET('/category/edit/{id}.html', [App\Http\Controllers\CategoryController::class, 'edit'])->name('category.edit');
      Route::PATCH('/category/update/{id}.html', [App\Http\Controllers\CategoryController::class, 'update'])->name('category.update');
      Route::DELETE('/category/delete/{id}', [App\Http\Controllers\CategoryController::class, 'destroy'])->name('category.destroy');
      
      //exchange rate
      Route::GET('/exchange_rate.html', [App\Http\Controllers\ExchangeRateController::class, 'index'])->name('exchange_rate.index');
      Route::GET('/exchange_rate/create.html', [App\Http\Controllers\ExchangeRateController::class, 'create'])->name('exchange_rate.create');
      Route::POST('/exchange_rate/store.html', [App\Http\Controllers\ExchangeRateController::class, 'store'])->name('exchange_rate.store');
      Route::GET('/exchange_rate/ajxlist.html', [App\Http\Controllers\ExchangeRateController::class, 'getExchangeRateList'])->name('exchange_rate.getExchangeRateList');
      Route::GET('/exchange_rate/edit/{id}.html', [App\Http\Controllers\ExchangeRateController::class, 'edit'])->name('exchange_rate.edit');
      Route::PATCH('/exchange_rate/update/{id}.html', [App\Http\Controllers\ExchangeRateController::class, 'update'])->name('exchange_rate.update');
      Route::DELETE('/exchange_rate/delete/{id}', [App\Http\Controllers\ExchangeRateController::class, 'destroy'])->name('exchange_rate.destroy');

      //more information
      Route::GET('/more_information.html', [App\Http\Controllers\MoreInformationController::class, 'index'])->name('more_information.index');
      Route::GET('/more_information/create.html', [App\Http\Controllers\MoreInformationController::class, 'create'])->name('more_information.create');
      Route::POST('/more_information/store.html', [App\Http\Controllers\MoreInformationController::class, 'store'])->name('more_information.store');
      Route::GET('/more_information/ajxlist.html', [App\Http\Controllers\MoreInformationController::class, 'getMoreInformationList'])->name('more_information.getMoreInformationList');
      Route::GET('/more_information/edit/{id}.html', [App\Http\Controllers\MoreInformationController::class, 'edit'])->name('more_information.edit');
      Route::PATCH('/more_information/update/{id}.html', [App\Http\Controllers\MoreInformationController::class, 'update'])->name('more_information.update');
      Route::DELETE('/more_information/delete/{id}', [App\Http\Controllers\MoreInformationController::class, 'destroy'])->name('more_information.destroy');

      //e_form detail
      Route::POST('/e_form_detail/store.html', [App\Http\Controllers\EformDetailController::class, 'store'])->name('e_form_detail.store');

      //complaint form
      Route::GET('/complaint_form.html', [App\Http\Controllers\ComplaintFormController::class, 'index'])->name('complaint_form.index');
      Route::GET('/complaint_form/create.html', [App\Http\Controllers\ComplaintFormController::class, 'create'])->name('complaint_form.create');
      Route::POST('/complaint_form/store.html', [App\Http\Controllers\ComplaintFormController::class, 'store'])->name('complaint_form.store');
      Route::GET('/complaint_form/ajxlist.html', [App\Http\Controllers\ComplaintFormController::class, 'getComplaintFormList'])->name('complaint_form.getComplaintFormList');
      Route::GET('/complaint_form/edit/{id}.html', [App\Http\Controllers\ComplaintFormController::class, 'edit'])->name('complaint_form.edit');
      Route::PATCH('/complaint_form/update/{id}.html', [App\Http\Controllers\ComplaintFormController::class, 'update'])->name('complaint_form.update');
      Route::DELETE('/complaint_form/delete/{id}', [App\Http\Controllers\ComplaintFormController::class, 'destroy'])->name('complaint_form.destroy');

      //complaint form detail
      Route::POST('/complaint_form_detail/store.html', [App\Http\Controllers\ComplaintFormDetailController::class, 'store'])->name('complaint_form_detail.store');
      
      //branches network
      Route::GET('/branches_network.html', [App\Http\Controllers\BranchesNetworkController::class, 'index'])->name('branches_network.index');
      Route::GET('/branches_network/create.html', [App\Http\Controllers\BranchesNetworkController::class, 'create'])->name('branches_network.create');
      Route::POST('/branches_network/store.html', [App\Http\Controllers\BranchesNetworkController::class, 'store'])->name('branches_network.store');
      Route::GET('/branches_network/ajxlist.html', [App\Http\Controllers\BranchesNetworkController::class, 'getBranchesNetworkList'])->name('branches_network.getBranchesNetworkList');
      Route::GET('/branches_network/edit/{id}.html', [App\Http\Controllers\BranchesNetworkController::class, 'edit'])->name('branches_network.edit');
      Route::PATCH('/branches_network/update/{id}.html', [App\Http\Controllers\BranchesNetworkController::class, 'update'])->name('branches_network.update');
      Route::DELETE('/branches_network/delete/{id}', [App\Http\Controllers\BranchesNetworkController::class, 'destroy'])->name('branches_network.destroy');

      //annual report
      Route::GET('/annual_report.html', [App\Http\Controllers\AnnualReportController::class, 'index'])->name('annual_report.index');
      Route::GET('/annual_report/create.html', [App\Http\Controllers\AnnualReportController::class, 'create'])->name('annual_report.create');
      Route::POST('/annual_report/store.html', [App\Http\Controllers\AnnualReportController::class, 'store'])->name('annual_report.store');
      Route::GET('/annual_report/ajxlist.html', [App\Http\Controllers\AnnualReportController::class, 'getAnnualReportList'])->name('annual_report.getAnnualReportList');
      Route::GET('/annual_report/edit/{id}.html', [App\Http\Controllers\AnnualReportController::class, 'edit'])->name('annual_report.edit');
      Route::PATCH('/annual_report/update/{id}.html', [App\Http\Controllers\AnnualReportController::class, 'update'])->name('annual_report.update');
      Route::DELETE('/annual_report/delete/{id}', [App\Http\Controllers\AnnualReportController::class, 'destroy'])->name('annual_report.destroy');

      //annual report detail
      Route::GET('/annual_report_detail.html', [App\Http\Controllers\AnnualReportDetailController::class, 'index'])->name('annual_report_detail.index');
      Route::GET('/annual_report_detail/create.html', [App\Http\Controllers\AnnualReportDetailController::class, 'create'])->name('annual_report_detail.create');
      Route::POST('/annual_report_detail/store.html', [App\Http\Controllers\AnnualReportDetailController::class, 'store'])->name('annual_report_detail.store');
      Route::GET('/annual_report_detail/ajxlist.html', [App\Http\Controllers\AnnualReportDetailController::class, 'getAnnualReportDetailList'])->name('annual_report_detail.getAnnualReportDetailList');
      Route::GET('/annual_report_detail/edit/{id}.html', [App\Http\Controllers\AnnualReportDetailController::class, 'edit'])->name('annual_report_detail.edit');
      Route::PATCH('/annual_report_detail/update/{id}.html', [App\Http\Controllers\AnnualReportDetailController::class, 'update'])->name('annual_report_detail.update');
      Route::DELETE('/annual_report_detail/delete/{id}', [App\Http\Controllers\AnnualReportDetailController::class, 'destroy'])->name('annual_report_detail.destroy');


});