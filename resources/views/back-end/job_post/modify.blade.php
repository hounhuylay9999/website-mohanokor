@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('job_post.index') }}">@lang('message.job_post')</a></li>
            <li class="breadcrumb-item active">@lang('message.modify')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('job_post.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    {!! Form::model($job_post,['method' => 'PATCH','route' => ['job_post.update', $job_post->id],'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="card-header">@lang('message.modify')</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.position')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('position_id')) has-error @endif">
                                        <select name="position_id"  class="form-control select2" style="width: 100% !important;">
                                            @foreach($position as $k => $v)
                                                <option @if($job_post->position_id == $v->id) selected @endif  @if(old('position_id') == $v->id) selected @endif value="{{ $v->id }}">{{ $v->name_en }}</option>
                                            @endforeach
                                        </select>

                                        @if($errors->first('position_id'))
                                            <span class="text-danger">{{$errors->first('position_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.location')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('location_id')) has-error @endif">
                                        <select name="location_id"  class="form-control select2" style="width: 100% !important;">
                                            @foreach($location as $k => $v)
                                            <option @if($job_post->location_id == $v->id) selected @endif  @if(old('location_id') == $v->id) selected @endif value="{{ $v->id }}">{{ $v->name_en }}</option>
                                            @endforeach
                                        </select>

                                        @if($errors->first('location_id'))
                                            <span class="text-danger">{{$errors->first('location_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6"> 
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.date_line')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('date_line')) has-error @endif">
                                        {!! Form::text('date_line',null, array('placeholder' => 'mm/dd/yy','class' => 'form-control datepicker')) !!}
                                        @if($errors->first('date_line'))
                                            <span class="text-danger">{{$errors->first('date_line')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6"> 
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.document')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('date_line')) has-error @endif">
                                        <input value="{{old('job_post_thumnail',$job_post->job_post_thumnail)}}" type="file" name="job_post_thumnail" class="news_cover" multiple />
                                        <div class="image_message_news_cover"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    @can('careers-edit')
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.modify')</button>
                    </div>
                    @endcan
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }
       
        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">

        $( ".datepicker" ).datepicker({
            format: "mm/dd/yy",
            weekStart: 0,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            rtl: true,
            orientation: "auto"
        }); 

        $('.select2').select2();

        $(".news_cover").change(function(e) {
            e.preventDefault();
            $(".image_message_news_cover").html('');
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = $('.news_cover').val();
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

            //check size
            if (sizefile < 9000000) {
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $(".image_message_news_cover").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $(".news_cover").css("color", "green");
                                $('.image-defalt-news_cover').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                            $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                    $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $(".image_message_news_cover").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
    });

    </script>
@endsection
