@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('complaint_form.index') }}">@lang('message.complaint_form')</a></li>
            <li class="breadcrumb-item active">@lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('complaint_form.index') }}">@lang('message.back')</a>
        </div>
    </div>
    <script src="//cdn.ckeditor.com/4.20.1/standard/ckeditor.js"></script>


@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    {!! Form::open(array('route' => 'complaint_form.store','method'=>'POST','class'=>'form-horizontal','enctype'=>'multipart/form-data','id'=>'ImageForm')) !!}
                    <div class="card-header">@lang('message.add_new')</div>

                    <div class="tab-pane" id="features" role="tabpanel">
                        <div class="col-md-12 mb-4">
                            <div class="nav-tabs-boxed lang-kh-en">
                                
                                <div class="tab-content mt-2">
                                    <div class="tab-pane active" id="chemical_properties" role="tabpanel">
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                   
                                                    <div class="ibox-content">
                                                        <div class="row">
                                                            <div class="col-sm-12 col-lg-6">
                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                                    <div class="col-sm-12 @if($errors->has('title_en')) has-error @endif">
                                                                        {!! Form::text('title_en', null, array('placeholder' => '','class' => 'form-control')) !!}
                                                                        @if($errors->first('title_en'))
                                                                            <span class="text-danger">{{$errors->first('title_en')}}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-lg-6">
                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                                    <div class="col-sm-12 @if($errors->has('title_kh')) has-error @endif">
                                                                        {!! Form::text('title_kh', null, array('placeholder' => '','class' => 'form-control')) !!}
                                                                        @if($errors->first('title_kh'))
                                                                            <span class="text-danger">{{$errors->first('title_kh')}}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 col-lg-6">
                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.note') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                                    <div class="col-sm-12 @if($errors->has('sort_text_en')) has-error @endif">
                                                                        {!! Form::textarea('sort_text_en',null, array('placeholder' => '','class' => 'form-control qty_en', 'id'=>'editor1')) !!}
                                                                        @if($errors->first('sort_text_en'))
                                                                            <span class="text-danger">{{$errors->first('sort_text_en')}}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 col-lg-6 ">
                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.note') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                                    <div class="col-sm-12 @if($errors->has('sort_text_kh')) has-error @endif">
                                                                        {!! Form::textarea('sort_text_kh', null, array('placeholder' => '','class' => 'form-control qty_kh', 'id'=>'editor2')) !!}
                                                                        @if($errors->first('sort_text_kh'))
                                                                            <span class="text-danger">{{$errors->first('sort_text_kh')}}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            

                                                            <div class="col-sm-12 col-lg-12">
                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                                    <div class="col-sm-12 @if($errors->has('full_text_en')) has-error @endif">
                                                                        {!! Form::textarea('full_text_en',null, array('placeholder' => '','class' => 'form-control qty_en', 'id'=>'editor3')) !!}
                                                                        @if($errors->first('full_text_en'))
                                                                            <span class="text-danger">{{$errors->first('full_text_en')}}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 col-lg-12">
                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                                    <div class="col-sm-12 @if($errors->has('full_text_kh')) has-error @endif">
                                                                        {!! Form::textarea('full_text_kh', null, array('placeholder' => '','class' => 'form-control qty_kh', 'id'=>'editor4')) !!}
                                                                        @if($errors->first('full_text_kh'))
                                                                            <span class="text-danger">{{$errors->first('full_text_kh')}}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      
                                                        <div class="row">
                                                            <div class="col-sm-12 col-lg-12">
                                                               
                                                                    <div class="text-center">
                                                                        <div class="file-box">
                                                                            <div class="file">
                                                                                <span class="corner"></span>
                                                                                <div class="">
                                                                                    <div class="m-b-sm">
                                                                                        <span class="profile-picture">
                                                                                            <div class="logo-thumnail">
                                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                                    <div class="fileinput-news img-circle">
                                                                                                        <img class="img-circle img-circle1 image-defalt-news_cover" src="{{ url('/assets/img/no_img.png') }}">
                                                                                                    </div>
                                                                                                    <div class="mt-3">
                                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                                            <span class="fileinput-exists"> Photo (1440x650) Pixcel</span>
                                                                                                            {{ Form::file('complaint_form_thumnail',['class'=>'news_cover','accept'=>'image/*']) }}
                                                                                                        </span>
                                                                                                    </div>
                                                                                                    <div class="image_message_news_cover"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </span>
                                                                                    </div>
                                                                                    
                                                                                </div>
                                                                                <div class="file-name"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @can('contact_us-create')
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.save')</button>
                    </div>
                    @endcan
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }
       
        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
    </style>
@endsection


@section('scripts')
<script type="text/javascript">

    $(document).ready(function () {
        CKEDITOR.replace('editor1',
            {
                allowedContent: true
            });

            CKEDITOR.replace('editor2',
            {
                allowedContent: true
            });

            CKEDITOR.replace('editor3',
            {
                allowedContent: true
            });

            CKEDITOR.replace('editor4',
            {
                allowedContent: true
            });
           
    });
    
    $(".news_cover").change(function(e) {
        e.preventDefault();
        $(".image_message_news_cover").html(''); 
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.news_cover').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_news_cover").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $(".news_cover").css("color", "green");
                            $('.image-defalt-news_cover').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                        $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_news_cover").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    </script>
@endsection
