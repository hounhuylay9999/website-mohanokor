@extends('layouts.app')
@section('breadcrumb')
<div class="c-subheader px-3">
    <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item active">@lang('message.mission')</li>
    </ol>
</div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            {!! Form::open(array('route' => 'mission.store','method'=>'POST','class'=>'form-horizontal form-box','enctype'=>'multipart/form-data')) !!}

            <div class="col-md-12 mb-4">
                <div class="nav-tabs-boxed nav-tabs-boxed-left">
                    <ul class="nav nav-tabs mm-left" role="tablist" style="width: 200px">
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#vision" role="tab" aria-controls="vision" aria-selected="true">@lang('message.vision_statement')</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#mission" role="tab" aria-controls="mission" aria-selected="true">@lang('message.mission_statement')</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#core" role="tab" aria-controls="core" aria-selected="true">@lang('message.core_values')</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#trust" role="tab" aria-controls="trust" aria-selected="true">@lang('message.trust')</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exellence" role="tab" aria-controls="exellence" aria-selected="true">@lang('message.exellence')</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#accountability" role="tab" aria-controls="accountability" aria-selected="true">@lang('message.accountability')</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#morality" role="tab" aria-controls="morality" aria-selected="true">@lang('message.morality')</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="vision" role="tabpanel">
    
                             <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#vision" role="tab" aria-controls="vision" aria-selected="true"><span class="back-lang">@lang('message.vision_statement')</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="vision" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('vision_title_en')) has-error @endif">
                                                            {!! Form::text('vision_title_en', $mission->vision_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('vision_title_en'))
                                                                <span class="text-danger">{{$errors->first('vision_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('vision_title_kh')) has-error @endif">
                                                            {!! Form::text('vision_title_kh', $mission->vision_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('vision_title_kh'))
                                                                <span class="text-danger">{{$errors->first('vision_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('vision_des_en')) has-error @endif">
                                                            {!! Form::textarea('vision_des_en', $mission->vision_des_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('vision_des_en'))
                                                                <span class="text-danger">{{$errors->first('vision_des_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('vision_des_kh')) has-error @endif">
                                                            {!! Form::textarea('vision_des_kh', $mission->vision_des_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('vision_des_kh'))
                                                                <span class="text-danger">{{$errors->first('vision_des_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="tab-pane active" id="mission" role="tabpanel">
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#mission" role="tab" aria-controls="mission" aria-selected="true"><span class="back-lang">@lang('message.mission_statement')</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="mission" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('mission_title_en')) has-error @endif">
                                                            {!! Form::text('mission_title_en', $mission->mission_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('mission_title_en'))
                                                                <span class="text-danger">{{$errors->first('mission_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('mission_title_kh')) has-error @endif">
                                                            {!! Form::text('mission_title_kh', $mission->mission_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('mission_title_kh'))
                                                                <span class="text-danger">{{$errors->first('mission_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('mission_des_en')) has-error @endif">
                                                            {!! Form::textarea('mission_des_en', $mission->mission_des_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('mission_des_en'))
                                                                <span class="text-danger">{{$errors->first('mission_des_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('mission_des_kh')) has-error @endif">
                                                            {!! Form::textarea('mission_des_kh', $mission->mission_des_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('mission_des_kh'))
                                                                <span class="text-danger">{{$errors->first('mission_des_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="core" role="tabpanel">
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#core" role="tab" aria-controls="core" aria-selected="true"><span class="back-lang">@lang('message.core_values')</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="core" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('core_title_en')) has-error @endif">
                                                            {!! Form::text('core_title_en', $mission->core_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('core_title_en'))
                                                                <span class="text-danger">{{$errors->first('core_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('core_title_kh')) has-error @endif">
                                                            {!! Form::text('core_title_kh', $mission->core_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('core_title_kh'))
                                                                <span class="text-danger">{{$errors->first('core_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('core_des_en')) has-error @endif">
                                                            {!! Form::textarea('core_des_en', $mission->core_des_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('core_des_en'))
                                                                <span class="text-danger">{{$errors->first('core_des_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('core_des_kh')) has-error @endif">
                                                            {!! Form::textarea('core_des_kh', $mission->core_des_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('core_des_kh'))
                                                                <span class="text-danger">{{$errors->first('core_des_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="trust" role="tabpanel">
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#trust" role="tab" aria-controls="trust" aria-selected="true"><span class="back-lang">@lang('message.trust')</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="trust" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('trust_title_en')) has-error @endif">
                                                            {!! Form::text('trust_title_en', $mission->trust_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('trust_title_en'))
                                                                <span class="text-danger">{{$errors->first('trust_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('trust_title_kh')) has-error @endif">
                                                            {!! Form::text('trust_title_kh', $mission->trust_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('trust_title_kh'))
                                                                <span class="text-danger">{{$errors->first('trust_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('trust_des_en')) has-error @endif">
                                                            {!! Form::textarea('trust_des_en', $mission->trust_des_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('trust_des_en'))
                                                                <span class="text-danger">{{$errors->first('trust_des_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('trust_des_kh')) has-error @endif">
                                                            {!! Form::textarea('trust_des_kh', $mission->trust_des_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('trust_des_kh'))
                                                                <span class="text-danger">{{$errors->first('trust_des_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="exellence" role="tabpanel">
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exellence" role="tab" aria-controls="exellence" aria-selected="true"><span class="back-lang">@lang('message.exellence')</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="exellence" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('exellence_title_en')) has-error @endif">
                                                            {!! Form::text('exellence_title_en', $mission->exellence_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('exellence_title_en'))
                                                                <span class="text-danger">{{$errors->first('exellence_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('exellence_title_kh')) has-error @endif">
                                                            {!! Form::text('exellence_title_kh', $mission->exellence_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('exellence_title_kh'))
                                                                <span class="text-danger">{{$errors->first('exellence_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('exellence_des_en')) has-error @endif">
                                                            {!! Form::textarea('exellence_des_en', $mission->exellence_des_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('exellence_des_en'))
                                                                <span class="text-danger">{{$errors->first('exellence_des_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('exellence_des_kh')) has-error @endif">
                                                            {!! Form::textarea('exellence_des_kh', $mission->exellence_des_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('exellence_des_kh'))
                                                                <span class="text-danger">{{$errors->first('exellence_des_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="accountability" role="tabpanel">
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#accountability" role="tab" aria-controls="accountability" aria-selected="true"><span class="back-lang">@lang('message.accountability')</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="accountability" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('accountability_title_en')) has-error @endif">
                                                            {!! Form::text('accountability_title_en', $mission->accountability_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('accountability_title_en'))
                                                                <span class="text-danger">{{$errors->first('accountability_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('accountability_title_kh')) has-error @endif">
                                                            {!! Form::text('accountability_title_kh', $mission->accountability_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('accountability_title_kh'))
                                                                <span class="text-danger">{{$errors->first('accountability_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('accountability_des_en')) has-error @endif">
                                                            {!! Form::textarea('accountability_des_en', $mission->accountability_des_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('accountability_des_en'))
                                                                <span class="text-danger">{{$errors->first('accountability_des_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('accountability_des_kh')) has-error @endif">
                                                            {!! Form::textarea('accountability_des_kh', $mission->accountability_des_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('accountability_des_kh'))
                                                                <span class="text-danger">{{$errors->first('accountability_des_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="morality" role="tabpanel">
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#morality" role="tab" aria-controls="morality" aria-selected="true"><span class="back-lang">@lang('message.morality')</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="morality" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('morality_title_en')) has-error @endif">
                                                            {!! Form::text('morality_title_en', $mission->morality_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('morality_title_en'))
                                                                <span class="text-danger">{{$errors->first('morality_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('morality_title_kh')) has-error @endif">
                                                            {!! Form::text('morality_title_kh', $mission->morality_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('morality_title_kh'))
                                                                <span class="text-danger">{{$errors->first('morality_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('morality_des_en')) has-error @endif">
                                                            {!! Form::textarea('morality_des_en', $mission->morality_des_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('morality_des_en'))
                                                                <span class="text-danger">{{$errors->first('morality_des_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.des') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('morality_des_kh')) has-error @endif">
                                                            {!! Form::textarea('morality_des_kh', $mission->morality_des_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('morality_des_kh'))
                                                                <span class="text-danger">{{$errors->first('morality_des_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @can('page-create')
            <div class="col-lg-12">
                <div class="card-footer text-right">
                    <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.save')</button>
                </div>
            </div>
            @endcan
            {!! Form::close() !!}
        </div>
    </div>

    <style>
        .editable{
            width:120px;
            height:120px;
            object-fit:cover;
            border-radius:50%;
        }
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .form-box{
            border: 1px solid #dadada;
            padding: 10px;
            margin-bottom: 20px;
            float: left;
            width: 100%;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
        .nav-link{
            float: left;
            width: 100%;
        }
</style>

@endsection
@section('scripts')

@endsection