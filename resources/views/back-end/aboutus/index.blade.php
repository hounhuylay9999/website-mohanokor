@extends('layouts.app')
@section('breadcrumb')
<div class="c-subheader px-3">
    <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item active">@lang('message.general')</li>
    </ol>
</div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            {!! Form::open(array('route' => 'aboutus.store','method'=>'POST','class'=>'form-horizontal form-box','enctype'=>'multipart/form-data')) !!}

            <div class="col-md-12 mb-4">
                <div class="nav-tabs-boxed nav-tabs-boxed-left">
                    <ul class="nav nav-tabs mm-left" role="tablist" style="width: 200px">
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="true">History</a></li>
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#vision" role="tab" aria-controls="vision" aria-selected="true">Vision & mission</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#organization" role="tab" aria-controls="organization" aria-selected="false">Organization Chart</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#management" role="tab" aria-controls="management" aria-selected="false">Management Team</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#photo" role="tab" aria-controls="photo" aria-selected="false">Photo</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="false">Video</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="false">News & Event</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="history" role="tabpanel">
                             
                             <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="true"><span class="back-lang">History</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="history" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">History Title (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('h_title_en')) has-error @endif">
                                                            {!! Form::text('h_title_en', $aboutus->h_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('h_title_en'))
                                                                <span class="text-danger">{{$errors->first('h_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">History Title (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('h_title_kh')) has-error @endif">
                                                            {!! Form::text('h_title_kh',  $aboutus->h_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('h_title_kh'))
                                                                <span class="text-danger">{{$errors->first('h_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">History Description (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('h_desc_en')) has-error @endif">
                                                            {{-- {!! Form::text('h_desc_en', $aboutus->h_desc_en, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="h_desc_en">{{ $aboutus->h_desc_en }}</textarea>
                                                            @if($errors->first('h_desc_en'))
                                                                <span class="text-danger">{{$errors->first('h_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">History Description (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('h_desc_kh')) has-error @endif">
                                                            {{-- {!! Form::text('h_desc_kh', $aboutus->h_desc_kh, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="h_desc_kh">{{ $aboutus->h_desc_kh }}</textarea>
                                                            @if($errors->first('h_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('h_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="img-circle img-circle1 image-defalt-h_thumbnail" src="@if(isset($aboutus->h_thumbnail) == null || $aboutus->h_thumbnail == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$aboutus->h_thumbnail) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                            {{ Form::file('h_thumbnail',['class'=>'h_thumbnail','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_h_thumbnail"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                            
                        </div>

                        <div class="tab-pane active" id="vision" role="tabpanel">
                            <!-- header -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#vision" role="tab" aria-controls="vision" aria-selected="true"><span class="back-lang">Vision & Mision</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="vision" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Vision Title (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('v_title_en')) has-error @endif">
                                                            {!! Form::text('v_title_en', $aboutus->v_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('v_title_en'))
                                                                <span class="text-danger">{{$errors->first('v_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Vision Title (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('v_title_kh')) has-error @endif">
                                                            {!! Form::text('v_title_kh', $aboutus->v_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('v_title_kh'))
                                                                <span class="text-danger">{{$errors->first('v_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Vision Description (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('v_desc_en')) has-error @endif">
                                                            {{-- {!! Form::text('v_desc_en', $aboutus->v_desc_en, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="v_desc_en">{{ $aboutus->v_desc_en }}</textarea>
                                                            @if($errors->first('v_desc_en'))
                                                                <span class="text-danger">{{$errors->first('v_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Vision Description (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('v_desc_kh')) has-error @endif">
                                                            {{-- {!! Form::text('v_desc_kh', $aboutus->v_desc_kh, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="v_desc_kh">{{ $aboutus->v_desc_kh }}</textarea>
                                                            @if($errors->first('v_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('v_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="img-circle img-circle1 image-defalt-v_thumbnail" src="@if(isset($aboutus->v_thumbnail) == null || $aboutus->v_thumbnail == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$aboutus->v_thumbnail) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                            {{ Form::file('v_thumbnail',['class'=>'v_thumbnail','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_v_thumbnail"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- header -->
                        </div>

                        <div class="tab-pane" id="organization" role="tabpanel">
                            <!-- footer -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#organization" role="tab" aria-controls="organization" aria-selected="true"><span class="back-lang">Organization Chart</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="organization" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Organization Chart Titlte (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('o_title_en')) has-error @endif">
                                                            {!! Form::text('o_title_en', $aboutus->o_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('o_title_en'))
                                                                <span class="text-danger">{{$errors->first('o_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Organization Chart Titlte (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('o_title_kh')) has-error @endif">
                                                            {!! Form::text('o_title_kh', $aboutus->o_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('o_title_kh'))
                                                                <span class="text-danger">{{$errors->first('o_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Organization Chart Description (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('o_desc_en')) has-error @endif">
                                                            {{-- {!! Form::text('o_desc_en', $aboutus->o_desc_en, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="o_desc_en">{{ $aboutus->o_desc_en }}</textarea>
                                                            @if($errors->first('o_desc_en'))
                                                                <span class="text-danger">{{$errors->first('o_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Organization Chart Description (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('o_desc_kh')) has-error @endif">
                                                            {{-- {!! Form::text('o_desc_kh', $aboutus->o_desc_kh, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="o_desc_kh">{{ $aboutus->o_desc_kh }}</textarea>
                                                            @if($errors->first('o_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('o_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="img-circle img-circle1 image-defalt-o_thumbnail" src="@if(isset($aboutus->o_thumbnail) == null || $aboutus->o_thumbnail == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$aboutus->o_thumbnail) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                            {{ Form::file('o_thumbnail',['class'=>'o_thumbnail','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_o_thumbnail"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer -->
                        </div>

                        <div class="tab-pane" id="management" role="tabpanel">
                            <!-- footer -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#management" role="tab" aria-controls="management" aria-selected="true"><span class="back-lang">Management Team</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="management" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Management Team Titlte (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('m_title_en')) has-error @endif">
                                                            {!! Form::text('m_title_en', $aboutus->m_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('m_title_en'))
                                                                <span class="text-danger">{{$errors->first('m_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Management Team Titlte (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('m_title_kh')) has-error @endif">
                                                            {!! Form::text('m_title_kh', $aboutus->m_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('m_title_kh'))
                                                                <span class="text-danger">{{$errors->first('m_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Management Team Description (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('m_desc_en')) has-error @endif">
                                                            {{-- {!! Form::text('m_desc_en', $aboutus->m_desc_en, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="m_desc_en">{{ $aboutus->m_desc_en }}</textarea>
                                                            @if($errors->first('m_desc_en'))
                                                                <span class="text-danger">{{$errors->first('m_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Management Team Description (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('m_desc_kh')) has-error @endif">
                                                            {{-- {!! Form::text('m_desc_kh', $aboutus->m_desc_kh, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="m_desc_kh">{{ $aboutus->m_desc_kh }}</textarea>
                                                            @if($errors->first('m_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('m_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="img-circle img-circle1 image-defalt-m_thumbnail" src="@if(isset($aboutus->m_thumbnail) == null || $aboutus->m_thumbnail == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$aboutus->m_thumbnail) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                            {{ Form::file('m_thumbnail',['class'=>'m_thumbnail','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_m_thumbnail"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer -->
                        </div>

                        <div class="tab-pane" id="photo" role="tabpanel">
                            <!-- footer -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#photo" role="tab" aria-controls="air" aria-selected="true"><span class="back-lang">Photo</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="air" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Photo Titlte (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('p_title_en')) has-error @endif">
                                                            {!! Form::text('p_title_en', $aboutus->p_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('p_title_en'))
                                                                <span class="text-danger">{{$errors->first('p_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Photo Titlte (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('p_title_kh')) has-error @endif">
                                                            {!! Form::text('p_title_kh', $aboutus->p_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('p_title_kh'))
                                                                <span class="text-danger">{{$errors->first('p_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Photo Description (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('p_desc_en')) has-error @endif">
                                                            {{-- {!! Form::text('p_desc_en', $aboutus->p_desc_en, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="p_desc_en">{{ $aboutus->p_desc_en }}</textarea>
                                                            @if($errors->first('p_desc_en'))
                                                                <span class="text-danger">{{$errors->first('p_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Photo Description (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('p_desc_kh')) has-error @endif">
                                                            {{-- {!! Form::text('p_desc_kh', $aboutus->p_desc_kh, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="p_desc_kh">{{ $aboutus->p_desc_kh }}</textarea>
                                                            @if($errors->first('p_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('p_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="img-circle img-circle1 image-defalt-p_thumbnail" src="@if(isset($aboutus->p_thumbnail) == null || $aboutus->p_thumbnail == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$aboutus->p_thumbnail) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                            {{ Form::file('p_thumbnail',['class'=>'p_thumbnail','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_p_thumbnail"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer -->
                        </div>

                        <div class="tab-pane" id="video" role="tabpanel">
                            <!-- footer -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="true"><span class="back-lang">Video</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="video" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Video Titlte (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('vdo_title_en')) has-error @endif">
                                                            {!! Form::text('vdo_title_en', $aboutus->vdo_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('vdo_title_en'))
                                                                <span class="text-danger">{{$errors->first('vdo_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Video Titlte (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('vdo_title_kh')) has-error @endif">
                                                            {!! Form::text('vdo_title_kh', $aboutus->vdo_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('vdo_title_kh'))
                                                                <span class="text-danger">{{$errors->first('vdo_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Video Description (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('vdo_desc_en')) has-error @endif">
                                                            {{-- {!! Form::text('vdo_desc_en', $aboutus->vdo_desc_en, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="vdo_desc_en">{{ $aboutus->vdo_desc_en }}</textarea>
                                                            @if($errors->first('vdo_desc_en'))
                                                                <span class="text-danger">{{$errors->first('vdo_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Video Description (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('vdo_desc_kh')) has-error @endif">
                                                            {{-- {!! Form::text('vdo_desc_kh', $aboutus->vdo_desc_kh, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="vdo_desc_kh">{{ $aboutus->vdo_desc_kh }}</textarea>
                                                            @if($errors->first('vdo_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('vdo_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="img-circle img-circle1 image-defalt-vdo_location" src="@if(isset($aboutus->vdo_location) == null || $aboutus->vdo_location == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$aboutus->p_thumbnail) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                            {{ Form::file('vdo_location',['class'=>'vdo_location','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_vdo_location"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer -->
                        </div>

                        <div class="tab-pane" id="news" role="tabpanel">
                            <!-- footer -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="true"><span class="back-lang">News & Event</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="news" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">New & Event Titlte (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('n_title_en')) has-error @endif">
                                                            {!! Form::text('n_title_en', $aboutus->n_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('n_title_en'))
                                                                <span class="text-danger">{{$errors->first('n_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">New & Event Titlte (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('n_title_kh')) has-error @endif">
                                                            {!! Form::text('n_title_kh', $aboutus->n_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('n_title_kh'))
                                                                <span class="text-danger">{{$errors->first('n_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">New & Event Description (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('n_desc_en')) has-error @endif">
                                                            {{-- {!! Form::text('n_desc_en', $aboutus->n_desc_en, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="n_desc_en">{{ $aboutus->n_desc_en }}</textarea>
                                                            @if($errors->first('n_desc_en'))
                                                                <span class="text-danger">{{$errors->first('n_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">New & Event Description (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('n_desc_kh')) has-error @endif">
                                                            {{-- {!! Form::text('n_desc_kh', $aboutus->n_desc_kh, array('placeholder' => '','class' => 'form-control')) !!} --}}
                                                            <textarea id="" class="form-control" name="n_desc_kh">{{ $aboutus->n_desc_kh }}</textarea>
                                                            @if($errors->first('n_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('n_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="img-circle img-circle1 image-defalt-n_thumbnail" src="@if(isset($aboutus->n_thumbnail) == null || $aboutus->n_thumbnail == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$aboutus->n_thumbnail) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                            {{ Form::file('n_thumbnail',['class'=>'n_thumbnail','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_n_thumbnail"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer -->
                        </div>

                        

                       


                    </div>
                </div>
            </div>
            @can('page-create')
            <div class="col-lg-12">
                <div class="card-footer text-right">
                    <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.save')</button>
                </div>
            </div>
            @endcan
            {!! Form::close() !!}
        </div>
    </div>

    <style>
        .editable{
            width:120px;
            height:120px;
            object-fit:cover;
            border-radius:50%;
        }
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .form-box{
            border: 1px solid #dadada;
            padding: 10px;
            margin-bottom: 20px;
            float: left;
            width: 100%;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
        .nav-link{
            float: left;
            width: 100%;
        }
</style>

@endsection
@section('scripts')
<script type="text/javascript">


    $(".h_thumbnail").change(function(e) {
        e.preventDefault();
        $(".image_message_h_thumbnail").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.h_thumbnail').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_h_thumbnail").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".h_thumbnail").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-h_thumbnail').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-h_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_h_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-h_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_h_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-h_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_h_thumbnail").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".v_thumbnail").change(function(e) {
        e.preventDefault();
        $(".image_message_v_thumbnail").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.v_thumbnail').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_v_thumbnail").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".v_thumbnail").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-v_thumbnail').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-v_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_v_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-v_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_v_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-v_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_v_thumbnail").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".o_thumbnail").change(function(e) {
        e.preventDefault();
        $(".image_message_o_thumbnail").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.o_thumbnail').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_o_thumbnail").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".o_thumbnail").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-o_thumbnail').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-o_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_o_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-o_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_o_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-o_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_o_thumbnail").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".m_thumbnail").change(function(e) {
        e.preventDefault();
        $(".image_message_m_thumbnail").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.m_thumbnail').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_m_thumbnail").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".m_thumbnail").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-m_thumbnail').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-m_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_m_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-m_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_m_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-m_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_m_thumbnail").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".p_thumbnail").change(function(e) {
        e.preventDefault();
        $(".image_message_p_thumbnail").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.p_thumbnail').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_p_thumbnail").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".p_thumbnail").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-p_thumbnail').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-p_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_p_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-p_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_p_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-p_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_p_thumbnail").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".n_thumbnail").change(function(e) {
        e.preventDefault();
        $(".image_message_n_thumbnail").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.n_thumbnail').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_n_thumbnail").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".n_thumbnail").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-n_thumbnail').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-n_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_n_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-n_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_n_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-n_thumbnail').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_n_thumbnail").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".vdo_location").change(function(e) {
        e.preventDefault();
        $(".image_message_vdo_location").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.vdo_location').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_vdo_location").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".vdo_location").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-vdo_location').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-vdo_location').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_vdo_location").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-vdo_location').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_vdo_location").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-vdo_location').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_vdo_location").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });


</script>
@endsection