@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('about.index') }}">@lang('message.aboutus')</a></li>
            <li class="breadcrumb-item active">@lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('about.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12"> 
                <div class="card">
                    {!! Form::open(array('route' => 'about.store','method'=>'POST','class'=>'form-horizontal','enctype'=>'multipart/form-data','id'=>'ImageForm')) !!}
                    <div class="card-header">@lang('message.add_new')</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-8 col-lg-8">
                                <?php @$about_object = json_decode($about_list->about_object);?>
                                <div class="card">
                                    <div class="card-header">@lang('message.page_info')</div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.about_title_kh')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('about_title_kh')) has-error @endif">
                                                        {!! Form::text('about_title_kh', @$about_object->about_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('about_title_kh'))
                                                            <span class="text-danger">{{$errors->first('about_title_kh')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.about_title_en')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('about_title_en')) has-error @endif">
                                                        {!! Form::text('about_title_en', @$about_object->about_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('about_title_en'))
                                                            <span class="text-danger">{{$errors->first('about_title_en')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header p-2">
                                        <ul class="nav nav-pills">
                                        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">@lang('message.company_overview_kh')</a></li> 
                                        <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">@lang('message.company_overview_eng')</a></li>
                                        </ul>
                                    </div><!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="tab-content">
                                        <!-- /.tab-pane -->
                                            <div class="active tab-pane" id="activity">                                       
                                                <div class="row">
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 @if($errors->has('company_overview_kh')) has-error @endif">
                                                                {!! Form::textarea('company_overview_kh', @$about_object->company_overview_kh, array('placeholder' => '','class' => 'form-control company_overview_kh')) !!}
                                                                @if($errors->first('company_overview_kh'))
                                                                    <span class="text-danger">{{$errors->first('company_overview_kh')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <!-- /.tab-pane -->
                                        
                                            <div class="tab-pane" id="settings">                                        
                                                <div class="row">
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 @if($errors->has('company_overview_eng')) has-error @endif">
                                                                {!! Form::textarea('company_overview_eng', @$about_object->company_overview_eng, array('placeholder' => '','class' => 'form-control company_overview_eng')) !!}
                                                                @if($errors->first('company_overview_eng'))
                                                                    <span class="text-danger">{{$errors->first('company_overview_eng')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>
                                <!--Our vision -->                                
                                 <div class="card">
                                    <div class="card-header p-2">
                                        <ul class="nav nav-pills">
                                        <li class="nav-item"><a class="nav-link active" href="#ourvission_kh" data-toggle="tab">@lang('message.ourvission_kh')</a></li> 
                                        <li class="nav-item"><a class="nav-link" href="#ourvission_eng" data-toggle="tab">@lang('message.ourvission_eng')</a></li>
                                        </ul>
                                    </div><!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="tab-content">
                                        <!-- /.tab-pane -->
                                            <div class="active tab-pane" id="ourvission_kh">                                       
                                                <div class="row">
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 @if($errors->has('ourvission_kh')) has-error @endif">
                                                                {!! Form::textarea('ourvission_kh', @$about_object->ourvission_kh, array('placeholder' => '','class' => 'form-control ourvission_kh')) !!}
                                                                @if($errors->first('ourvission_kh'))
                                                                    <span class="text-danger">{{$errors->first('ourvission_kh')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <!-- /.tab-pane -->
                                        
                                            <div class="tab-pane" id="ourvission_eng">                                        
                                                <div class="row">
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 @if($errors->has('ourvission_eng')) has-error @endif">
                                                                {!! Form::textarea('ourvission_eng', @$about_object->ourvission_eng, array('placeholder' => '','class' => 'form-control ourvission_eng')) !!}
                                                                @if($errors->first('ourvission_eng'))
                                                                    <span class="text-danger">{{$errors->first('ourvission_eng')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>
                                <!--Our mission -->
                                <div class="card">
                                    <div class="card-header p-2">
                                        <ul class="nav nav-pills">
                                        <li class="nav-item"><a class="nav-link active" href="#ourmission_kh" data-toggle="tab">@lang('message.ourmission_kh')</a></li> 
                                        <li class="nav-item"><a class="nav-link" href="#ourmission_eng" data-toggle="tab">@lang('message.ourmission_eng')</a></li>
                                        </ul>
                                    </div><!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="tab-content">
                                        <!-- /.tab-pane -->
                                            <div class="active tab-pane" id="ourmission_kh">                                       
                                                <div class="row">
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 @if($errors->has('ourmission_kh')) has-error @endif">
                                                                {!! Form::textarea('ourmission_kh', @$about_object->ourmission_kh, array('placeholder' => '','class' => 'form-control ourmission_kh')) !!}
                                                                @if($errors->first('ourmission_kh'))
                                                                    <span class="text-danger">{{$errors->first('ourmission_kh')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <!-- /.tab-pane -->
                                        
                                            <div class="tab-pane" id="ourmission_eng">                                        
                                                <div class="row">
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 @if($errors->has('ourmission_eng')) has-error @endif">
                                                                {!! Form::textarea('ourmission_eng', @$about_object->ourmission_eng, array('placeholder' => '','class' => 'form-control ourmission_eng')) !!}
                                                                @if($errors->first('ourmission_eng'))
                                                                    <span class="text-danger">{{$errors->first('ourmission_eng')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>
                                <!--Our core value -->
                                <div class="card">
                                    <div class="card-header p-2">
                                        <ul class="nav nav-pills">
                                            <li class="nav-item"><a class="nav-link active" href="#core_value_khmer" data-toggle="tab">@lang('message.core_value_khmer')</a></li> 
                                            <li class="nav-item"><a class="nav-link" href="#core_value_english" data-toggle="tab">@lang('message.core_value_english')</a></li>
                                        </ul>
                                    </div><!-- /.card-header -->

                                    <div class="card-body">

                                        <div class="tab-content">
                                            <div class="active tab-pane" id="core_value_khmer">                                                
                                                <?php 
                                                    
                                                    @$core_value = json_decode(@$about_object->core_value_title_kh);
                                                    if($core_value==null){
                                                        $total_core_value = 1;
                                                    }else{
                                                        $total_core_value = count(@$core_value); 
                                                    }
                                                    @$core_value_description = json_decode(@$about_object->core_value_description_kh);
                                                    
                                                ?>
                                                    @for($i=0;$i<$total_core_value;$i++)
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">                                            
                                                            <textarea type="text" name="core_value_title_kh[]" class="form-control col-sm-4 @if($errors->has('core_value_title_kh')) has-error @endif" placeholder="Enter title">{{ @$core_value[$i] }}</textarea>
                                                                @if($errors->first('core_value_title_kh'))
                                                                    <span class="text-danger">{{$errors->first('core_value_title_kh')}}</span>
                                                                @endif
                                                            <textarea type="text" name="core_value_description_kh[]" class="form-control col-sm-7 @if($errors->has('features_description_kh')) has-error @endif" style="margin-left:10px;" placeholder="Enter Description">{{ @$core_value_description[$i] }}</textarea>
                                                                @if($errors->first('core_value_description_kh'))
                                                                    <span class="text-danger">{{$errors->first('core_value_description_kh')}}</span>
                                                                @endif                                                                                                                                                   
                                                        </div>
                                                        <div class="form-group row">   
                                                            <div class="input-group-append col-sm-3 nopadding">                
                                                                <button id="removeRow" type="button" class="btn btn-danger">@lang('message.remove')</button>
                                                            </div>   
                                                        </div>
                                                    </div>         
                                                    @endfor
                                                    <div id="newRow"></div>                                               
                                                    <button id="addRow" type="button" class="btn btn-info" style="margin-top:-5px;">@lang('message.add_row')</button>                                             
                                                
                                            </div>

                                            <div class="tab-pane" id="core_value_english">
                                            <?php 
                                                    
                                                    @$core_value_en = json_decode(@$about_object->core_value_title_en);
                                                    if($core_value_en==null){
                                                        $total_core_value_en = 1;
                                                    }else{
                                                        $total_core_value_en = count(@$core_value_en); 
                                                    }
                                                    @$core_value_description_en = json_decode(@$about_object->core_value_description_en);
                                                    
                                                ?>
                                                    @for($i=0;$i<$total_core_value_en;$i++)
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">                                            
                                                            <textarea type="text" name="core_value_title_en[]" class="form-control col-sm-4 @if($errors->has('core_value_title_en[]')) has-error @endif" placeholder="Enter title">{{ @$core_value_en[$i] }}</textarea>
                                                                @if($errors->first('core_value_title_en[]'))
                                                                    <span class="text-danger">{{$errors->first('core_value_title_en[]')}}</span>
                                                                @endif
                                                            <textarea type="text" name="core_value_description_en[]"  class="form-control col-sm-7 @if($errors->has('core_value_description_en[]')) has-error @endif" style="margin-left:10px;" placeholder="Enter Description">{{ @$core_value_description_en[$i] }}</textarea>
                                                                @if($errors->first('core_value_description_en[]'))
                                                                    <span class="text-danger">{{$errors->first('core_value_description_en[]')}}</span>
                                                                @endif                                                                                                                                                                     
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="input-group-append col-sm-3 nopadding">                
                                                                    <button id="removeRow_eng" type="button" class="btn btn-danger">@lang('message.remove')</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endfor
                                                    <div class="clear"></div>         
                                                    <div id="newRow_eng"></div>                                               
                                                    <button id="addRow_eng" type="button" class="btn btn-info" style="margin-top:-5px;">@lang('message.add_row')</button>
                                                                                                                                      
                                            </div>                                            
                                        </div>                                   
                                    </div>

                                </div>

                            </div>
                            <div class="col-sm-8 col-lg-4">
                                <div class="card">
                                    <div class="card-header">#1 @lang('message.about_cover_images')</div>
                                    <div class="card-body">
                                        @lang('message.product_image_size') (1440x636) pixcel *
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-center">
                                                    <div class="file-box">
                                                        <div class="file">                                                           
                                                                <span class="corner"></span>                                
                                                                <div>
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">                                                                                       
                                                                                        @if(@$about_object->about_cover_image==null)
                                                                                        <img class="editable img-circle" src="{{ url('/images/no_img.png') }}" id="image-defalt" class="img-circle1">
                                                                                        @else
                                                                                        <img class="editable img-circle" src="{{ asset('upload/thumbnail/'.$about_object->about_cover_image) }}" id="image-defalt" class="img-circle1">
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> @lang('message.upload_images') </span>
                                                                                            {{ Form::file('thumbnail',['id'=>'images','accept'=>'image/*']) }}
                                    
                                                                                        </span>
                                                                                    </div>
                                                                                    <div id="image_message"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                 
                                                                </div>
                                                                
                                                        </div>
                                                    </div> 
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>          
                                
                                <div class="card">
                                    <div class="card-header">#2 @lang('message.our_mission_image')</div>
                                    <div class="card-body">
                                        @lang('message.product_image_size') (710x603) pixcel *
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-center">
                                                    <div class="file-box">
                                                        <div class="file">
                                                           
                                                                <span class="corner"></span>
                                
                                                                <div>
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        @if(@$about_object->our_mission_image==null)
                                                                                        <img class="editable img-circle" src="{{ url('/images/no_img.png') }}" id="image-defalt2" class="img-circle1">
                                                                                        @else
                                                                                        <img class="editable img-circle" src="{{ asset('upload/thumbnail/'.$about_object->our_mission_image) }}" id="image-defalt2" class="img-circle1">
                                                                                        @endif
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists2"> @lang('message.upload_images') </span>
                                                                                            {{ Form::file('thumbnail2',['id'=>'images2','accept'=>'image/*']) }}
                                    
                                                                                        </span>
                                                                                    </div>
                                                                                    <div id="image_message2"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                 
                                                                </div>
                                                                
                                                        </div>
                                                    </div>
                    
                    
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>   

                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.save')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <style>       
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .nopadding {
            padding: 0 !important;
            margin: 0 !important;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">
       
       $('.select2').select2({width: "100%"});

       $('.company_overview_kh').summernote({
        //placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200,
         toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //['fontname', ['fontname']],
        // ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
        // ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            //['view', ['fullscreen', 'codeview']],
            //['help', ['help']]
        ],
      });

       $('.company_overview_eng').summernote({
        //placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200,
         toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //['fontname', ['fontname']],
        // ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
        // ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            //['view', ['fullscreen', 'codeview']],
            //['help', ['help']]
        ],
      });
      $('.ourvission_kh').summernote({
        //placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200,
         toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //['fontname', ['fontname']],
        // ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
        // ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            //['view', ['fullscreen', 'codeview']],
            //['help', ['help']]
        ],
      });

      $('.ourvission_eng').summernote({
        //placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200,
         toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //['fontname', ['fontname']],
        // ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
        // ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            //['view', ['fullscreen', 'codeview']],
            //['help', ['help']]
        ],
      });

      
      $('.ourmission_eng').summernote({
        //placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200,
         toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //['fontname', ['fontname']],
        // ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
        // ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            //['view', ['fullscreen', 'codeview']],
            //['help', ['help']]
        ],
      });
      
      $('.ourmission_kh').summernote({
        //placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200,
         toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //['fontname', ['fontname']],
        // ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
        // ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            //['view', ['fullscreen', 'codeview']],
            //['help', ['help']]
        ],
      });

     //   $('.product_description').summernote();


     
        $("#images").change(function(e) {
            e.preventDefault();
            $("#image_message").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if( (width <= 1024 && height <= 1024) && ((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#image_message").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#image_message").html("<p class='text-danger'>Your image allow max size(1024x1024)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#image_message").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                $("#image_message").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });

        $("#images2").change(function(e) {
            e.preventDefault();
            $("#image_message2").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("images2").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if( (width <= 1024 && height <= 1024) && ((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#image_message2").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images2").css("color", "green");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt2').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt2').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#image_message2").html("<p class='text-danger'>Your image allow max size(1024x1024)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt2').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists2').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#image_message2").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                $("#image_message2").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });

        $("#addRow").click(function () {
            var html = '';           
            html += '<div class="col-sm-12 col-lg-12" style="margin-top:10px;" id="inputFormRow"><div class="form-group row">';
            html += '<textarea type="text" name="core_value_title_kh[]" class="form-control col-sm-4" placeholder="Enter title" autocomplete="off"></textarea>';
            html += '<textarea type="text" name="core_value_description_kh[]" class="form-control col-sm-7" style="margin-left:10px;" placeholder="Enter Description" autocomplete="off"></textarea>';
            html += '</div><div class="form-group row"><div class="input-group-append col-sm-3 nopadding">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">@lang('message.remove')</button>';
            html += '</div></div></div>';

            $('#newRow').append(html);
        });
        $("#addRow_eng").click(function () {
            var html = '';           
            html += '<div class="col-sm-12 col-md-12 col-lg-12" style="margin-top:10px;" id="inputFormRow"><div class="form-group row">';
            html += '<textarea type="text" name="core_value_title_en[]" class="form-control col-sm-4 col-md-4" placeholder="Enter title" autocomplete="off"></textarea>';
            html += '<textarea type="text" name="core_value_description_en[]" class="form-control col-sm-7 col-md-7" style="margin-left:10px;" placeholder="Enter Description" autocomplete="off"></textarea>';
            html += '</div><div class="form-group row"><div class="input-group-append col-sm-3 nopadding">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">@lang('message.remove')</button>';
            html += '</div></div></div>';

            $('#newRow_eng').append(html);
        });
        

        
        // remove row
        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });
        // remove row
        $(document).on('click', '#removeRow_eng', function () {
            $(this).closest('#inputFormRow_eng').remove();
        });

    </script>
@endsection
