@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('about.index') }}">@lang('message.aboutus')</a></li>
            <li class="breadcrumb-item active">@lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('about.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                <?php @$about_object = json_decode($about_list->about_object);?>
                {!! Form::open(array('route' => 'about.store','method'=>'POST','class'=>'form-horizontal','enctype'=>'multipart/form-data','id'=>'ImageForm')) !!}
                    <div class="card-header">@lang('message.add_new')</div>
                    <div class="card-body">
                    <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" id="about_informations" href="#about_information" data-toggle="tab">About Information</a></li> 
                        <li class="nav-item"><a class="nav-link" id="company_overview_" href="#company_overview" data-toggle="tab">Company Overview</a></li>
                        <li class="nav-item"><a class="nav-link" id="our_vission_" href="#our_vission" data-toggle="tab">Our vission</a></li>
                        <li class="nav-item"><a class="nav-link" id="our_mission_" href="#our_mission" data-toggle="tab">Our mission</a></li>
                        <li class="nav-item"><a class="nav-link" id="our_value_" href="#our_value" data-toggle="tab">Our value</a></li>
                        </ul>
                    </div><!-- /.card-header -->
                    
                    <div class="card-body">
                        <div class="tab-content">
                        <!-- /.tab-pane about_information-->
                            <div class="active tab-pane" id="about_information"> 

                                <div class="row">
                                     
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.about_title_kh')</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('about_title_kh')) has-error @endif">
                                                {!! Form::text('about_title_kh', @$about_object->about_title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                @if($errors->first('about_title_kh'))
                                                    <span class="text-danger">{{$errors->first('about_title_kh')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.about_title_en')</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('about_title_en')) has-error @endif">
                                                {!! Form::text('about_title_en', @$about_object->about_title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                @if($errors->first('about_title_en'))
                                                    <span class="text-danger">{{$errors->first('about_title_en')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                @can('page-create')
                                <div class="card-footer text-right">                                    
                                    <button class="btn btn-md btn-primary btn-square" type="button" id="next_to_company_overview"> @lang('message.next')</button>
                                </div>
                                @endcan

                            </div>
                        <!-- /.tab-pane -->
                        
                        <!-- /.tab-pane company overview-->
                            <div class="tab-pane" id="company_overview">  
                                <div class="row">
                                
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">Company Overview Khmer</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('company_overview_kh')) has-error @endif">
                                                {!! Form::textarea('company_overview_kh', @$about_object->company_overview_kh, array('placeholder' => '','class' => 'form-control company_overview')) !!}
                                                @if($errors->first('company_overview_kh'))
                                                    <span class="text-danger">{{$errors->first('company_overview_kh')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>                               
                                
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">Company Overview English</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('company_overview_eng')) has-error @endif">
                                                {!! Form::textarea('company_overview_eng', @$about_object->company_overview_eng, array('placeholder' => '','class' => 'form-control company_overview')) !!}
                                                @if($errors->first('company_overview_eng'))
                                                    <span class="text-danger">{{$errors->first('company_overview_eng')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <div class="control-label"><label for="nf-email">Cover Images</label></div>
                                            <input type="file" id="benefits_images" name="company_images">
                                            <div class="fileinput-news img-circle"> 
                                                @if(@$about_object->company_overview_images==null)
                                                <img class="editable img-circle" src="{{ url('/images/no_img.png') }}" id="image-defalt_benefits" style="height:120px;" class="img-circle1">
                                                @else
                                                <img class="editable img-circle" src="{{ asset('upload/thumbnail/'.$about_object->company_overview_images) }}" style="height:120px;" id="image-defalt_benefits" class="img-circle1">
                                                @endif
                                            </div>
                                            <!-- <p class="help-block" style="font-style: italic;color:red;">@lang('message.product_image_size') (756x756) pixcel *</p> -->
                                            <div id="images_benefits"></div>
                                            @if($errors->first('company_images'))
                                                <span class="text-danger">{{$errors->first('company_images')}}</span>
                                            @endif 
                                        </div>         
                                    </div>

                                </div>
                                @can('page-create')
                                 <div class="card-footer text-right">
                                    <button class="btn btn-md btn-primary btn-square" type="button" id="next_to_vission"> @lang('message.next')</button>
                                </div>
                                @endcan
                            </div>
                        <!-- /.tab-pane -->


                        <!-- /.tab-pane our vission-->
                            <div class="tab-pane" id="our_vission">
                                <div id="application_container">                                   
                                    <div class="row">

                                        <div class="col-sm-6 col-lg-6">
                                            <div class="form-group row">
                                                <div class="col-sm-12 control-label"><label for="nf-email">Our vission khmer</label> <span class="text-danger">*</span></div>
                                                <div class="col-sm-12 @if($errors->has('ourvission_kh')) has-error @endif">
                                                    {!! Form::textarea('ourvission_kh', @$about_object->ourvission_kh, array('placeholder' => '','class' => 'form-control company_overview')) !!}
                                                    @if($errors->first('ourvission_kh'))
                                                        <span class="text-danger">{{$errors->first('ourvission_kh')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-lg-6">
                                            <div class="form-group row">
                                                <div class="col-sm-12 control-label"><label for="nf-email">Our vission English</label> <span class="text-danger">*</span></div>
                                                <div class="col-sm-12 @if($errors->has('ourvission_eng')) has-error @endif">
                                                    {!! Form::textarea('ourvission_eng', @$about_object->ourvission_eng, array('placeholder' => '','class' => 'form-control company_overview')) !!}
                                                    @if($errors->first('ourvission_eng'))
                                                        <span class="text-danger">{{$errors->first('ourvission_eng')}}</span>
                                                    @endif
                                                </div>
                                            </div> 
                                        </div>  

                                    </div>
                                </div>         
                                @can('page-create')
                                <div class="card-footer text-right">
                                    <button class="btn btn-md btn-primary btn-square" type="button" id="next_to_mission"> @lang('message.next')</button>
                                </div>
                                @endcan
                            </div>
                        <!-- /.tab-pane -->

                        <!-- /.tab-pane our mission-->
                            <div class="tab-pane" id="our_mission">  
                                <div id="application_container">                                   
                                    <div class="row">

                                        <div class="col-sm-6 col-lg-6">
                                            <div class="form-group row">
                                                <div class="col-sm-12 control-label"><label for="nf-email">Our mission khmer</label> <span class="text-danger">*</span></div>
                                                <div class="col-sm-12 @if($errors->has('ourmission_kh')) has-error @endif">
                                                    {!! Form::textarea('ourmission_kh', @$about_object->ourmission_kh, array('placeholder' => '','class' => 'form-control company_overview')) !!}
                                                    @if($errors->first('ourmission_kh'))
                                                        <span class="text-danger">{{$errors->first('ourmission_kh')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-lg-6">
                                            <div class="form-group row">
                                                <div class="col-sm-12 control-label"><label for="nf-email">Our mission English</label> <span class="text-danger">*</span></div>
                                                <div class="col-sm-12 @if($errors->has('ourmission_eng')) has-error @endif">
                                                    {!! Form::textarea('ourmission_eng', @$about_object->ourmission_eng, array('placeholder' => '','class' => 'form-control company_overview')) !!}
                                                    @if($errors->first('ourmission_eng'))
                                                        <span class="text-danger">{{$errors->first('ourmission_eng')}}</span>
                                                    @endif
                                                </div>
                                            </div> 
                                        </div> 
                                                               
                                    </div>
                                
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <div class="control-label"><label for="nf-email">Our Mission</label></div>
                                            <input type="file" id="applications​_images" name="our_mission_images">   
                                            <div class="fileinput-news img-circle">  
                                                @if(@$about_object->our_mission_image==null)
                                                <img class="editable img-circle" src="{{ url('/images/no_img.png') }}" id="image-defalt_benefits" style="height:120px;" class="img-circle1">
                                                @else
                                                <img class="editable img-circle" src="{{ asset('upload/thumbnail/'.$about_object->our_mission_image) }}" style="height:120px;" id="image-defalt_benefits" class="img-circle1">
                                                @endif  
                                            </div>                                     
                                            <div id="images_applications"></div>
                                            @if($errors->first('our_mission_images'))
                                            <span class="text-danger">{{$errors->first('our_mission_images')}}</span>
                                            @endif
                                        </div> 
                                    </div> 

                                </div>
                                @can('page-create') 
                                <div class="card-footer text-right">
                                    <button class="btn btn-md btn-primary btn-square" type="button" id="next_to_value"> @lang('message.next')</button>
                                </div>
                                @endcan
                            </div>
                        <!-- /.tab-pane -->
                        <!-- /.tab-pane our value-->
                        <div class="tab-pane" id="our_value"> 
                                <?php 
                                    $core_object=@$about_object->core_value;
                                    
                                    @$core_value_en = json_decode(@$core_object->core_value_title_en);
                                    @$core_value_kh = json_decode(@$core_object->core_value_title_kh);

                                    if($core_value_en==null){
                                        $total_core_value_en = 1;
                                    }else{
                                        $total_core_value_en = count(@$core_value_en); 
                                    }
                                    @$core_value_description_kh = json_decode(@$core_object->core_value_description_kh);
                                    @$core_value_description_en = json_decode(@$core_object->core_value_description_en);
                                    
                                ?>
                              @for($i=0;$i<$total_core_value_en;$i++)
                              
                                <div id="our_value_container">                                   
                                        <div class="col-sm-12 col-lg-12">
                                            <div class="form-group row">
                                                <div class="col-sm-12 control-label"><label for="nf-email">Our value khmer</label> <span class="text-danger">*</span></div>                                   
                                                <textarea type="text" name="core_value_title_kh[]" class="form-control col-sm-4 @if($errors->has('core_value_title_kh')) has-error @endif" placeholder="Enter title">{{ @$core_value_kh[$i] }}</textarea>
                                                    @if($errors->first('core_value_title_kh'))
                                                        <span class="text-danger">{{$errors->first('core_value_title_kh')}}</span>
                                                    @endif
                                                
                                                <textarea type="text" name="core_value_description_kh[]" class="form-control col-sm-7 @if($errors->has('features_description_kh')) has-error @endif" style="margin-left:10px;" placeholder="Enter Description">{{ @$core_value_description_kh[$i] }}</textarea>
                                                    @if($errors->first('core_value_description_kh'))
                                                        <span class="text-danger">{{$errors->first('core_value_description_kh')}}</span>
                                                    @endif                                                                                                                                                   
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12 control-label"><label for="nf-email">Our value english</label> <span class="text-danger">*</span></div>                                   
                                                <textarea type="text" name="core_value_title_en[]" class="form-control col-sm-4 @if($errors->has('core_value_title_en')) has-error @endif" placeholder="Enter title">{{ @$core_value_en[$i] }}</textarea>
                                                    @if($errors->first('core_value_title_en'))
                                                        <span class="text-danger">{{$errors->first('core_value_title_en')}}</span>
                                                    @endif
                                                
                                                <textarea type="text" name="core_value_description_en[]" class="form-control col-sm-7 @if($errors->has('features_description_en')) has-error @endif" style="margin-left:10px;" placeholder="Enter Description">{{ @$core_value_description_en[$i] }}</textarea>
                                                    @if($errors->first('core_value_description_en'))
                                                        <span class="text-danger">{{$errors->first('core_value_description_en')}}</span>
                                                    @endif                                                                                                                                                   
                                            </div>

                                        </div>     
                                </div>
                                <hr/>
                                @endfor
                                
                                <div id="output-ourvalue_container"></div>  
                                <div class="row form-group col-sm-12" >
                                    <button id="addRow_ourvalue" type="button" class="btn btn-success" style="margin-left:5px;"><i class="fas fa-plus-circle"></i> Add New Row</button>
                                </div>
                                @can('page-create') 
                                    <div class="card-footer text-right">
                                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.save')</button>
                                    </div>
                                @endcan
                            </div>
                        <!-- /.tab-pane -->
                        
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>    


                    </div>
                   
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
<style>
    .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
        color: #fff;
        background-color: #3399ffcf !important;
    }
    .note-editable{
            min-height: 200px !important;
        }
    .editable{
        max-width: 340px;
        object-fit:cover;
        border-radius: 5px;
    }
    .editable img{ max-width: 100%;}
</style>

@endsection
@section('scripts')
    <script type="text/javascript">
        /** Add Form aplication */
        
        $("#addRow_ourvalue").click(function(){
            $('#our_value_container').addClass('our_value_container');            
            $("#our_value_container")
                .each(function () {
            // "this" is the current child
            // in the loop grabbing this 
            // element in the form of string
            // and appending it to the 
            // "#output-container" div
            var element = ' <div id="our_value_container"><div class="col-sm-12 col-lg-12"><div class="form-group row"><div class="col-sm-12 control-label"><label for="nf-email">Our value khmer</label> <span class="text-danger">*</span></div><textarea type="text" name="core_value_title_kh[]" class="form-control col-sm-4 @if($errors->has('core_value_title_kh')) has-error @endif" placeholder="Enter title"></textarea>@if($errors->first('core_value_title_kh'))<span class="text-danger">{{$errors->first('core_value_title_kh')}}</span>@endif<textarea type="text" name="core_value_description_kh[]" class="form-control col-sm-7 @if($errors->has('features_description_kh')) has-error @endif" style="margin-left:10px;" placeholder="Enter Description"></textarea>@if($errors->first('core_value_description_kh'))<span class="text-danger">{{$errors->first('core_value_description_kh')}}</span>@endif</div><div class="form-group row"><div class="col-sm-12 control-label"><label for="nf-email">Our value english</label> <span class="text-danger">*</span></div><textarea type="text" name="core_value_title_en[]" class="form-control col-sm-4 @if($errors->has('core_value_title_en')) has-error @endif" placeholder="Enter title"></textarea>@if($errors->first('core_value_title_en'))<span class="text-danger">{{$errors->first('core_value_title_en')}}</span>@endif<textarea type="text" name="core_value_description_en[]" class="form-control col-sm-7 @if($errors->has('features_description_en')) has-error @endif" style="margin-left:10px;" placeholder="Enter Description">{{ @$core_value_description_en[$i] }}</textarea>@if($errors->first('core_value_description_en'))<span class="text-danger"></span>@endif</div></div></div>';      
            
            $("#output-ourvalue_container")
                    .append(element);
            });
        });
        /**Remove container for features */
        $(document).on('click', '#removeRow_features', function () {
            $(this).closest('.features_container').remove();
        });

        /**Next to benefits*/
        $('#next_to_company_overview').click(function(e){            
            $('#about_information').removeClass('active');            
            $('#about_informations').removeClass('active'); 
            $('#company_overview_').addClass('active');
            $('#company_overview').addClass('active');
        });

        /**Next to next_to_applications*/
        $('#next_to_vission').click(function(){
            $('#company_overview_').removeClass('active');
            $('#company_overview').removeClass('active');
            $('#our_vission_').addClass('active');
            $('#our_vission').addClass('active');
        });

        /**Next to next_to_features*/
        $('#next_to_mission').click(function(){
            $('#our_vission_').removeClass('active');
            $('#our_vission').removeClass('active');
            $('#our_mission_').addClass('active');
            $('#our_mission').addClass('active');
        });
        /** Next to next_to_value */
        $('#next_to_value').click(function(){
            $('#our_mission_').removeClass('active');
            $('#our_mission').removeClass('active');
            $('#our_value_').addClass('active');
            $('#our_value').addClass('active');
        });
        /**Editor for benefits */
        $('.company_overview').summernote({
            //placeholder: 'Hello bootstrap 4',
            tabsize: 2,
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                //['fontname', ['fontname']],
            // ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                //['height', ['height']],
            // ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                //['view', ['fullscreen', 'codeview']],
                //['help', ['help']]
            ],
        });
        /**Image for thumbnail image */
        $("#thumbnail_images").change(function(e) {
            e.preventDefault();
            $("#images_thumbnail").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("thumbnail_images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if( (width == 710 && height == 400) && ((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#images_thumbnail").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                $(".img-circle").css("display","block");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#images_thumbnail").html("<p class='text-danger'>Your image allow max size(710X400)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#images_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                $("#images_thumbnail").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });
        /**Cover image */
        $("#conver_images").change(function(e) {
            e.preventDefault();
            $("#images_convers").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("conver_images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#images_convers").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                $(".img-circle").css("display","block");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt_convers').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt_convers').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#images_convers").html("<p class='text-danger'>Your image allow max size(710X400)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt_convers').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#images_convers").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt_convers').attr('src', '{{asset('/')}}images/no_img.png');
                $("#images_convers").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });
        /**Image for benefits */
        $("#benefits_images").change(function(e) {
            e.preventDefault();
            $("#images_benefits").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("benefits_images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#images_benefits").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                $(".img-circle").css("display","block");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt_benefits').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt_benefits').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#images_benefits").html("<p class='text-danger'>Your image allow max size(756X756)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt_benefits').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#images_benefits").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt_benefits').attr('src', '{{asset('/')}}images/no_img.png');
                $("#images_benefits").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });
        /**Image for application icon */
        $("#applications​_images").change(function(e) {
            e.preventDefault();
            $("#images_benefits").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("applications​_images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#images_applications").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                $(".img-circle").css("display","block");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt_applications').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            // $('#image-defalt_applications').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#images_applications").html("<p class='text-danger'>Your image allow max size(66X64)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    // $('#image-defalt_applications').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#images_applications").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                // $('#image-defalt_applications').attr('src', '{{asset('/')}}images/no_img.png');
                $("#images_applications").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });
        /**Image for features */
        $("#features_images").change(function(e) {
            e.preventDefault();
            $("#images_features").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("features_images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#images_features").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images_features").css("color", "green");
                                $(".img-circle").css("display","block");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt_features').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;                           
                            $("#images_features").html("<p class='text-danger'>Your image allow max size(1440X636)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {                   
                    
                    document.getElementById("submit1").disabled = true;
                    $("#images_features").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                // $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                $("#image_message").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });


    </script>
@endsection

