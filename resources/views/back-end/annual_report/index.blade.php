@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item active">@lang('message.annual_report')</li>
        </ol>
        @can('page-create')
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('annual_report.create') }}">@lang('message.add_new')</a>
        </div>
        @endcan
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body">
                        <table class="table table-responsive-sm" id="data-productlist">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('message.title') (@lang('message.english'))</th>
                                    <th>@lang('message.title') (@lang('message.khmer'))</th>
                                    <th>@lang('message.date')</th>
                                    <th>@lang('message.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog">
            <form action="" id="deleteForm" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('message.delete_information')</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <p class="text-center">@lang('message.are_you_sure_do_you_want_to_delete')</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-square" data-dismiss="modal">@lang('message.cancel')</button>
                        <button type="submit"  class="btn btn-danger btn-square" data-dismiss="modal" onclick="formSubmit()">@lang('message.yes_delete')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection


@section('scripts')

    <script type="text/javascript">

        var url = "{{route('annual_report.getAnnualReportList')}}";
        $(document).ready(function() {
            var dataUser =$('#data-productlist').DataTable({
                "language": {
                    "lengthMenu": "@lang('message.display') _MENU_ @lang('message.records_per_page')",
                    "zeroRecords": "@lang('message.no_matching_records_found')",
                    "info": "@lang('message.showing_page') _PAGE_ @lang('message.of') _PAGES_",
                    "infoEmpty": "@lang('message.no_records_available')",
                    "infoFiltered": "(@lang('message.filtered_from') _MAX_ @lang('message.total_records'))",
                    "search": "@lang('message.search')",
                    "paginate": {
                        "previous": "@lang('message.previous')",
                        "next": "@lang('message.next')"
                    }
                },
                "processing": true,
                "serverSide": true,
                // "retrieve": true,
                "bLengthMenu" : true, //thought this line could hide the LengthMenu
                "bInfo":true,
                //"length":10,
                "pageLength": 10,
                "autoWidth": false,
                "scrollX": true,
                "ajax": url,
                "columns": [
                    {
                        "render": function (data, type, aa){
                            return aa.DT_RowIndex;
                        },
                    },                   
                    {data: 'title_en',name: 'title_en'},
                    {data: 'title_kh',name: 'title_kh'},
                    {data: 'date',name: 'date'},
                    {data: 'action',name: 'action'},
                ],
                'columnDefs': [
                    {targets: [0],"className": "text-center"},
                  //   { targets: '_all', "className": "small " },
                    { "orderable": true},
                ],
            });
        });

        function deleteData(id)
        {
            var id = id;
            var url = '{{ route("annual_report.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#deleteForm").attr('action', url);
        }

        function formSubmit()
        {
            $("#deleteForm").submit();
        }
    </script>
@endsection
