@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('contact.index') }}">@lang('message.contactus')</a></li>
            <li class="breadcrumb-item active">@lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('contact.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <?php @$contact_object = json_decode($contact_list->contactus_object);?>
                    {!! Form::open(array('route' => 'contact.store','method'=>'POST','class'=>'form-horizontal','enctype'=>'multipart/form-data','id'=>'ImageForm')) !!}
                    <div class="card-header">@lang('message.add_new')</div>
                    <div class="card-body">

                        <div class="row">

                            <div class="col-sm-8 col-lg-8">

                                <div class="card">
                                    <div class="card-header">@lang('message.contactus')</div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.business_name_khmer')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('business_name_khmer')) has-error @endif">
                                                        {!! Form::text('business_name_khmer', @$contact_object->business_name_khmer, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('business_name_khmer'))
                                                            <span class="text-danger">{{$errors->first('business_name_khmer')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.business_name_english')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('business_name_english')) has-error @endif">
                                                        {!! Form::text('business_name_english', @$contact_object->business_name_english, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('business_name_english'))
                                                            <span class="text-danger">{{$errors->first('business_name_english')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.business_type_kh')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('business_type_kh')) has-error @endif">
                                                        {!! Form::text('business_type_kh', @$contact_object->business_type_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('business_type_kh'))
                                                            <span class="text-danger">{{$errors->first('business_type_kh')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.business_type_en')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('business_type_en')) has-error @endif">
                                                        {!! Form::text('business_type_en', @$contact_object->business_type_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('business_type_en'))
                                                            <span class="text-danger">{{$errors->first('business_type_en')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.physical_address_kh')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('physical_address_kh')) has-error @endif">
                                                        {!! Form::text('physical_address_kh', @$contact_object->physical_address_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('physical_address_kh'))
                                                            <span class="text-danger">{{$errors->first('physical_address_kh')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.physical_address_en')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('physical_address_en')) has-error @endif">
                                                        {!! Form::text('physical_address_en', @$contact_object->physical_address_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('physical_address_en'))
                                                            <span class="text-danger">{{$errors->first('physical_address_en')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.email_address')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('email_address')) has-error @endif">
                                                        {!! Form::text('email_address', @$contact_object->email_address, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('email_address'))
                                                            <span class="text-danger">{{$errors->first('email_address')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.facebook_page')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('facebook_page')) has-error @endif">
                                                        {!! Form::text('facebook_page', @$contact_object->facebook_page, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('facebook_page'))
                                                            <span class="text-danger">{{$errors->first('facebook_page')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.google_map')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('google_map')) has-error @endif">
                                                        {!! Form::text('google_map', @$contact_object->google_map, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('google_map'))
                                                            <span class="text-danger">{{$errors->first('google_map')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header p-2">
                                        <ul class="nav nav-pills">
                                        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">@lang('message.company_definitions_kh')</a></li> 
                                        <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">@lang('message.company_definitions_en')</a></li>
                                        </ul>
                                    </div><!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="tab-content">
                                        <!-- /.tab-pane -->
                                            <div class="active tab-pane" id="activity">                                       
                                                <div class="row">
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 @if($errors->has('event_description_kh')) has-error @endif">
                                                                {!! Form::textarea('event_description_kh', @$contact_object->event_description_kh, array('placeholder' => '','class' => 'form-control event_description_kh')) !!}
                                                                @if($errors->first('event_description_kh'))
                                                                    <span class="text-danger">{{$errors->first('event_description_kh')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <!-- /.tab-pane -->
                                        
                                            <div class="tab-pane" id="settings">                                        
                                                <div class="row">
                                                    <div class="col-sm-12 col-lg-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 @if($errors->has('event_description_en')) has-error @endif">
                                                                {!! Form::textarea('event_description_en', @$contact_object->event_description_en, array('placeholder' => '','class' => 'form-control event_description_en')) !!}
                                                                @if($errors->first('event_description_en'))
                                                                    <span class="text-danger">{{$errors->first('event_description_en')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>

                              

                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="card">
                                    <div class="card-header">#1 @lang('message.contact_cover_image')</div>
                                    <div class="card-body">
                                        @lang('message.product_image_size') (1440x636) pixcel *
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="text-center">


                                                    <div class="file-box">
                                                        <div class="file">
                                                           
                                                                <span class="corner"></span>
                                
                                                                <div>
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        @if(@$contact_object->contact_conver_image==null)
                                                                                        <img class="editable img-circle" src="{{ url('/images/no_img.png') }}" id="image-defalt" class="img-circle1">
                                                                                        @else
                                                                                        <img class="editable img-circle" src="{{ asset('upload/thumbnail/'.$contact_object->contact_conver_image) }}" id="image-defalt" class="img-circle1">
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> @lang('message.upload_images') </span>
                                                                                            {{ Form::file('thumbnail',['id'=>'images','accept'=>'image/*']) }}
                                    
                                                                                        </span>
                                                                                    </div>
                                                                                    <div id="image_message"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                 
                                                                </div>
                                                                
                                                        </div>
                                                    </div>
                    
                    
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>          
                                
                               

                            </div>

                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.save')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }
       
        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">
       
       $('.select2').select2({width: "100%"});

       $('.event_description_en').summernote({
        //placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200,
         toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //['fontname', ['fontname']],
        // ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
        // ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            //['view', ['fullscreen', 'codeview']],
            //['help', ['help']]
        ],
      });

       $('.event_description_kh').summernote({
        //placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200,
         toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //['fontname', ['fontname']],
        // ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
        // ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            //['view', ['fullscreen', 'codeview']],
            //['help', ['help']]
        ],
      });
     //   $('.product_description').summernote();


     
        $("#images").change(function(e) {
            e.preventDefault();
            $("#image_message").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if( (width <= 1024 && height <= 1024) && ((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#image_message").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#image_message").html("<p class='text-danger'>Your image allow max size(1024x1024)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#image_message").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                $("#image_message").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });

        $("#images2").change(function(e) {
            e.preventDefault();
            $("#image_message2").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("images2").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if( (width <= 1024 && height <= 1024) && ((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#image_message2").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images2").css("color", "green");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt2').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt2').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#image_message2").html("<p class='text-danger'>Your image allow max size(1024x1024)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt2').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists2').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#image_message2").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                $("#image_message2").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });

        $("#addRow").click(function () {
            var html = '';           
            html += '<div class="col-sm-12 col-lg-12" style="margin-top:10px;" id="inputFormRow"><div class="form-group row">';
            html += '<input type="text" name="features_title_kh[]" class="form-control col-sm-4" placeholder="Enter title" autocomplete="off">';
            html += '<input type="text" name="features_description_en[]" class="form-control col-sm-4" style="margin-left:10px;" placeholder="Enter Description" autocomplete="off">';
            html += '<div class="input-group-append col-sm-3">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">@lang('message.remove')</button>';
            html += '</div></div></div>';

            $('#newRow').append(html);
        });
        $("#addRow_eng").click(function () {
            var html = '';           
            html += '<div class="col-sm-12 col-md-12 col-lg-12" style="margin-top:10px;" id="inputFormRow"><div class="form-group row">';
            html += '<input type="text" name="features_title_kh[]" class="form-control col-sm-4 col-md-4" placeholder="Enter title" autocomplete="off">';
            html += '<input type="text" name="features_description_en[]" class="form-control col-sm-4 col-md-4" style="margin-left:10px;" placeholder="Enter Description" autocomplete="off">';
            html += '<div class="input-group-append col-sm-3">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">@lang('message.remove')</button>';
            html += '</div></div></div>';

            $('#newRow_eng').append(html);
        });
        

        
        // remove row
        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });
        // remove row
        $(document).on('click', '#removeRow_eng', function () {
            $(this).closest('#inputFormRow_eng').remove();
        });

    </script>
@endsection
