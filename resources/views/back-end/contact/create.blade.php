@extends('layouts.app')
@section('breadcrumb')
      <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('contact.index') }}">@lang('message.contactus')</a></li>
            <li class="breadcrumb-item active">@lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('contact.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                 <?php @$contact_object = json_decode($contact_list->contactus_object);?>
                    {!! Form::open(array('route' => 'contact.store','method'=>'POST','class'=>'form-horizontal','enctype'=>'multipart/form-data','id'=>'ImageForm')) !!}
                    <div class="card-header">@lang('message.add_new')</div>
                    <div class="card-body">
                        <div class="card">
                            <div class="card-header">
                                <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link active" id="company_definitions" href="#company_definition" data-toggle="tab">Company Definitions</a></li> 
                                    <li class="nav-item"><a class="nav-link" id="get_in_touch_" href="#get_in_touch" data-toggle="tab">Get In Touch</a></li>
                                </ul>
                            </div><!-- /.card-header -->
                            
                            <div class="card-body">
                                <div class="tab-content">
                                <!-- /.tab-pane product_information-->
                                    <div class="active tab-pane" id="company_definition"> 

                                        <div class="row">
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 @if($errors->has('event_description_kh')) has-error @endif">
                                                        {!! Form::textarea('company_definition_kh', @$contact_object->company_definition_kh, array('placeholder' => '','class' => 'form-control event_description')) !!}
                                                        @if($errors->first('company_definition_kh'))
                                                            <span class="text-danger">{{$errors->first('company_definition_kh')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 @if($errors->has('event_description_en')) has-error @endif">
                                                        {!! Form::textarea('company_definition_en', @$contact_object->company_definition_en, array('placeholder' => '','class' => 'form-control event_description')) !!}
                                                        @if($errors->first('company_definition_en'))
                                                            <span class="text-danger">{{$errors->first('company_definition_en')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="form-group">
                                                    <div class="control-label"><label for="nf-email">Conver Images</label></div>
                                                    <input type="file" id="conver_images" name="conver_images">
                                                    <div class="fileinput-news img-circle">
                                                        <br/>
                                                        <img class="editable img-circle" src="{{ asset('upload/'.@$contact_object->contact_conver_image) }}" style="height:120px;" id="image-defalt_convers" class="img-circle1">
                                                    </div>
                                                    <!-- <p class="help-block" style="font-style: italic;color:red;">@lang('message.product_image_size') (710x603) pixcel *</p> -->
                                                    <div id="images_convers"></div>
                                                    @if($errors->first('conver_images'))
                                                        <span class="text-danger">{{$errors->first('conver_images')}}</span>
                                                    @endif 
                                                </div>
                                            </div>
                                        </div>
                                        @can('page-create') 
                                        <div class="card-footer text-right">                                    
                                            <button class="btn btn-md btn-primary btn-square" type="button" id="next_to_touch"> @lang('message.next')</button>
                                        </div>
                                        @endcan
                                    </div>
                                <!-- /.tab-pane -->
                                
                                <!-- /.tab-pane benefits-->
                                    <div class="tab-pane" id="get_in_touch">  
                                        <div class="row">

                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.business_name_khmer')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('business_name_khmer')) has-error @endif">
                                                        {!! Form::text('business_name_khmer', @$contact_object->business_name_khmer, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('business_name_khmer'))
                                                            <span class="text-danger">{{$errors->first('business_name_khmer')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>  

                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.business_name_english')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('business_name_english')) has-error @endif">
                                                        {!! Form::text('business_name_english', @$contact_object->business_name_english, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('business_name_english'))
                                                            <span class="text-danger">{{$errors->first('business_name_english')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>   

                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.business_type_kh')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('business_type_kh')) has-error @endif">
                                                        {!! Form::text('business_type_kh', @$contact_object->business_type_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('business_type_kh'))
                                                            <span class="text-danger">{{$errors->first('business_type_kh')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>                                 
                                    
                                        </div>

                                        <div class="row">
                                        
                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.business_type_en')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('business_type_en')) has-error @endif">
                                                        {!! Form::text('business_type_en', @$contact_object->business_type_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('business_type_en'))
                                                            <span class="text-danger">{{$errors->first('business_type_en')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>  

                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.physical_address_kh')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('physical_address_kh')) has-error @endif">
                                                        {!! Form::text('physical_address_kh', @$contact_object->physical_address_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('physical_address_kh'))
                                                            <span class="text-danger">{{$errors->first('physical_address_kh')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>   

                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.physical_address_en')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('physical_address_en')) has-error @endif">
                                                        {!! Form::text('physical_address_en', @$contact_object->physical_address_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('physical_address_en'))
                                                            <span class="text-danger">{{$errors->first('physical_address_en')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>                                 
                                        </div>
                                        <div class="row">
                                        
                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.email_address')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('email_address')) has-error @endif">
                                                        {!! Form::text('email_address', @$contact_object->email_address, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('email_address'))
                                                            <span class="text-danger">{{$errors->first('email_address')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>  

                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.facebook_page')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('facebook_page')) has-error @endif">
                                                        {!! Form::text('facebook_page', @$contact_object->facebook_page, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('facebook_page'))
                                                            <span class="text-danger">{{$errors->first('facebook_page')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>   

                                            <div class="col-sm-4 col-lg-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.google_map')</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('google_map')) has-error @endif">
                                                        {!! Form::text('google_map', @$contact_object->google_map, array('placeholder' => '','class' => 'form-control')) !!}
                                                        @if($errors->first('google_map'))
                                                            <span class="text-danger">{{$errors->first('google_map')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>   

                                        </div>
                                        <div class="row">
                                        
                                            <div class="col-sm-8 col-lg-8">
                                                <div class="form-group row">
                                                    <div class="col-sm-12 control-label"><label for="nf-email">TELEPHONE No.</label> <span class="text-danger">*</span></div>
                                                    <div class="col-sm-12 @if($errors->has('telephone_no')) has-error @endif">
                                                        {!! Form::textarea('telephone_no', @$contact_object->telephone_no, array('placeholder' => '','class' => 'form-control','rows'=>3)) !!}
                                                        @if($errors->first('telephone_no'))
                                                            <span class="text-danger">{{$errors->first('telephone_no')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>
                                        @can('page-create') 
                                        <div class="card-footer text-right">
                                            <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.save')</button>
                                        </div>
                                        @endcan
                                    </div>
                                <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>    
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
<style>
    .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
        color: #fff;
        background-color: #3399ffcf !important;
    }
    .note-editable{
            min-height: 200px !important;
        }
    .editable{
        max-width: 340px;
        object-fit:cover;
        border-radius: 5px;
    }
    .editable img{ max-width: 100%;}
</style>

@endsection
@section('scripts')
    <script type="text/javascript">
        /**Next to benefits*/
        $('#next_to_touch').click(function(e){            
            $('#company_definition').removeClass('active');
            $('#company_definitions').removeClass('active');
            $('#get_in_touch').addClass('active');
            $('#get_in_touch_').addClass('active');
        });

       
        /**Editor for benefits */
        $('.event_description').summernote({
            //placeholder: 'Hello bootstrap 4',
            tabsize: 2,
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                //['fontname', ['fontname']],
            // ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                //['height', ['height']],
            // ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                //['view', ['fullscreen', 'codeview']],
                //['help', ['help']]
            ],
        });

        /**Image for thumbnail image */
        $("#thumbnail_images").change(function(e) {
            e.preventDefault();
            $("#images_thumbnail").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("thumbnail_images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if( (width == 400 && height == 250) && ((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#images_thumbnail").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                $(".img-circle").css("display","block");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#images_thumbnail").html("<p class='text-danger'>Your image allow max size(400X250)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#images_thumbnail").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt').attr('src', '{{asset('/')}}images/no_img.png');
                $("#images_thumbnail").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });

        /**Cover image */
        $("#conver_images").change(function(e) {
            e.preventDefault();
            $("#images_convers").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("conver_images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#images_convers").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                $(".img-circle").css("display","block");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt_convers').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt_convers').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#images_convers").html("<p class='text-danger'>Your image allow max size(710X400)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt_convers').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#images_convers").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt_convers').attr('src', '{{asset('/')}}images/no_img.png');
                $("#images_convers").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });

        /**Image for benefits */
        $("#benefits_images").change(function(e) {
            e.preventDefault();
            $("#images_benefits").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("benefits_images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

           // console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#images_benefits").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                $(".img-circle").css("display","block");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt_benefits').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt_benefits').attr('src', '{{asset('/')}}images/no_img.png');
                            $("#images_benefits").html("<p class='text-danger'>Your image allow max size(756X756)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt_benefits').attr('src', '{{asset('/')}}images/no_img.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}images/no_img.png');
                    document.getElementById("submit1").disabled = true;
                    $("#images_benefits").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt_benefits').attr('src', '{{asset('/')}}images/no_img.png');
                $("#images_benefits").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });
       
        /**Date picker */
        $('.newsEventsDate').datepicker('setDate', new Date());

    </script>
@endsection

