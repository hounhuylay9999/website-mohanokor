@extends('layouts.app')
@section('breadcrumb')
<div class="c-subheader px-3">
    <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item active">@lang('message.general')</li>
    </ol>
</div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            {!! Form::open(array('route' => 'general.store','method'=>'POST','class'=>'form-horizontal form-box','enctype'=>'multipart/form-data')) !!}

            <div class="col-md-12 mb-4">
                <div class="nav-tabs-boxed nav-tabs-boxed-left">
                    <ul class="nav nav-tabs mm-left" role="tablist" style="width: 200px">
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#logo" role="tab" aria-controls="logo" aria-selected="true">Logo</a></li>
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Header</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Footer</a></li>
                        {{-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a></li> --}}
                        {{-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#product" role="tab" aria-controls="product" aria-selected="false">Product</a></li> --}}
                        {{-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="false">About Us</a></li> --}}
                        {{-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#team" role="tab" aria-controls="team" aria-selected="false">Contact Us</a></li> --}}
                        {{-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="false">Media</a></li> --}}
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="logo" role="tabpanel">
                             <!-- logo -->
                             <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#logo_1" role="tab" aria-controls="logo_1" aria-selected="true"><span class="back-lang">English</span> <i class="c-icon c-icon-2xl cif-gb"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#logo_2" role="tab" aria-controls="logo_2" aria-selected="false"><span class="back-lang">Khmer</span> <i class="c-icon c-icon-2xl cif-kh"></i></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="logo_1" role="tabpanel">
                                            <div class="row">

                                                <div class="col-md-4 mb-4">
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="editable img-circle img-circle1 image-defalt" src="@if(isset($general_en->header_logo) == null || $general_en->header_logo == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$general_en->header_logo) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Logo (560x300)pixcel</span>
                                                                                            {{ Form::file('header_logo',['class'=>'header_logo','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 mb-4">
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="editable img-circle image-defalt-favicon img-circle1" src="@if(isset($general_en->favicon) == null || $general_en->favicon == ''){{ url('/assets/img/no_img.png') }}@else{{ asset('upload/'.$general_en->favicon) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Favicon (60x60)Pixcel</span>
                                                                                            {{ Form::file('favicon',['class'=>'favicon','accept'=>'image/*']) }}
                                    
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_1"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 mb-4">
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="editable img-circle image-defalt-footer img-circle1" src="@if(isset($general_en->footer_logo) == null || $general_en->footer_logo == ''){{ url('/assets/img/no_img.png') }}@else{{ asset('upload/'.$general_en->footer_logo) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Footer Logo (192x192)Pixcel</span>
                                                                                            {{ Form::file('footer_logo',['class'=>'footer-logo','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_2"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12"><hr></div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Copyright </label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('copyright_en')) has-error @endif">
                                                            {!! Form::text('copyright_en', $general_en->footer_copyright, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('copyright_en'))
                                                                <span class="text-danger">{{$errors->first('copyright_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Exclusive Distributor</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('footer_title_en')) has-error @endif">
                                                            {!! Form::text('footer_title_en', $general_en->footer_title, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('footer_title_en'))
                                                                <span class="text-danger">{{$errors->first('footer_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="logo_2" role="tabpanel">
                                            
                                            <div class="row">

                                                <div class="col-md-4 mb-4">
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="editable img-circle img-circle1 image-defalt-kh" src="@if(isset($general_en->header_logo) == null || $general_en->header_logo == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$general_en->header_logo) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Logo (560x300)pixcel</span>
                                                                                            {{ Form::file('header_logo_kh',['class'=>'header_logo_kh','accept'=>'image/*']) }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_kh"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 mb-4">
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="editable img-circle image-defalt-favicon-kh img-circle1" src="@if(isset($general_en->favicon) == null || $general_en->favicon == ''){{ url('/assets/img/no_img.png') }}@else{{ asset('upload/'.$general_en->favicon) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Favicon (60x60)Pixcel</span>
                                                                                            {{ Form::file('favicon_kh',['class'=>'favicon-kh','accept'=>'image/*']) }}
                                    
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_1_kh"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 mb-4">
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <div class="logo-thumnail">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-news img-circle">
                                                                                        <img class="editable img-circle image-defalt-footer-kh img-circle1" src="@if(isset($general_en->footer_logo) == null || $general_en->footer_logo == ''){{ url('/assets/img/no_img.png') }}@else{{ asset('upload/'.$general_en->footer_logo) }}@endif">
                                                                                    </div>
                                                                                    <div class="mt-3">
                                                                                        <span class="btn btn-square btn-info btn-file">
                                                                                            <span class="fileinput-exists"> Footer Logo (192x192)Pixcel</span>
                                                                                            {{ Form::file('footer_logo_kh',['class'=>'footer-logo-kh','accept'=>'image/*']) }}
                                    
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="image_message_2_kh"></div>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12"><hr></div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Copyright </label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('copyright_kh')) has-error @endif">
                                                            {!! Form::text('copyright_kh', $general_kh->footer_copyright, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('copyright_kh'))
                                                                <span class="text-danger">{{$errors->first('copyright_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Exclusive Distributor</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('footer_title_kh')) has-error @endif">
                                                            {!! Form::text('footer_title_kh', $general_kh->footer_title, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('footer_title_kh'))
                                                                <span class="text-danger">{{$errors->first('footer_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- logo -->
                            
                        </div>

                        <div class="tab-pane active" id="home" role="tabpanel">
                            <!-- header -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home_1" role="tab" aria-controls="home_1" aria-selected="true"><span class="back-lang">Egnlish</span> <i class="c-icon c-icon-2xl cif-gb"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile_1" role="tab" aria-controls="profile_1" aria-selected="false"><span class="back-lang">Khmer</span> <i class="c-icon c-icon-2xl cif-kh"></i></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="home_1" role="tabpanel">
                                            
                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.phone_number')</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('phone_number_en')) has-error @endif">
                                                            {!! Form::text('phone_number_en', $general_en->header_phone, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('phone_number_en'))
                                                                <span class="text-danger">{{$errors->first('phone_number_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.email')</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('email_en')) has-error @endif">
                                                            {!! Form::text('email_en', $general_en->header_email, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('email_en'))
                                                                <span class="text-danger">{{$errors->first('email_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.fb_url') </label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('facebook_url_en')) has-error @endif">
                                                            {!! Form::url('facebook_url_en', $general_en->header_fb_url, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('facebook_url_en'))
                                                                <span class="text-danger">{{$errors->first('facebook_url_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.youtube_url')  </label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('youtube_url_en')) has-error @endif">
                                                            {!! Form::url('youtube_url_en', $general_en->header_yt_url, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('youtube_url_en'))
                                                                <span class="text-danger">{{$errors->first('youtube_url_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="profile_1" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.phone_number')</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('phone_number_kh')) has-error @endif">
                                                            {!! Form::text('phone_number_kh', $general_kh->header_phone, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('phone_number_kh'))
                                                                <span class="text-danger">{{$errors->first('phone_number_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.email')</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('email_kh')) has-error @endif">
                                                            {!! Form::text('email_kh', $general_kh->header_email, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('email_kh'))
                                                                <span class="text-danger">{{$errors->first('email_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.fb_url') </label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('facebook_url_kh')) has-error @endif">
                                                            {!! Form::url('facebook_url_kh', $general_kh->header_fb_url, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('facebook_url_kh'))
                                                                <span class="text-danger">{{$errors->first('facebook_url_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.youtube_url')</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('youtube_url_kh')) has-error @endif">
                                                            {!! Form::url('youtube_url_kh', $general_kh->header_yt_url, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('youtube_url_kh'))
                                                                <span class="text-danger">{{$errors->first('youtube_url_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- header -->
                        </div>
                        <div class="tab-pane" id="profile" role="tabpanel">
                            <!-- footer -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#footer_1" role="tab" aria-controls="footer_1" aria-selected="true"><span class="back-lang">Egnlish</span> <i class="c-icon c-icon-2xl cif-gb"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#footer_2" role="tab" aria-controls="footer_2" aria-selected="false"><span class="back-lang">Khmer</span> <i class="c-icon c-icon-2xl cif-kh"></i></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="footer_1" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Head Office Title</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('head_ofiice_title')) has-error @endif">
                                                            {!! Form::text('head_ofiice_title_en', $general_en->footer_head, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('head_ofiice_title_en'))
                                                                <span class="text-danger">{{$errors->first('head_ofiice_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Head Office Desc</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('head_office_desc_en')) has-error @endif">
                                                            {!! Form::text('head_office_desc_en', $general_en->footer_head_desc, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('head_office_desc_en'))
                                                                <span class="text-danger">{{$errors->first('head_office_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Branch Title</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('branch_title_en')) has-error @endif">
                                                            {!! Form::text('branch_title_en', $general_en->footer_branch, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('branch_title_en'))
                                                                <span class="text-danger">{{$errors->first('branch_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Branch Desc</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('branch_desc_en')) has-error @endif">
                                                            {!! Form::text('branch_desc_en', $general_en->footer_branch_desc, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('branch_desc_en'))
                                                                <span class="text-danger">{{$errors->first('branch_desc_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Call Us Title</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('call_us_title_en')) has-error @endif">
                                                            {!! Form::text('call_us_title_en', $general_en->footer_call, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('call_us_title_en'))
                                                                <span class="text-danger">{{$errors->first('call_us_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Call Us Number 1</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('call_us_number_1_en')) has-error @endif">
                                                            {!! Form::text('call_us_number_1_en',  $general_en->footer_call_1, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('call_us_number_1_en'))
                                                                <span class="text-danger">{{$errors->first('call_us_number_1_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Call Us Number 2</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('call_us_number_2_en')) has-error @endif">
                                                            {!! Form::text('call_us_number_2_en', $general_en->footer_call_2, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('call_us_number_2_en'))
                                                                <span class="text-danger">{{$errors->first('call_us_number_2_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">E-mail Us Title</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('email_us_title_en')) has-error @endif">
                                                            {!! Form::text('email_us_title_en', $general_en->footer_email, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('email_us_title_en'))
                                                                <span class="text-danger">{{$errors->first('email_us_title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Email</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('email_link_en')) has-error @endif">
                                                            {!! Form::email('email_link_en', $general_en->footer_email_url, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('email_link_en'))
                                                                <span class="text-danger">{{$errors->first('email_link_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Website Url</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('website_url_en')) has-error @endif">
                                                            {!! Form::url('website_url_en', $general_en->footer_web_url, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('website_url_en'))
                                                                <span class="text-danger">{{$errors->first('website_url_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="footer_2" role="tabpanel">
                                            
                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Head Office Title</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('head_ofiice_title_kh')) has-error @endif">
                                                            {!! Form::text('head_ofiice_title_kh', $general_kh->footer_head, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('head_ofiice_title_kh'))
                                                                <span class="text-danger">{{$errors->first('head_ofiice_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Head Office Desc</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('head_office_desc_kh')) has-error @endif">
                                                            {!! Form::text('head_office_desc_kh', $general_kh->footer_head_desc, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('head_office_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('head_office_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Branch Title</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('branch_title_kh')) has-error @endif">
                                                            {!! Form::text('branch_title_kh', $general_kh->footer_branch, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('branch_title_kh'))
                                                                <span class="text-danger">{{$errors->first('branch_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Branch Desc</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('branch_desc_kh')) has-error @endif">
                                                            {!! Form::text('branch_desc_kh', $general_kh->footer_branch_desc, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('branch_desc_kh'))
                                                                <span class="text-danger">{{$errors->first('branch_desc_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Call Us Title</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('call_us_title_kh')) has-error @endif">
                                                            {!! Form::text('call_us_title_kh', $general_kh->footer_call, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('call_us_title_kh'))
                                                                <span class="text-danger">{{$errors->first('call_us_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Call Us Number 1</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('call_us_number_1_kh')) has-error @endif">
                                                            {!! Form::text('call_us_number_1_kh', $general_kh->footer_call_1, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('call_us_number_1_kh'))
                                                                <span class="text-danger">{{$errors->first('call_us_number_1_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Call Us Number 2</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('call_us_number_2_kh')) has-error @endif">
                                                            {!! Form::text('call_us_number_2_kh', $general_kh->footer_call_2, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('call_us_number_2_kh'))
                                                                <span class="text-danger">{{$errors->first('call_us_number_2_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">E-mail Us Title</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('email_us_title_kh')) has-error @endif">
                                                            {!! Form::text('email_us_title_kh', $general_kh->footer_email, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('email_us_title_kh'))
                                                                <span class="text-danger">{{$errors->first('email_us_title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Email</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('email_link_kh')) has-error @endif">
                                                            {!! Form::email('email_link_kh', $general_kh->footer_email_url, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('email_link_kh'))
                                                                <span class="text-danger">{{$errors->first('email_link_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Website Url</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('website_url_kh')) has-error @endif">
                                                            {!! Form::url('website_url_kh', $general_kh->footer_web_url, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('website_url_kh'))
                                                                <span class="text-danger">{{$errors->first('website_url_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer -->
                        </div>

                        {{-- <div class="tab-pane" id="seo" role="tabpanel">
                            <!-- SEO -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#seo_1" role="tab" aria-controls="seo_1" aria-selected="true"><span class="back-lang">Egnlish</span> <i class="c-icon c-icon-2xl cif-gb"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#seo_2" role="tab" aria-controls="seo_2" aria-selected="false"><span class="back-lang">Khmer</span> <i class="c-icon c-icon-2xl cif-kh"></i></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="seo_1" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Meta description</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('meta_description')) has-error @endif">
                                                            {!! Form::textarea('meta_description_en', $general_en->meta_description, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('meta_description_en'))
                                                                <span class="text-danger">{{$errors->first('meta_description_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Meta Keyword</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('meta_keyword_en')) has-error @endif">
                                                            {!! Form::text('meta_keyword_en', $general_en->meta_keyword, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('meta_keyword_en'))
                                                                <span class="text-danger">{{$errors->first('meta_keyword_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="seo_2" role="tabpanel">
                                            
                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Meta Description</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('meta_description_kh')) has-error @endif">
                                                            {!! Form::textarea('meta_description_kh', $general_kh->meta_description, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('meta_description_kh'))
                                                                <span class="text-danger">{{$errors->first('meta_description_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-12">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Meta Keyword</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('meta_keyword_kh')) has-error @endif">
                                                            {!! Form::text('meta_keyword_kh', $general_kh->meta_keyword, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('meta_keyword_kh'))
                                                                <span class="text-danger">{{$errors->first('meta_keyword_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- SEO -->
                        </div> --}}

                        {{-- <div class="tab-pane" id="product" role="tabpanel">
                            <!-- product -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#product_1" role="tab" aria-controls="product_1" aria-selected="true"><span class="back-lang">Egnlish</span> <i class="c-icon c-icon-2xl cif-gb"></i></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="product_1" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                        <div class="text-center">
                                                            <div class="file-box">
                                                                <div class="file">
                                                                    <span class="corner"></span>
                                                                    <div class="">
                                                                        <div class="m-b-sm">
                                                                            <span class="profile-picture">
                                                                                <div class="logo-thumnail">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <div class="fileinput-news img-circle">
                                                                                            <img class="img-circle img-circle1 image-defalt-product_cover" src="@if(isset($general_en->product_cover) == null || $general_en->product_cover == ''){{ url('/assets/img/no_img.png') }}
                                                                                            @else{{ asset('upload/'.$general_en->product_cover) }}@endif">
                                                                                        </div>
                                                                                        <div class="mt-3">
                                                                                            <span class="btn btn-square btn-info btn-file">
                                                                                                <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                                {{ Form::file('product_cover',['class'=>'product_cover','accept'=>'image/*']) }}
                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="image_message_product_cover"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </span>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="file-name"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- product -->
                        </div> --}}

                        {{-- <div class="tab-pane" id="about" role="tabpanel">
                            <!-- about_cover -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#about_cover" role="tab" aria-controls="about_cover" aria-selected="true"><span class="back-lang">Egnlish</span> <i class="c-icon c-icon-2xl cif-gb"></i></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="about_cover" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                        <div class="text-center">
                                                            <div class="file-box">
                                                                <div class="file">
                                                                    <span class="corner"></span>
                                                                    <div class="">
                                                                        <div class="m-b-sm">
                                                                            <span class="profile-picture">
                                                                                <div class="logo-thumnail">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <div class="fileinput-news img-circle">
                                                                                            <img class="img-circle img-circle1 image-defalt-about_cover" src="@if(isset($general_en->about_cover) == null || $general_en->about_cover == ''){{ url('/assets/img/no_img.png') }}
                                                                                            @else{{ asset('upload/'.$general_en->about_cover) }}@endif">
                                                                                        </div>
                                                                                        <div class="mt-3">
                                                                                            <span class="btn btn-square btn-info btn-file">
                                                                                                <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                                {{ Form::file('about_cover',['class'=>'about_cover','accept'=>'image/*']) }}
                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="image_message_about_cover"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </span>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="file-name"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- about_cover -->
                        </div> --}}

                        {{-- <div class="tab-pane" id="team" role="tabpanel">
                            <!-- team_cover -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#team_cover" role="tab" aria-controls="team_cover" aria-selected="true"><span class="back-lang">Egnlish</span> <i class="c-icon c-icon-2xl cif-gb"></i></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="team_cover" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                        <div class="text-center">
                                                            <div class="file-box">
                                                                <div class="file">
                                                                    <span class="corner"></span>
                                                                    <div class="">
                                                                        <div class="m-b-sm">
                                                                            <span class="profile-picture">
                                                                                <div class="logo-thumnail">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <div class="fileinput-news img-circle">
                                                                                            <img class="img-circle img-circle1 image-defalt-team_cover" src="@if(isset($general_en->team_cover) == null || $general_en->about_cover == ''){{ url('/assets/img/no_img.png') }}
                                                                                            @else{{ asset('upload/'.$general_en->team_cover) }}@endif">
                                                                                        </div>
                                                                                        <div class="mt-3">
                                                                                            <span class="btn btn-square btn-info btn-file">
                                                                                                <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                                {{ Form::file('team_cover',['class'=>'team_cover','accept'=>'image/*']) }}
                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="image_message_team_cover"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </span>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="file-name"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- team_cover -->
                        </div> --}}

                        {{-- <div class="tab-pane" id="news" role="tabpanel">
                            <!-- news_cover -->
                            <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#news_cover" role="tab" aria-controls="news_cover" aria-selected="true"><span class="back-lang">Egnlish</span> <i class="c-icon c-icon-2xl cif-gb"></i></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="news_cover" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                        <div class="text-center">
                                                            <div class="file-box">
                                                                <div class="file">
                                                                    <span class="corner"></span>
                                                                    <div class="">
                                                                        <div class="m-b-sm">
                                                                            <span class="profile-picture">
                                                                                <div class="logo-thumnail">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <div class="fileinput-news img-circle">
                                                                                            <img class="img-circle img-circle1 image-defalt-news_cover" src="@if(isset($general_en->news_cover) == null || $general_en->news_cover == ''){{ url('/assets/img/no_img.png') }}
                                                                                            @else{{ asset('upload/'.$general_en->news_cover) }}@endif">
                                                                                        </div>
                                                                                        <div class="mt-3">
                                                                                            <span class="btn btn-square btn-info btn-file">
                                                                                                <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                                {{ Form::file('news_cover',['class'=>'news_cover','accept'=>'image/*']) }}
                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="image_message_news_cover"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </span>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="file-name"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- news_cover -->
                        </div> --}}
                    </div>
                </div>
            </div>
            @can('general-create')
            <div class="col-lg-12">
                <div class="card-footer text-right">
                    <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.save')</button>
                </div>
            </div>
            @endcan
            {!! Form::close() !!}
        </div>
    </div>

    <style>
        .editable{
            width:120px;
            height:120px;
            object-fit:cover;
            border-radius:50%;
        }
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .form-box{
            border: 1px solid #dadada;
            padding: 10px;
            margin-bottom: 20px;
            float: left;
            width: 100%;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
</style>

@endsection
@section('scripts')
<script type="text/javascript">
    $(".header_logo").change(function(e) {
        e.preventDefault();
        $(".image_message").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.header_logo').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".header_logo").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".favicon").change(function(e) {
        e.preventDefault();
        $(".image_message_1").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.favicon').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_1").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".favicaon").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-favicon').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-favicon').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_1").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-favicon').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_1").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-favicon').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_1").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".footer-logo").change(function(e) {
        e.preventDefault();
        $(".image_message_2").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.footer-logo').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

       // console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_2").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".footer-logo").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-footer').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-footer').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_2").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-footer').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_2").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-footer').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_2").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });




    $(".header_logo_kh").change(function(e) {
        e.preventDefault();
        $(".image_message_kh").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.header_logo_kh').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_kh").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".header_logo_kh").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-kh').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_kh").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_kh").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_kh").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".favicon-kh").change(function(e) {
        e.preventDefault();
        $(".image_message_1_kh").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.favicon-kh').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_1_kh").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".favicaon-kh").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-favicon-kh').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-favicon-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_1_kh").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-favicon-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_1_kh").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-favicon-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_1_kh").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".footer-logo-kh").change(function(e) {
        e.preventDefault();
        $(".image_message_2_kh").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.footer-logo-kh').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

       // console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_2_kh").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".footer-logo-kh").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-footer-kh').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-footer-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_2_kh").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-footer-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_2_kh").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-footer-kh').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_2_kh").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    

    //product cover
    $(".product_cover").change(function(e) {
        e.preventDefault();
        $(".image_message_product_cover").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.product_cover').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_product_cover").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".product_cover").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-product_cover').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-product_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_product_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-product_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_product_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-product_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_product_cover").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".about_cover").change(function(e) {
        e.preventDefault();
        $(".image_message_about_cover").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.about_cover').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_about_cover").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".about_cover").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-about_cover').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-about_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_about_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-about_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_about_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-about_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_about_cover").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    
    $(".team_cover").change(function(e) {
        e.preventDefault();
        $(".image_message_team_cover").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.team_cover').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_team_cover").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".team_cover").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-team_cover').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-team_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_team_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-team_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_team_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-team_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_team_cover").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

    $(".news_cover").change(function(e) {
        e.preventDefault();
        $(".image_message_news_cover").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.news_cover').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_news_cover").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".news_cover").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-news_cover').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_news_cover").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });

</script>
@endsection