@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('atm.index') }}">@lang('message.atm')</a></li>
            <li class="breadcrumb-item active">@lang('message.modify')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('atm.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    {!! Form::model($atm,['method' => 'PATCH','route' => ['atm.update', $atm->id],'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="card-header">@lang('message.modify')</div>
                    <div class="card-body">
                        <div class="row">

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Title (English)</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('title_en')) has-error @endif">
                                        {!! Form::text('title_en', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('title_en'))
                                            <span class="text-danger">{{$errors->first('title_en')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Title (khmer)</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('title_kh')) has-error @endif">
                                        {!! Form::text('title_kh', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('title_kh'))
                                            <span class="text-danger">{{$errors->first('title_kh')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Short Title (English)</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('short_title_en')) has-error @endif">
                                        {!! Form::text('short_title_en', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('short_title_en'))
                                            <span class="text-danger">{{$errors->first('short_title_en')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Short Title (khmer)</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('short_title_kh')) has-error @endif">
                                        {!! Form::text('short_title_kh', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('short_title_kh'))
                                            <span class="text-danger">{{$errors->first('short_title_kh')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Short Description (English)</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('short_desc_en')) has-error @endif">
                                        {!! Form::textarea('short_desc_en', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('short_desc_en'))
                                            <span class="text-danger">{{$errors->first('short_desc_en')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Short Description (khmer)</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('short_desc_kh')) has-error @endif">
                                        {!! Form::textarea('short_desc_kh', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('short_desc_kh'))
                                            <span class="text-danger">{{$errors->first('short_desc_kh')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                        </div>
                   

                        <div class="row">
                            <div class="col-sm-12 col-lg-12">
                               
                                    <div class="text-center">
                                        <div class="file-box">
                                            <div class="file">
                                                <span class="corner"></span>
                                                <div class="">
                                                    <div class="m-b-sm">
                                                        <span class="profile-picture">
                                                            <div class="logo-thumnail">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-news img-circle">
                                                                        <img class="img-circle img-circle1 image-defalt-news_cover" src="@if(isset($atm->atm_thumnail) == null || $atm->atm_thumnail == ''){{ url('/assets/img/no_img.png') }}
                                                                                            @else{{ asset('upload/'.$atm->atm_thumnail) }}@endif">
                                                                    </div>
                                                                    <div class="mt-3">
                                                                        <span class="btn btn-square btn-info btn-file">
                                                                            <span class="fileinput-exists"> ATM (1440x650) Pixcel</span>
                                                                            {{ Form::file('atm_thumnail',['class'=>'news_cover','accept'=>'image/*']) }}
                                                                        </span>
                                                                    </div>
                                                                    <div class="image_message_news_cover"></div>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="file-name"></div>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>


                    </div>
                    @can('page-edit')
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.modify')</button>
                    </div>
                    @endcan
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }
       
        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">
     $(".news_cover").change(function(e) {
        e.preventDefault();
        $(".image_message_news_cover").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.news_cover').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_news_cover").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".news_cover").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-news_cover').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_news_cover").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });


    </script>
@endsection
