@extends('layouts.app')
@section('breadcrumb')
<div class="c-subheader px-3">
    <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
        <li class="breadcrumb-item active">@lang('message.menu_home')</li>
    </ol>
</div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            {!! Form::open(array('route' => 'menuhome.store','method'=>'POST','class'=>'form-horizontal form-box','enctype'=>'multipart/form-data')) !!}

            <div class="col-md-12 mb-4">
                <div class="nav-tabs-boxed nav-tabs-boxed-left">
                    <ul class="nav nav-tabs mm-left" role="tablist" style="width: 200px">
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#menuhome" role="tab" aria-controls="menuhome" aria-selected="true">Menu Home</a></li>
                      
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="menuhome" role="tabpanel">
                             
                             <div class="col-md-12 mb-4">
                                <div class="nav-tabs-boxed lang-kh-en">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menuhome" role="tab" aria-controls="menuhome" aria-selected="true"><span class="back-lang">Menu Home</span></a></li>
                                    </ul>
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane active" id="menuhome" role="tabpanel">

                                            <div class="row">
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">title (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('title_en')) has-error @endif">
                                                            {!! Form::text('title_en', $menuhome->title_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('title_en'))
                                                                <span class="text-danger">{{$errors->first('title_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">title (Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('title_kh')) has-error @endif">
                                                            {!! Form::text('title_kh', $menuhome->title_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('title_kh'))
                                                                <span class="text-danger">{{$errors->first('title_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Page Url (English)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('home_url_en')) has-error @endif">
                                                            {!! Form::text('home_url_en', $menuhome->home_url_en, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('home_url_en'))
                                                                <span class="text-danger">{{$errors->first('home_url_en')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-lg-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 control-label"><label for="nf-email">Page Url(Khmer)</label> <span class="text-danger">*</span></div>
                                                        <div class="col-sm-12 @if($errors->has('home_url_kh')) has-error @endif">
                                                            {!! Form::text('home_url_kh', $menuhome->home_url_kh, array('placeholder' => '','class' => 'form-control')) !!}
                                                            @if($errors->first('home_url_kh'))
                                                                <span class="text-danger">{{$errors->first('home_url_kh')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-lg-12">
                                                   
                                                    <div class="text-center">
                                                        <div class="file-box">
                                                            <div class="file">
                                                                <span class="corner"></span>
                                                                <div class="">
                                                                    <div class="m-b-sm">
                                                                        <span class="profile-picture">
                                                                            <span class="profile-picture">
                                                                                <div class="logo-thumnail">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <div class="fileinput-news img-circle">
                                                                                            <img class="img-circle img-circle1 image-defalt-img" src="@if(isset($menuhome->img) == null || $menuhome->img == ''){{ url('/assets/img/no_img.png') }}
                                                                                        @else{{ asset('upload/'.$menuhome->img) }}@endif">
                                                                                       
                                                                                        </div>
                                                                                        <div class="mt-3">
                                                                                            <span class="btn btn-square btn-info btn-file">
                                                                                                <span class="fileinput-exists"> Cover (1440x650) Pixcel</span>
                                                                                                {{ Form::file('img',['class'=>'img','accept'=>'image/*']) }}
                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="image_message_features_img"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="file-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @can('page-create')
            <div class="col-lg-12">
                <div class="card-footer text-right">
                    <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.save')</button>
                </div>
            </div>
            @endcan
            {!! Form::close() !!}
        </div>
    </div>

    <style>
        .editable{
            width:120px;
            height:120px;
            object-fit:cover;
            border-radius:50%;
        }
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .form-box{
            border: 1px solid #dadada;
            padding: 10px;
            margin-bottom: 20px;
            float: left;
            width: 100%;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
        .nav-link{
            float: left;
            width: 100%;
        }
</style>

@endsection
@section('scripts')
<script type="text/javascript">



   $(".img").change(function(e) {
        e.preventDefault();
        $(".image_message_img").html(''); // To remove the previous error message
        var file = this.files[0];
        defaultimg = file.name;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        var sizefile = this.files[0].size;
        var form = this;
        file_check = this.files && this.files[0];

        var fileName = $('.img').val();
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

        //console.log(extFile);

        //check size
        if (sizefile < 9000000) {
            //check width and height
            if( file_check ) {
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    window.URL.revokeObjectURL( img.src );

                    if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                        $(".image_message_img").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                        var reader = new FileReader();
                        //imageIsLoaded
                        reader.onload = function(e) {
                            $(".img").css("color", "green");
                          //  document.getElementById("submit1").disabled = false;
                            $('.image-defalt-img').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);

                    }
                    else {
                       // document.getElementById("submit1").disabled = true;
                        $('.image-defalt-img').attr('src', '{{asset('/')}}assets/img/no_img.png');
                        $(".image_message_img").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                        return false;
                    }
                }
            }else {
                $('.image-defalt-img').attr('src', '{{asset('/')}}assets/img/no_img.png');
                $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
                //document.getElementById("submit1").disabled = true;
                $(".image_message_img").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                return false;
            }

        } else {
            //document.getElementById("submit1").disabled = true;
            $('.image-defalt-img').attr('src', '{{asset('/')}}assets/img/no_img.png');
            $(".image_message_img").html("<p class='text-danger'>Your image more then 1M</p>");
            return false;
        }
    });


    
</script>
@endsection