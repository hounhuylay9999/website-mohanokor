@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('branch.index') }}">@lang('message.branch')</a></li>
            <li class="breadcrumb-item active">@lang('message.modify')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('branch.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    {!! Form::model($branch,['method' => 'PATCH','route' => ['branch.update', $branch->id],'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="card-header">@lang('message.modify')</div>
                    <div class="card-body">
                        <div class="row">

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.branches')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('province_id')) has-error @endif">
                                        <select name="province_id"  class="form-control select2" style="width: 100% !important;">
                                            @foreach($province as $k => $v)
                                                <option @if($branch->province_id == $v->id) selected @endif  @if(old('province_id') == $v->id) selected @endif value="{{ $v->id }}">{{ $v->title_en }}</option>
                                            @endforeach
                                        </select>

                                        @if($errors->first('province_id'))
                                            <span class="text-danger">{{$errors->first('province_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('title_en')) has-error @endif">
                                        {!! Form::text('title_en', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('title_en'))
                                            <span class="text-danger">{{$errors->first('title_en')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.title') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('title_kh')) has-error @endif">
                                        {!! Form::text('title_kh', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('title_kh'))
                                            <span class="text-danger">{{$errors->first('title_kh')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.e_mail')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('e_mail')) has-error @endif">
                                        {!! Form::text('e_mail', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('e_mail'))
                                            <span class="text-danger">{{$errors->first('e_mail')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.phone') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('phone_en')) has-error @endif">
                                        {!! Form::text('phone_en', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('phone_en'))
                                            <span class="text-danger">{{$errors->first('phone_en')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.phone') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('phone_kh')) has-error @endif">
                                        {!! Form::text('phone_kh', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('phone_kh'))
                                            <span class="text-danger">{{$errors->first('phone_kh')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.address') (@lang('message.english'))</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('address_en')) has-error @endif">
                                        {!! Form::text('address_en', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('address_en'))
                                            <span class="text-danger">{{$errors->first('address_en')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.address') (@lang('message.khmer'))</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('address_kh')) has-error @endif">
                                        {!! Form::text('address_kh', null, array('placeholder' => '','class' => 'form-control')) !!}
                                        @if($errors->first('address_kh'))
                                            <span class="text-danger">{{$errors->first('address_kh')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    @can('contact_us-edit')
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.modify')</button>
                    </div>
                    @endcan
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }
       
        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">
    $('.select2').select2();
    </script>
@endsection
