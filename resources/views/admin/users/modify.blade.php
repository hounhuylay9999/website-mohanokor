@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">@lang('message.users')</a></li>
            <li class="breadcrumb-item active">@lang('message.modify')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('users.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id],'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="card-header">@lang('message.modify')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.full_name')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('name')) has-error @endif">
                                        {!! Form::text('name', null, array('placeholder' => 'Please enter fullname','class' => 'form-control')) !!}
                                        @if($errors->first('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.email')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('email')) has-error @endif">
                                        {!! Form::email('email', null, ['placeholder' => 'Please enter email','class' => 'form-control']) !!}
                                        @if($errors->first('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.roles')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('roles')) has-error @endif">
                                        <select name="roles[]"  class="form-control select2" style="width: 100% !important;">
                                            @foreach($roles as $key => $v)
                                                <option @if($v->name == $userRole) selected @endif value="{{ $v->name }}">{{ $v->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->first('roles'))
                                            <span class="text-danger">{{$errors->first('roles')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label class="col-md-12 col-form-label">@lang('message.gender') <span class="text-danger">*</span></label>
                                    <div class="col-md-12 col-form-label">
                                        @foreach($gender as $k => $v)
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="inline-radio1" type="radio" value="{{ $v->id }}" name="gender" @if($user->gender == $v->id) checked @endif  {{ old('gender') == $v->id ? 'checked' : '' }} >
                                            <label class="form-check-label" for="inline-radio1">{{ $v->name_en }}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.password')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('password')) has-error @endif">
                                        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                                        @if($errors->first('password'))
                                            <span class="text-danger">{{$errors->first('password')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.confirm_password')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('confirm-password')) has-error @endif">
                                        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                                        @if($errors->first('confirm-password'))
                                            <span class="text-danger">{{$errors->first('confirm-password')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                   
                        
                    </div>
                    @can('user-edit')
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.modify')</button>
                    </div>
                    @endcan
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script type="text/javascript">

    
    </script>
@endsection
