
<li class="c-sidebar-nav-item @if(Route::currentRouteName()=='home') c-show @endif">
    <a class="c-sidebar-nav-link @if(Route::currentRouteName()=='home') c-active @endif" href="{{ route('home') }}">
        <i class="c-sidebar-nav-icon cil-speedometer"></i>@lang('message.dashboards')
    </a>
</li>

@can('home-list')
    <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
        <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <i class="c-sidebar-nav-icon cil-find-in-page"></i>@lang('message.home')
        </a>
        <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='herobanner.index' || Route::currentRouteName()=='herobanner.edit' || Route::currentRouteName()=='herobanner.create') c-active @endif" href="{{ route('herobanner.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.herobanner')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='partner.index') c-active @endif" href="{{ route('partner.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.partner')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='exchange_rate.index') c-active @endif" href="{{ route('exchange_rate.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.exchange_rate')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='more_information.index') c-active @endif" href="{{ route('more_information.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.more_information')</a></li>
        </ul>
    </li>
@endcan

@can('page-list')
    <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
        <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <i class="c-sidebar-nav-icon cil-find-in-page"></i>@lang('message.about_us')
        </a>
        <ul class="c-sidebar-nav-dropdown-items">

            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='history.index') c-active @endif" href="{{ route('history.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.history')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='history_detail.index') c-active @endif" href="{{ route('history_detail.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.history_detail')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='mission.index') c-active @endif" href="{{ route('mission.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.vision')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='organization.index') c-active @endif" href="{{ route('organization.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.organization_chart')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='organization_detail.index') c-active @endif" href="{{ route('organization_detail.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.organization_chart_detail')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='management.index') c-active @endif" href="{{ route('management.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.management')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='management_detail.index') c-active @endif" href="{{ route('management_detail.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.management_detail')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='photo.index') c-active @endif" href="{{ route('photo.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.photo')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='photo_detail.index') c-active @endif" href="{{ route('photo_detail.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.photo_detail')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='video.index') c-active @endif" href="{{ route('video.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.video')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='video_detail.index') c-active @endif" href="{{ route('video_detail.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.video_detail')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='news.index') c-active @endif" href="{{ route('news.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.news')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='news_detail.index') c-active @endif" href="{{ route('news_detail.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.news_detail')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='annual_report.index') c-active @endif" href="{{ route('annual_report.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.annual_report')</a></li>
        
        </ul>
    </li>
@endcan

@can('page-list')
    <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
        <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <i class="c-sidebar-nav-icon cil-find-in-page"></i>@lang('message.products_&_services')
        </a>
        <ul class="c-sidebar-nav-dropdown-items">

            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='product.index') c-active @endif" href="{{ route('product.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.product')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='category.index') c-active @endif" href="{{ route('category.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.category')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='product_service.index') c-active @endif" href="{{ route('product_service.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.add_list_product_service')</a></li>
            
        
        </ul>
    </li>
@endcan

@can('careers-list')
    <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
        <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <i class="c-sidebar-nav-icon cil-find-in-page"></i>@lang('message.careers')
        </a>
        <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='job_title.index') c-active @endif" href="{{ route('job_title.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.job_title')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='job_post.index') c-active @endif" href="{{ route('job_post.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.form_job_post')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='location.index') c-active @endif" href="{{ route('location.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.location')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='position.index') c-active @endif" href="{{ route('position.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.position')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='e_form.index') c-active @endif" href="{{ route('e_form.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.E_form_post')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='position_apply.index') c-active @endif" href="{{ route('position_apply.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.position_apply')</a></li>   
        </ul>
    </li>
@endcan

@can('contact_us-list')
    <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
        <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <i class="c-sidebar-nav-icon cil-find-in-page"></i>@lang('message.contact_us')
        </a>
        <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='branches_network.index') c-active @endif" href="{{ route('branches_network.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.branches_network_title')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='branch.index') c-active @endif" href="{{ route('branch.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.branches_network')</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='complaint_form.index') c-active @endif" href="{{ route('complaint_form.index') }}"><span class="c-sidebar-nav-icon"></span>@lang('message.complaint_form')</a></li>
        </ul>
    </li> 
@endcan

@can('setting-list')
<li class="c-sidebar-nav-item c-sidebar-nav-dropdown 
@if(Route::currentRouteName()=='profile.companyProfile' 
|| Route::currentRouteName()=='users.index' || Route::currentRouteName()=='users.create' || Route::currentRouteName()=='users.edit' 
|| Route::currentRouteName()=='roles.index' || Route::currentRouteName()=='roles.create' || Route::currentRouteName()=='roles.edit' 
|| Route::currentRouteName()=='permission.index') c-show @endif">
    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
        <i class="c-sidebar-nav-icon cil-cog"></i>@lang('message.setting')
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
        
        @can('general-list')
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='general.index') c-active @endif" href="{{ route('general.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.general')</a></li>
        @endcan
        @can('company-list')
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='profile.companyProfile') c-active @endif" href="{{ route('profile.companyProfile') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.company_profile')</a></li>
        @endcan
        @can('user-list')
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='users.index' || Route::currentRouteName()=='users.create' || Route::currentRouteName()=='users.edit') c-active @endif" href="{{ route('users.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.manage_users')</a></li>
        @endcan
        @can('role-list')
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='roles.index' || Route::currentRouteName()=='roles.create' || Route::currentRouteName()=='roles.edit') c-active @endif" href="{{ route('roles.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.manage_roles')</a></li>
        @endcan
        @can('role-list')
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='permission.index') c-active @endif" href="{{ route('permission.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.manage_permissions')</a></li>
        @endcan
    </ul>
</li>
@endcan
