<div class="modal modal-primary" id="loading">
    <div class="modal-dialog" style="top:30%">
        <div class="">
            <div class="modal-body" style="text-align: center;">
                <img src="{{ asset('assets/img/loading.gif') }}"/>
            </div>
        </div>
    </div>
</div>

<link href="{{asset('assets/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/js/plugins/toastr/toastr.min.js')}}"></script>

<script>
    var is_success  = "@if(session('success')) {{session('success')}} @endif";
    var is_error    = "@if(session('error')) {{session('error')}} @endif";
    var is_warning  = "@if(session('warning')) {{session('warning')}} @endif";
    var is_info     = "@if(session('info')) {{session('info')}} @endif";

    $(document).ready(function() {
       // setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: false,
                // showMethod: 'slideDown',
                //timeOut: 4111110600
            };

            if($.trim(is_success) != ''){
                toastr.success('Success', is_success);
            }

            if($.trim(is_info) != ''){
                toastr.info('info', is_info);
            }

            if($.trim(is_error) != ''){
                toastr.error('Error', is_error);
            }

            if($.trim(is_warning) != ''){
                toastr.warning('Warning', is_warning);
            }

       // }, 13880);
    });
</script>