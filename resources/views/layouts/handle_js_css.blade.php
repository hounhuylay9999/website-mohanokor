@php
        $url = url('/');
        $_jscss = '';

        $_lib = array(
            'select2'				=>
                '<link href="'.$url.'/assets/css/plugins/select2/select2.min.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/select2/select2.full.min.js"></script>',
            'highcharts'				=>
               '<link href="'.$url.'/assets/css/plugins/select2/select2.min.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/highcharts.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/highcharts-more.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/solid-gauge.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/data.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/drilldown.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/exporting.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/export-data.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/accessibility.js"></script>',
            'solid_gauge'				=>
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/highcharts.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/highcharts-more.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/solid-gauge.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/data.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/drilldown.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/exporting.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/export-data.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/accessibility.js"></script>',
            'highcharts_map'				=>
                '<script src="https://code.highcharts.com/maps/highmaps.js"></script>'.
                '<script src="'.$url.'/assets/js/kh_map.js"></script>',
            'chosen'				=>
                '<link href="'.$url.'/assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/chosen/chosen.jquery.js"></script>',
            'clockpicker'			=>
                '<link href="'.$url.'/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/clockpicker/clockpicker.js"></script>',
            'datepicker'			=>
                '<link href="'.$url.'/assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>',

            'dataTable'				=>
                '<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet">'.
                '<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" defer></script>',

            'wizard'				=>
                '<link href="'.$url.'/assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/steps/jquery.steps.min.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/validate/jquery.validate.min.js"></script>',

            'c3'				=>
                '<link href="'.$url.'/assets/css/plugins/c3/c3.min.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/d3/d3.min.js"></script>'.
               '<script src="'.$url.'/assets/js/plugins/c3/c3.min.js"></script>',

            'moment'				=>
                '<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>',

                'summernote'				=>
                '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>'.
                '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>'.
                '<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">'.
                '<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>',

            'jasny-bootstrap'				=>
                '<link href="'.$url.'/assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>',
            'typeahead'				=>
                '<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>',
            'slick'				=>
                '<link href="'.$url.'/assets/css/plugins/slick/slick.css" rel="stylesheet">'.
                '<link href="'.$url.'/assets/css/plugins/slick/slick-theme.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/slick/slick.min.js"></script>',
            'fullcalendar'				=>
                '<link href="'.$url.'/assets/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/fullcalendar/moment.min.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>',
            'publicjs'   =>
                '<script src="'.$url.'/assets/js/public.js"></script>',
        );


        $_libmap = array(
            'home'								            => $_lib['select2'].$_lib['c3'].$_lib['datepicker'],
            //users
            'users.index'							    	=> $_lib['dataTable'],
  
            //roles
            'roles.index'							    	=> $_lib['dataTable'],
            
            //herobanner
            'herobanner.index'							    => $_lib['dataTable'],

            //menuhome
            'menuhome.index'							    => $_lib['dataTable'],

            //home page
            'aboutus.index'							    	=> $_lib['dataTable'],

            //history
            'history.index'							    	=> $_lib['dataTable'],

            //history detail
            'history_detail.index'							=> $_lib['dataTable'],

            //atm
            'atm.index'							    	    => $_lib['dataTable'],

            //mobile
            'mobile.index'							    	=> $_lib['dataTable'],

            //payroll
            'payroll.index'							    	=> $_lib['dataTable'],

            //mobile
            'mobile.index'							    	=> $_lib['dataTable'],

            //payroll
            'payroll.index'							    	=> $_lib['dataTable'],

            //mission
            'mission.index'							    	=> $_lib['dataTable'],

            //vision
            'vision.index'							    	=> $_lib['dataTable'],

            //vision detail
            'vision_detail.index'						    => $_lib['dataTable'],

            //deposit
            'deposit.index'							        => $_lib['dataTable'],

            //organization
            'organization.index'							=> $_lib['dataTable'],

            //organization
            'organization_detail.index'					    => $_lib['dataTable'],

            //management
            'management.index'							    => $_lib['dataTable'],

            //management detail
            'management_detail.index'				        => $_lib['dataTable'],

            //photo
            'photo.index'							    	=> $_lib['dataTable'],

            //photo detail
            'photo_detail.index'						    => $_lib['dataTable'],
            'photo_detail.create'                           => $_lib['select2'].$_lib['summernote'],
            'photo_detail.edit'                             => $_lib['select2'].$_lib['summernote'],

            //video
            'video.index'							    	=> $_lib['dataTable'],

            //video detail
            'video_detail.index'							=> $_lib['dataTable'],
            'video_detail.create'                           => $_lib['select2'].$_lib['summernote'],
            'video_detail.edit'                             => $_lib['select2'].$_lib['summernote'],

            //news 
            'news.index'							    	=> $_lib['dataTable'],

            //news detail
            'news_detail.index'							    => $_lib['dataTable'],
            'news_detail.create'						    => $_lib['select2'].$_lib['c3'].$_lib['datepicker'],
            'news_detail.edit'						        => $_lib['select2'].$_lib['c3'].$_lib['datepicker'],

            //credit
            'credit.index'							    	=> $_lib['dataTable'],

            //branch
            'branch.index'							    	=> $_lib['dataTable'],
            'branch.create'                                 => $_lib['select2'].$_lib['summernote'],
            'branch.edit'                                   => $_lib['select2'].$_lib['summernote'],

            //partner
            'partner.index'							    	=> $_lib['dataTable'],
            
            //permission
            'permission.index'							    => $_lib['dataTable'],

            //straightTube
            'straightTube.index'                            => $_lib['summernote'].$_lib['select2'],

            //pancake-coil
            'pancakeCoil.index'                             => $_lib['summernote'].$_lib['select2'],
            'pancakeCoil.create'                            => $_lib['summernote'].$_lib['select2'],

            //copperfitting
            'copperfitting.index'                           => $_lib['summernote'].$_lib['select2'],
            'copperfitting.create'                          => $_lib['summernote'].$_lib['select2'],

            //Position
            'position.index'							    => $_lib['dataTable'],

            //location
            'location.index'							    => $_lib['dataTable'],

            //position apply
            'position_apply.index'							=> $_lib['dataTable'],

            //position apply
            'complaint.index'							    => $_lib['dataTable'],

            //e-form
            'e_form.index'							        => $_lib['dataTable'],
            'e_form.create'                                 => $_lib['select2'].$_lib['summernote'],
            'e_form.edit'                                   => $_lib['select2'].$_lib['summernote'],

            //job post
            'job_post.index'							    => $_lib['dataTable'],
            'job_post.create'                               => $_lib['select2'].$_lib['summernote'],
            'job_post.edit'                                 => $_lib['select2'].$_lib['summernote'],
            'job_post.create'						        => $_lib['select2'].$_lib['c3'].$_lib['datepicker'],
            'job_post.edit'						            => $_lib['select2'].$_lib['c3'].$_lib['datepicker'],

            //job title 
            'job_title.index'							    => $_lib['dataTable'],

            //product
            'product.index'							        => $_lib['dataTable'],

            //category
            'category.index'							    => $_lib['dataTable'],
            'category.create'                               => $_lib['select2'].$_lib['summernote'],
            'category.edit'                                 => $_lib['select2'].$_lib['summernote'], 

            //more informatin
            'exchange_rate.index'					        => $_lib['dataTable'],
            'exchange_rate.create'						    => $_lib['select2'].$_lib['c3'].$_lib['datepicker'],
            'exchange_rate.edit'						    => $_lib['select2'].$_lib['c3'].$_lib['datepicker'],

            //more informatin
            'more_information.index'					    => $_lib['dataTable'],

            //product service
            'product_service.index'							=> $_lib['dataTable'],
            'product_service.create'                        => $_lib['select2'].$_lib['summernote'],
            'product_service.edit'                          => $_lib['select2'].$_lib['summernote'],   

            //complaint form
            'complaint_form.index'							=> $_lib['dataTable'],

            //branches network
            'branches_network.index'					    => $_lib['dataTable'],

            //annual report
            'annual_report.index'					        => $_lib['dataTable'],

            //annual report detail
            'annual_report_detail.index'					=> $_lib['dataTable'],

        );

        if( isset($_libmap[\Request::route()->getName()]) ){
            $_jscss				= $_libmap[\Request::route()->getName()];
        }
        echo $_jscss;
@endphp