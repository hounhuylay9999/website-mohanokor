@extends('front-end.layouts.app')
@section('title','E-Form')
@section('content')

<head>
 
    <link rel="stylesheet" href="{{ asset('css/sweetalert/sweetalert.css')}}">
	<link rel="stylesheet" href="{{ asset('css/css/ApplicationForm/style.css')}}">
	<link rel="stylesheet" href="{{ asset('css/css/ApplicationForm/style_customer_complaint.css')}}">
	<script src="{{ asset('js/sweetalert/sweetalert.js')}}"></script>
    
</head>
<style>
    .select-option{
        padding: 10px 30px;
        border-radius: 10px;
        background: rgb(209, 209, 209);
        font-weight: bold;
    }

</style>
<script>
    
    function preventupload(){
            
        var myfile=document.getElementById("userfile").value;
        var ext = myfile.split('.').pop();
            //var extension = myfile.substr( (myfile.lastIndexOf('.') +1) );
            if(ext=="pdf" || ext=="png" || ext=="jpg"){
                swal({title: "",text: "ឯកសារបានបញ្ជូនរួចហើយ",type: "warning"});
            }
            else{
                swal({title: "",text: "Please Upload: PDF, JPG, PNG!",type: "warning"});			
                document.getElementById("userfile").value="";
                return false;
            }	
    }
                  
    //only khmer or english language
    function checklanguage(field,language){
            var sNewVal = "";
            var sFieldVal = field.value;
            for(var i = 0; i < sFieldVal.length; i++) {
                var ch = sFieldVal.charAt(i);
                var c = ch.charCodeAt(0);
        
        //English
        if(language=="English"){
        if(c < 0 || c > 255) {
            field.value="";
            swal({title: "",text: "Please Type English!",type: "warning"});
            //msg("Please Type English ");
            //msg(coffee);
            return false;}
        else {
                    sNewVal += ch;}   
    
        //Khmer 
    
        }else{
        if(c < 0 || c > 255 || c==32) {
        sNewVal += ch;}
        else {
            field.value="";
            swal({title: "",text: "Please Type Khmer",type: "warning"});    
            return false;  } } }  
    }
  
  </script>

<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
		<div class="loader"></div>
	</div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main">
                <section class="signup">
                    <div class="container container_form grey-box"><div class="blue-title"><h3 style="margin-top:11px;"><b>{{$e_form->title}}</b></h3></div>
                    <div class="signup-content">
                    <div class="signup-form">
                
                    {!! Form::open(array('route' => 'e_form_detail.store','method'=>'POST','class'=>'register-form','enctype'=>'multipart/form-data','id'=>'contact-form', 'onSubmit'=>"return preventupload()")) !!}    
                    <div class="form-group">
                        <label for="first_name"><i class="fa fa-user"></i></label>
                        <input type="text" placeholder="First Name" tabindex="1" name="first_name" required autofocus style="font-family:contents;font-size:16px;">
                    </div>
                    <div class="form-group">
                        <label for="last_name"><i class="fa fa-user"></i></label>
                        <input type="text" placeholder="Last Name" tabindex="2" name="last_name" required autofocus style="font-family:contents;font-size:16px;">
                    </div>
                    <div class="form-group">
                        <label for="pass"><i class="fa fa-envelope"></i></label>
                        <input type="email" placeholder="E-Mail" tabindex="3" name="e_mail"  required style="font-family:contents;font-size:16px;">
                    </div>
                    <div class="form-group">
                        <label for="phone"><i class="fa fa-phone"></i></label>
                        <input type="text" placeholder="Phone"  tabindex="4" name="phone"  required style="font-family:contents;font-size:16px;">
                    </div>
                    <p class="text-block m-b-5" style="font-size:13pt;"><b>@lang('front-end.position_apply'): (@lang('front-end.required'))</b></p>
                    <div class="form-group">
                        <select class="select-option"  name="position_apply_id" required="" style="font-family:contents;font-size:16px;">
                            <option>@lang('front-end.please_select_position')</option>
                            @foreach($position_apply as $key => $value)
                                <option>{{$value->name}}</option>
                            @endforeach
                        </select>	
                    </div> 
                    <p class="text-block m-b-5"><b>@lang('front-end.document'):  <span style="font-family:Arials">(Image, PDF, Max: 5MB)</span></b></p>
                    <div class="form-group">
                        <input  name="file" id="userfile" type="file" accept="application/pdf, image/*"><h4></h4>
                    </div>
                    <div class="form-group form-button">
                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1" style="background:#990000;">@lang('front-end.submit')</button>
                    </div>
                {!! Form::close() !!}
                </div>
                    <div class="signup-image">
                        <figure><img src="{{ asset('upload/'.$e_form->e_form_thumnail) }}"></figure>
                    </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
  
  </section>


@endsection

