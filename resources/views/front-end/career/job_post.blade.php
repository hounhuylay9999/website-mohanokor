@extends('front-end.layouts.app')
@section('title','JobPost')
@section('content')

<style>
    .color{
        background: #990000;
        color:white;    
    }
</style>
<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
		<div class="loader"></div>
	</div>
    <div class="page-content p-b-70">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <section class="section post-section-2 p-r-40">
                        <div class="post-header">
                            <h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">{{$job_title->title}}</h3>
							<hr class="hr-vision" style="margin-top:-60px;"><br>
                        </div>
                        <div class="post-content">
							<div class="scroll">								
                                    <table class="table">
                                        <thead>
                                            <tr style="font-size: 16px;">
                                                <th style="font-family:mohanokor;">@lang('front-end.position')</th>
                                                <th style="font-family:mohanokor;">@lang('front-end.location')</th>
                                                <th style="font-family:mohanokor;">@lang('front-end.close_date')</th>
                                                <th style="font-family:mohanokor;">@lang('front-end.details')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($job_post as $k => $v)
                                                <tr style="font-family:contents;font-size:11pt;">		
                                                    {{-- <td style="width:30%;vertical-align:middle;">{{$v->position_name}}&nbsp;<span class="label label-danger"></span></td> --}}
                                                    @if(app()->getLocale() == 'en')
                                                        <td style="width:30%;vertical-align:middle;">{{$v->name_en}}&nbsp;<span class="label label-danger"></span></td>
                                                    @elseif(app()->getLocale() == 'kh') 
                                                        <td style="width:30%;vertical-align:middle;">{{$v->name_kh}}&nbsp;<span class="label label-danger"></span></td>
                                                    @endif 
                                                    {{-- <td style="vertical-align:middle;">{{$v->location_name}}</td> --}}
                                                    @if(app()->getLocale() == 'en')
                                                        <td style="width:30%;vertical-align:middle;">{{$v->location_name_en}}&nbsp;<span class="label label-danger"></span></td>
                                                    @elseif(app()->getLocale() == 'kh') 
                                                        <td style="width:30%;vertical-align:middle;">{{$v->location_name_kh}}&nbsp;<span class="label label-danger"></span></td>
                                                    @endif 

                                                    <td style="vertical-align:middle;">{{$v->date_line}}</td>
                                                    <td align="left"><a href="{{ asset('upload/'.$v->job_post_thumnail) }}" class="btn btn-danger color" target="_blank" style="width:85px;">@lang('front-end.download')</a></td>					
                                                </tr>
                                            @endforeach
                                        </tbody>
                                </table>	
							</div>
                        </div>
                    </section>
                </div>
                <div class="col-md-4">
				<div class="m-b-50" style="margin-top:65px;"></div>
				<div class="row">
                        <div class="m-b-50">
                            <div class="gallery-box m-b-15">
                                <div class="gallery-overlay">
                                    <a class="fa fa-search-plus gallery-photo" href="{{ asset('upload/'.$job_title->job_title_thumnail) }}"></a>
                                </div>
                                <img class="img-responsive image-box" src="{{ asset('upload/'.$job_title->job_title_thumnail) }}">
                            </div>
                        </div>
                        <div class="m-b-50">
                            <div class="m-b-50" style="text-align:center;">					
								<div class="sidebar-freshnews">
									<div class="title-freshnews">
										@lang('front-end.application_form')
									</div>
									<div style="text-align:center;padding:10px;">
										<h4 style="color:#990000;font-family:mohanokor;">"@lang('front-end.click')</h4>
										<h4 style="color:#990000;font-family:mohanokor;">@lang('front-end.button')"</h4> 
										<div style="text-align:center;margin-top:15px;">
											<a href="{{ asset('Apply-job/External_app_form.pdf') }}" class="btn btn-primaryDownload color" target="_blank">@lang('front-end.download')</a>
										</div><br>										
										&nbsp;&nbsp;&nbsp;&nbsp;<a href=""><img src="upload/app-form.jpg"></a>
									</div>
								</div>			
							</div>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
  </section>

@endsection

@section('scripts')
<script>
    
</script>
@endsection