@extends('front-end.layouts.app')
@section('title','Product & Service')
@section('content')
<style>
    table th{
        background: #990000;
        color: white;
    }
    
    ul.no-list-style {
    list-style-image: url('{{ asset('upload/list.gif') }}');
    margin-left: 15px;
}
</style>

    <section class="section p-t-70 p-b-40 bg-white">
        <div class="heading-page heading-page-1 heading-page-2">
      
        </div>
        <hr>
        <div class="page-loader">
            <div class="loader"></div>
        </div>
        <div class="page-content p-t-80 p-b-30" style="margin-top: -20px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-md-push-3">
                        <section class="section post-section-1 p-l-10">                       
                            <div class="post-content">                           
                                <h4 class="text-block text-bold text-black text-med-large m-b-30 header-title-color" style="margin-top:-2px">{{$service_product->title}}</h4>                           
                                <hr class="hr-vision" style="margin-top:-70px;">
                                <div class="row">
                                    <div class="col-md-12">                                   
                                        <p class="text-block m-b-30"><br>
                                            {!! $service_product->sort_text !!}    
                                        </p>
                                        
                                        @if(app()->getLocale() == 'en')
                                            <div align="center"><img src="{{ asset('upload/'.$service_product->product_service_thumnail_en) }}" class="loan-responsive-img"></div>
                                        @elseif(app()->getLocale() == 'kh') 
                                            <div align="center"><img src="{{ asset('upload/'.$service_product->product_service_thumnail_kh) }}" class="loan-responsive-img"></div>
                                        @endif 
                                        <div class="col-sm-12" style="padding-bottom:15px;"><br>
                                        <ul class="no-list-style" style="text-align:justify;text-justify: inter-cluster;padding-left:18px;padding-top:10px;word-wrap: break-word;line-height:28px;">
                                            {!! $service_product->full_text !!}
                                        </ul>
                                    </div>
                                </div>                                
                            </div>		
                        </section>
                    </div>
                    <div class="col-md-3 col-md-pull-9">                    
                        <div class="m-b-50" >	
                            <div class="sidebar-loan">
                                <div class="title-loan">
                                    {{$product->title}}
                                </div><br>
                                <ul class="list-loan">  
                                    @foreach($category as $key => $value)
                                        <li><a  class="text-center title-loan-content" href="{{route('product_show.index')}}?type={{$value->id}}&main={{$value->product_id}}">{{$value->title}}</a></li>	
                                    @endforeach
                                </ul>
                            </div>                        		
                        </div>			
                       
                        <div class="m-b-50" style="text-align:center;">
                           <div class="sidebar-loan">
                                @if(app()->getLocale() == 'en')
                                    <div class="title-loan">
                                        @lang('front-end.contact_information')
                                    </div>
                                    <img src="{{ asset('images/phone_call_en.png') }}" style="width:100%; height:auto;"/>
                                @elseif(app()->getLocale() == 'kh') 
                                    <div class="title-loan">
                                        @lang('front-end.contact_information')
                                    </div>
                                    <img src="{{ asset('images/phone_call_kh.png') }}" style="width:100%; height:auto;"/>
                                @endif 
                            </div>	

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('scripts')
<script>
  
</script>
@endsection