<?php 
use App\Http\Controllers\frontend\HomePageController;
$menuhome= HomePageController::Getmenu(); 
?>
    @if(app()->getLocale() == 'en')
        <link href="{{asset('fonts/fonts/english_fonts.css')}}" rel="stylesheet">
    @elseif(app()->getLocale() == 'kh') 
        <link href="{{asset('fonts/fonts/khmer_fonts.css')}}" rel="stylesheet">
    @endif 

<header>

    <div class="hidden-tablet-landscape-up">
        <div class="header header-mobile-1">
            <div class="top-header">
                <div class="container-fluid">
                    <div class="logo">
                        <a class="" href="{{ route('homepage.index') }}">
                            <img src="{{ asset('upload/'.$general->header_logo) }}"/>
                        </a>
                    </div>
                    
                    <button class="hamburger hamburger--spin hidden-tablet-landscape-up" id="toggle-icon">	
                    <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                    <div class="search-widget search-widget-1" style="width:25px;">
                        
                        <ul class="horizontal-list" style="margin-right:0px;margin-top:21px;">			
                            <li style="cursor: pointer;">
                                @if(app()->getLocale() == 'en')
                                    <span>
                                        <a  href="{{ url('locale/kh') }}">
                                            <img src="{{ asset('upload/kh.png') }}" title="Khmer">
                                        </a>
                                    </span>
                                @elseif(app()->getLocale() == 'kh') 
                                    <span>
                                        <a  href="{{ url('locale/en') }}">
                                            <img src="{{ asset('upload/en.png') }}" title="English">
                                        </a>
                                    </span>
                                @endif 
                            </li>                           
                        </ul>
                        <input class="animated" type="text" placeholder="Search" />
                    </div>
                    
                   
                </div>
            </div>
            <div class="au-navbar-mobile navbar-mobile-1">
                <ul class="au-navbar-menu" style="font-color:red;">
                    <li><a class="nav-link" @if(Route::currentRouteName()=='homepage.index') aria-current="page" @endif href="{{ route('homepage.index') }}">@lang('front-end.home')</a></li>
                    <li  class="drop">
                        <a href="#">@lang('front-end.about_us')</a>
                        <span class="arrow">
                            <i></i>
                        </span>
                        <ul class="drop-menu bottom-right">
                            <li><a class="nav-link" @if(Route::currentRouteName()=='historys.index') aria-current="page" @endif href="{{ route('historys.index') }}">@lang('front-end.history')</a></li>
                            <li><a class="nav-link" @if(Route::currentRouteName()=='missions.index') aria-current="page" @endif href="{{ route('missions.index') }}">@lang('front-end.vision_mission')</a></li>
                            <li><a class="nav-link" @if(Route::currentRouteName()=='organizations.index') aria-current="page" @endif href="{{ route('organizations.index') }}">@lang('front-end.organization_chat')</a></li>
                            <li><a class="nav-link" @if(Route::currentRouteName()=='managements.index') aria-current="page" @endif href="{{ route('managements.index') }}">@lang('front-end.management')</a></li>
                            <li><a class="nav-link" @if(Route::currentRouteName()=='photos.index') aria-current="page" @endif href="{{ route('photos.index') }}">@lang('front-end.photo')</a></li>
                            <li><a class="nav-link" @if(Route::currentRouteName()=='videos.index') aria-current="page" @endif href="{{ route('videos.index') }}">@lang('front-end.video')</a></li>
                            <li><a class="nav-link" @if(Route::currentRouteName()=='news_event.index') aria-current="page" @endif href="{{ route('news_event.index') }}">@lang('front-end.news_event')</a></li>
                            <li><a class="nav-link" @if(Route::currentRouteName()=='annual_reports.index') aria-current="page" @endif href="{{ route('annual_reports.index') }}">@lang('front-end.annual_report')</a></li>
                        </ul>
                    </li>
                    <li class="drop">
                        <a href="#">@lang('front-end.product_service')</a>
                        <span class="arrow">
                            <i></i>
                        </span>
                        <ul class="drop-menu bottom-right">
                            <li class="drop">
                                <a href="#">@lang('front-end.credit')</a>
                                <ul class="drop-menu top-right">
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=1&main=1">@lang('front-end.business_loan')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=2&main=1">@lang('front-end.group_loan')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=3&main=1">@lang('front-end.agriculture_loan')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=4&main=1">@lang('front-end.car_loan')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=5&main=1">@lang('front-end.motorbike_loan')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=6&main=1">@lang('front-end.home_improvement_loan')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=7&main=1">@lang('front-end.housing_loan')</a></li>
                                </ul>
                            </li>
                            <li class="drop">
                                <a href="#">@lang('front-end.deposit')</a>
                                <ul class="drop-menu top-right">
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=8&main=2">@lang('front-end.general_saving')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=9&main=2">@lang('front-end.fixed_deposit')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=10&main=2">@lang('front-end.future_account')</a></li>										 
                                </ul>
                            </li>
                            <li class="drop">
                                <a href="#">@lang('front-end.atm')</a>
                                <ul class="drop-menu top-right">
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=11&main=3">@lang('front-end.atm_card')</a></li>
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=12&main=3">@lang('front-end.atm')</a></li>
                                </ul>
                            </li>
                            
                            <li class="drop">
                                <a href="#">@lang('front-end.mohanokor_mobile')</a>
                                <ul class="drop-menu top-right">
                                    <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=13&main=4">@lang('front-end.mobile')</a></li>
                                </ul>
                            </li>
                            
                            <li class="drop">
                                <a href="#">@lang('front-end.payroll')</a>
                                <ul class="drop-menu top-right">
                                    <a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=14&main=5">@lang('front-end.payroll')</a></li>	
                                </ul>
                            </li>	
                            
                        </ul>
                    </li>
                    <li class="drop">
                        <a href="#">@lang('front-end.carerr')</a>
                         <span class="arrow">
                            <i></i>
                        </span>
                        <ul class="drop-menu bottom-right">
                            <li><a @if(Route::currentRouteName()=='job_posts.index') aria-current="career" @endif href="{{ route('job_posts.index') }}">@lang('front-end.job_information')</a></li>
                            <li><a @if(Route::currentRouteName()=='e_forms.index') aria-current="career" @endif href="{{ route('e_forms.index') }}">@lang('front-end.e_form')</a></li>
                        </ul>
                    </li>
                    
                    <li class="drop">
                        <a href="#">@lang('front-end.contact_us')</a>
                        <span class="arrow">
                            <i></i>
                        </span>
                        <ul class="drop-menu bottom-right">
                            <li><a @if(Route::currentRouteName()=='branches_networks.index') aria-current="contact" @endif href="{{ route('branches_networks.index') }}">@lang('front-end.branch_network')</a></li>
                            <li><a @if(Route::currentRouteName()=='complaints.index') aria-current="contact" @endif href="{{ route('complaints.index') }}">@lang('front-end.complain_form')</a></li>
                        </ul>
                    </li>    
                </ul>
                
                
            </div>
            <div class="container-fluid">
                <div class="contact-widget contact-widget-1">
                    {{-- <a class="hidden-tablet-landscape au-btn au-btn-primary" href="#"></a> --}}
                </div>
            </div>
        </div>
    </div>

    <div class="section hidden-tablet-landscape bg-gradient-light section-navbar-1 section-navbar-2 section-navbar-3 section-navbar-5 section-navbar-6" id="js-navbar-fixed">
        <div class="container-fluid">
            <div class="header header-2 header-3">
                <div class="block-left">
                    <div class="logo">
                        <a class="" href="{{ route('homepage.index') }}">
                            <img src="{{ asset('upload/'.$general->header_logo) }}"/>
                        </a>
                        <a class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                            <div class="hum"></div>
                            <div class="hum"></div>
                            <div class="hum"></div>
                        </a>
                    </div>
                </div>
                <div class="block-center">
                    <div class="au-navbar navbar-1 navbar-2">
                        <ul class="au-navbar-menu"> 
                            <li><a class="nav-link" @if(Route::currentRouteName()=='homepage.index') aria-current="page" @endif href="{{ route('homepage.index') }}">@lang('front-end.home')</a></li>
                            <li class="drop">
                                <a href="#">@lang('front-end.about_us')</a>
                                 <ul class="drop-menu bottom-right">
                                    <li><a class="nav-link" @if(Route::currentRouteName()=='historys.index') aria-current="page" @endif href="{{ route('historys.index') }}">@lang('front-end.history')</a></li>
                                    <li><a class="nav-link" @if(Route::currentRouteName()=='missions.index') aria-current="page" @endif href="{{ route('missions.index') }}">@lang('front-end.vision_mission')</a></li>
                                    <li><a class="nav-link" @if(Route::currentRouteName()=='organizations.index') aria-current="page" @endif href="{{ route('organizations.index') }}">@lang('front-end.organization_chat')</a></li>
                                    <li><a class="nav-link" @if(Route::currentRouteName()=='managements.index') aria-current="page" @endif href="{{ route('managements.index') }}">@lang('front-end.management')</a></li>
                                    <li><a class="nav-link" @if(Route::currentRouteName()=='photos.index') aria-current="page" @endif href="{{ route('photos.index') }}">@lang('front-end.photo')</a></li>
                                    <li><a class="nav-link" @if(Route::currentRouteName()=='videos.index') aria-current="page" @endif href="{{ route('videos.index') }}">@lang('front-end.video')</a></li>
                                    <li><a class="nav-link" @if(Route::currentRouteName()=='news_event.index') aria-current="page" @endif href="{{ route('news_event.index') }}">@lang('front-end.news_event')</a></li>
                                    <li><a class="nav-link" @if(Route::currentRouteName()=='annual_reports.index') aria-current="page" @endif href="{{ route('annual_reports.index') }}">@lang('front-end.annual_report')</a></li>
                                </ul>
                            </li>
                            <li class="drop">
                                <a href="#">@lang('front-end.product_service')</a>
                                <ul class="drop-menu bottom-right">
                                    <li class="drop">
                                        <a href="#">@lang('front-end.credit')</a>
                                        <ul class="drop-menu top-right">
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=1&main=1">@lang('front-end.business_loan')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=2&main=1">@lang('front-end.group_loan')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=3&main=1">@lang('front-end.agriculture_loan')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=4&main=1">@lang('front-end.car_loan')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=5&main=1">@lang('front-end.motorbike_loan')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=6&main=1">@lang('front-end.home_improvement_loan')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=7&main=1">@lang('front-end.housing_loan')</a></li>
                                        </ul>
                                    </li>
                                    <li class="drop">
                                        <a href="#">@lang('front-end.deposit')</a>
                                        <ul class="drop-menu top-right">
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=8&main=2">@lang('front-end.general_saving')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=9&main=2">@lang('front-end.fixed_deposit')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=10&main=2">@lang('front-end.future_account')</a></li>
                                        </ul>
                                    </li>										
                                    <li class="drop">
                                        <a href="#">@lang('front-end.atm')</a>
                                        <ul class="drop-menu top-right">
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=11&main=3">@lang('front-end.atm_card')</a></li>
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=12&main=3">@lang('front-end.atm')</a></li>
                                        </ul>
                                    </li>
                            
                                    <li class="drop">
                                        <a href="#">@lang('front-end.mohanokor_mobile')</a>
                                        <ul class="drop-menu top-right">
                                            <li><a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=13&main=4">@lang('front-end.mobile')</a></li>
                                        </ul>
                                    </li>

                                    <li class="drop">
                                        <a href="#">@lang('front-end.payroll')</a>
                                        <ul class="drop-menu top-right">
                                            <a @if(Route::currentRouteName()=='product_show.index') aria-current="product" @endif href="{{route('product_show.index')}}?type=14&main=5">@lang('front-end.payroll')</a></li>	
                                        </ul>
                                    </li>
                                </ul>

                                <li class="drop">
                                    <a href="#">@lang('front-end.carerr')</a>
                                    <ul class="drop-menu bottom-right">
                                        <li><a @if(Route::currentRouteName()=='job_posts.index') aria-current="career" @endif href="{{ route('job_posts.index') }}">@lang('front-end.job_information')</a></li>
                                        <li><a @if(Route::currentRouteName()=='e_forms.index') aria-current="career" @endif href="{{ route('e_forms.index') }}">@lang('front-end.e_form')</a></li>
                                    </ul>
                                </li>

                                <li class="drop">
                                    <a href="#">@lang('front-end.contact_us')</a>
                                    <ul class="drop-menu bottom-right">
                                        <li><a @if(Route::currentRouteName()=='branches_networks.index') aria-current="contact" @endif href="{{ route('branches_networks.index') }}">@lang('front-end.branch_network')</a></li>
                                        <li><a @if(Route::currentRouteName()=='complaints.index') aria-current="contact" @endif href="{{ route('complaints.index') }}">@lang('front-end.complain_form')</a></li>
                                    </ul>
                                </li>  
                                <li style="margin-top: -7px;">
                                    <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                       <span class="m-r-sm text-muted welcome-message mr-2">
                                        @if(app()->getLocale() == 'en')
                                            <span>
                                                <a  href="{{ url('locale/kh') }}">
                                                    <img src="{{ asset('upload/kh.png') }}" title="Khmer">
                                                </a>
                                            </span>
                                        @elseif(app()->getLocale() == 'kh') 
                                            <span>
                                                <a  href="{{ url('locale/en') }}">
                                                    <img src="{{ asset('upload/en.png') }}" title="English">
                                                </a>
                                            </span>
                                        @endif 
                                </li>      
                            </li>  
                        </ul>
                    </div>
                </div>
                <div class="block-right hidden-desktop-large" style="margin-right:0px;margin-top:10px;">
                    <div class="contact-widget contact-widget-2 contact-widget-3">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>