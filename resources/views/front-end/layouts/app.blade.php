

<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Mohanokor | @yield('title')</title> 
    <link rel="shortcut icon" href="{{ asset('upload/'.$general->favicon) }}">
    <link href="{{ asset('upload/'.$general->favicon) }}" rel="apple-touch-icon">
    <meta name="description" content="{{ $general->meta_description }}">
    <meta name="keyword" content="{{ $general->meta_keyword }}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    
    {{-- <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/carousel/">
    <link href="{{ asset('front-end/assets/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front-end/assets/brand/carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('front-end/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/> --}}
  

    <link href="{{ asset('css/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/elegant-icons/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/owl.carousel/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/css-hamburgers/dist/hamburgers.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/plyr/plyr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/magnific-popup/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/revolution/css/layers.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/revolution/css/navigation.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/revolution/css/settings.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css/switcher.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css/colors/primary.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css/retina.css') }}" rel="stylesheet">

    <script src="{{asset('js/js/modernizr-custom.js')}}"></script>

    {{-- <script src="{{ asset('js/mnk_map/ios-orientationchange-fix.min.js')}}"></script> 
    <script src="{{ asset('js/mnk_map/jquery.rwdImageMaps.min.js')}}"></script>  --}}

	{{-- <link href="{{asset('fonts/fonts/khmer_fonts.css')}}" rel="stylesheet"> --}}
    {{-- <link href="{{asset('fonts/fonts/english_fonts.css')}}" rel="stylesheet"> --}}

	{{-- <link href="{{asset('fonts/fonts/english_fonts.css')}}" rel="stylesheet"> --}}
    {{-- <link rel="shortcut icon" href="favicon.png">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png"> --}}

    @if(app()->getLocale() == 'en')
        <link href="{{asset('fonts/fonts/english_fonts.css')}}" rel="stylesheet">
    @elseif(app()->getLocale() == 'kh') 
        <link href="{{asset('fonts/fonts/khmer_fonts.css')}}" rel="stylesheet">
    @endif 

	<style>
	body {
		font-family: contents;
		font-size: 14px;
		background:#FFF;	
		color: #000;
	}
	

	</style>
    <style>
        .image-map{
        
        width:800;
        height:678;
    }	
    .image-map a.btn:before {
        font-family: FontAwesome;
        content: "\f041";
        display: inline-block;
        vertical-align: middle;
        font-size:17pt;
    }
    .image-map a.battambang {
        left: 9%;
        top: 0%;
    }
        </style>
        <style>
    .tooltiptext {
     
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
    
      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }
        .well {
          
            display:none;
            margin:1em;
            max-width: 740px;
        }
        .well .popup_close {
            position: absolute;
            top: 0;
            right: 0px;
            border-radius: 2px;
            background: none;
            border: 0;
            font-size: 25px;
            padding: 0 10px;
        }
        pre.prettyprint {
            padding: 9px 14px;
        }
        pre {
          font-size: 12px;
        }
    
        .well--tooltip {
            min-width: 300px;
            max-width: 300px;
            margin: 0;
        }
    
        @media (min-width: 500px) {
            .well--tooltip {
                max-width: 380px;
            }
        }
    
        /* Tooltip Arrow */
        .well--tooltip::before,
        .well--tooltip::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -14px;
            width: 0;
            height: 0;
            border-left: 14px solid transparent;
            border-right: 14px solid transparent;
            border-top: 14px solid #cccccc;
        }
        .well--tooltip::after {
            border-top-color: #f5f5f5;
            margin-top:  -1px;
        }
        #fadeandscale {
            -webkit-transform: scale(0.8);
               -moz-transform: scale(0.8);
                -ms-transform: scale(0.8);
                    transform: scale(0.8);
        }
        .popup_visible #fadeandscale {
            -webkit-transform: scale(1);
               -moz-transform: scale(1);
                -ms-transform: scale(1);
                    transform: scale(1);
        }
    
        </style>

</head>

<body>
   
    <header>
        @include('front-end.layouts.menu_list')
    </header>
    
	<main>
      @yield('content')
    </main>

	<footer>
        @include('front-end.layouts.footer')
    </footer>
  
    {{-- <script src="{{ asset('js/mnk_map/jquery.min.js')}}"></script> --}}
<script src="{{ asset('js/map_tooltip/jquery.popupoverlay.js')}}"></script>
  
</body>
<script>


    
    $(document).ready(function () {
        
        $('#fadeandscale').popup({
                transition: 'all 0.3s',
                blur: false,
                escape: false,
                scrolllock: true,
                closebutton: true
            });
        });

    function area_click(Id){	
        hide_all_div();
        document.getElementById("fade"+Id).style.display="";
    }
    
    function hide_all_div(){	
        document.getElementById("fade1").style.display="none";
        document.getElementById("fade2").style.display="none";
        document.getElementById("fade3").style.display="none";
        document.getElementById("fade4").style.display="none";
        document.getElementById("fade5").style.display="none";
        document.getElementById("fade6").style.display="none";
        document.getElementById("fade7").style.display="none";
        document.getElementById("fade8").style.display="none";
        document.getElementById("fade9").style.display="none";
        document.getElementById("fade10").style.display="none";
        document.getElementById("fade11").style.display="none";
        document.getElementById("fade12").style.display="none";
        document.getElementById("fade13").style.display="none";
        document.getElementById("fade14").style.display="none";
        document.getElementById("fade15").style.display="none";
        document.getElementById("fade16").style.display="none";
        document.getElementById("fade17").style.display="none";
        document.getElementById("fade18").style.display="none";
        document.getElementById("fade19").style.display="none";
        document.getElementById("fade20").style.display="none";
        document.getElementById("fade21").style.display="none";
        document.getElementById("fade22").style.display="none";
        document.getElementById("fade23").style.display="none";
        document.getElementById("fade24").style.display="none";
        document.getElementById("fade25").style.display="none";  
    }
</script>
</html>