
	 <footer id="footer">        
        <div class="footer footer-1 bg-black">            
            <div class="top-footer p-t-50 p-b-50">                
                <div class="container">                    
                    <div class="row">                        
                        <div class="col-md-5">                            
                            <div class="footer-block-1">                               
                                <div class="title">									
                                    <h3 style="color:#FFD500;font-family:mohanokor;font-size:24px;">@lang('front-end.contact_us')</h3>								
                                </div>                                
                                <div class="quick-link">                                    
                                    <div class="row">                                        
                                        <div class="col-md-12 col-sm-12" style="color:white;">                                           
                                            <ul>                                                                                           
                                                 <li>													
                                                    <?php $head_office = \App\Models\GeneralModel::head_office();?>
                                                    <p>{{ $head_office->footer_head }}</p>
                                                    <p>{{ $head_office->footer_head_desc }}
                                                    <?php $call_us = \App\Models\GeneralModel::call_us();?>
                                                    <p>{{ $call_us->footer_call }}  @lang('front-end.number') <strong> {{ $call_us->footer_call_1 }}</strong> &nbsp; / &nbsp;
                                                    <strong> {{ $call_us->footer_call_2 }} </strong></p>
                                                    <?php $email_us = \App\Models\GeneralModel::email_us();?>
                                                    <p>{{ $email_us->footer_email }} &nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:{{ $email_us->footer_email_url }}" style="color: white;">{{ $email_us->footer_email_url }}</a></p> 
                                                    <p> 
                                                        @lang('front-end.website'): &nbsp;&nbsp;&nbsp;<a href="{{ $email_us->footer_web_url }}" target="_blank" style="color: white;">{{ $email_us->footer_web_url }}</a>
                                                    </p>                                          
                                                </li>                                                																															                                           
                                             </ul>                                        
                                            </div>                                                                           
                                        </div>                                
                                    </div>                            
                                </div>                        
                            </div>                        
                            <div class="col-md-4">                            
                                <div class="footer-block-2">                                
                                    <div class="title">                                    
                                        <h3 style="color:#FFD500;font-family:mohanokor;font-size:24px;">@lang('front-end.useful_link')</h3>                                
                                    </div>                                
                                    <div class="contact-list">                                   
                                        <ul>										
                                            <li>@lang('front-end.link')</li>                                        
                                            <a href="http://127.0.0.1:8000/historys"><li>@lang('front-end.about_us')</li></a>                                        
                                            <a href="http://127.0.0.1:8000/product_show?type=1&main=1"> <li>@lang('front-end.product_service')</li></a>                                        
                                            <a href="http://127.0.0.1:8000/job_posts"><li>@lang('front-end.carerrs')</li></a>										
                                            <a href="http://127.0.0.1:8000/branches_networks"><li>@lang('front-end.branches')</li></a>                                    
                                        </ul>                                
                                    </div>                            
                                </div>                        
                            </div>                        
                            <div class="col-md-3">                            
                                <div class="footer-block-3">                                
                                    <div class="title">                                    
                                        <h3 style="color:#FFD500;font-family:mohanokor;font-size:24px;">@lang('front-end.social_network')</h3>                                
                                    </div>                                <div class="social-list">                                    
                                        <ul class="horizontal-list">                                        
                                            <li class="social-item-2 social-item-1">                                            
                                                <a class="fa fa-facebook" href="https://www.facebook.com/Mohanokor-Microfinance-Institution-Plc-440515659464763" target="_blank"></a>                                        
                                            </li>                                        <li class="social-item-2 social-item-1">                                            
                                                <a class="fa fa-twitter" href="https://twitter.com/mohanokor_mfi?lang=en" target="_blank">
                                                </a>                                        
                                                </li>                                                                                
                                                <li class="social-item-2 social-item-1">                                            
                                                    <a class="fa fa-linkedin" href="https://www.linkedin.com/in/mohanokor-microfinance-institution-plc-a28a4b16b/" target="_blank"></a>                                        
                                                </li>										
                                                <li class="social-item-2 social-item-1">                                            
                                                    <a class="fa fa-youtube" href="https://www.youtube.com/channel/UCV7IO5opvw1ScY8TIvNvXyQ/videos" target="_blank">
                                                    </a>                                        
                                                    </li>					                                    
                                                </ul>                                
                                            </div>                            
                                        </div>                        
                                    </div>                    
                                </div>                
                            </div>            
                        </div>            
                        <div class="bot-footer">                
                            <div class="container">                    
                                <div class="block-inner p-t-35 p-b-60">                        
                                    <div class="block-left" style="color: white">                            
                                        <p >{{$head_office->footer_copyright}}</p>                        
                                    </div>                        
                                    <div class="block-right">                            
                                        <div class="quick-link" style="color: white">                                 
                                            <ul>                                    
                                                <li>                                        
                                                    <p>{{$head_office->footer_title}}</p>                                    
                                                </li>									                                                               
                                            </ul>                            
                                        </div>                        
                                    </div>                    
                                </div>                
                            </div>            
                        </div>        
                    </div>    
                </footer>	    
                <div id="up-to-top">        
                    <i class="fa fa-angle-up"></i>    
                </div>
 
<script src="{{asset('js/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('js/vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<script src="{{asset('js/vendor/owl.carousel/dist/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/vendor/headroom/headroom.min.js')}}"></script>
<script src="{{asset('js/vendor/matchHeight/dist/jquery.matchHeight-min.js')}}"></script>
<script src="{{asset('js/vendor/isotope/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('js/vendor/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('js/vendor/plyr/plyr.min.js')}}"></script>
<script src="{{asset('js/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery-accordion/js/jquery.accordion.js')}}"></script>
<script src="{{asset('js/vendor/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('js/vendor/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery.counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/vendor/paroller.js/jquery.paroller.min.js')}}"></script>
<script src="{{asset('js/vendor/retinajs/dist/retina.min.js')}}"></script>
<script src="{{asset('js/js/owl-custom.js')}}"></script>
<script src="{{asset('js/js/main.js')}}"></script>
<script src="{{asset('js/js/switcher-custom.js')}}"></script>
<script src="{{asset('js/vendor/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('js/vendor/revolution/js/jquery.themepunch.revolution.min.js')}}"></script> 

<script  src="{{asset('js/vendor/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script  src="{{asset('js/vendor/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script  src="{{asset('js/vendor/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script  src="{{asset('js/vendor/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script  src="{{asset('js/vendor/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script  src="{{asset('js/vendor/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script  src="{{asset('js/vendor/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script  src="{{asset('js/vendor/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script  src="{{asset('js/js/revo-slider-custom.js')}}"></script>

    