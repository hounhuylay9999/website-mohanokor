@extends('front-end.layouts.app')
@section('title','New Detail')
@section('content')

<section class="section p-t-70 p-b-40 bg-white">

    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
        <div class="loader"></div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 50px;">
                <h4 class="text-block text-bold text-black text-med-large m-b-20 header-title-color">{{$news->title}}</h4>
                <hr class="hr-vision" style="margin-top:-60px;">
            </div>
            
            @foreach($news_detail as $key => $value)
                <div class="col-md-4">
                    <div class="image-card image-card-4">
                        <div class="image">
                            <a @if(Route::currentRouteName()=='news_feature.index') aria-current="page" @endif href="{{ route('news_feature.index') }}?detail={{$value->id}}"><img src="{{ asset('upload/'.$value->news_detail_thumnail) }}" class="image-box"/></a>
                        </div>
                        <div class="date">
                            <a class="fa fa-calendar article_padding"></a>
                            <span>{{$value->event_date}}</span>
                        </div>
                        <h3 class="title">
                            <a @if(Route::currentRouteName()=='news_feature.index') aria-current="page" @endif href="{{ route('news_feature.index') }}?detail={{$value->id}}">
                                <?php
                                    echo substr($value->title,0,60,)."...";
                                ?>
                            </a>
                        </h3>
                        <div class="content">
                            <a @if(Route::currentRouteName()=='news_feature.index') aria-current="page" @endif href="{{ route('news_feature.index') }}?detail={{$value->id}}">
                                <?php
                                    echo substr($value->short_desc,0,70,)."...";
                                ?>
                            </a>
                        </div>
                        <div class="link">
                            <a @if(Route::currentRouteName()=='news_feature.index') aria-current="page" @endif href="{{ route('news_feature.index') }}?detail={{$value->id}}"><p class="readmore-freshnews">@lang('front-end.read_more')</p></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

@endsection

@section('scripts')
<script>
    
</script>
@endsection