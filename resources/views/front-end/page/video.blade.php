@extends('front-end.layouts.app')
@section('title','History')
@section('content')

<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
	<div class="page-loader">
		<div class="loader"></div>
	</div>
    <div class="section bg-white p-t-70 p-b-40">
        <div class="container" style="margin-top: -70px;">
            <ul class="project-tabs m-b-50 filter-tope-group" style="font-size:13pt;">
			<h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">{{$video->title}}</h3>
			<hr class="hr-vision" style="margin-top:-60px;"><br><br>
				<li class="active">
					 <span data-filter=".Events, .Songs">@lang('front-end.all')</span>
                </li>
                <li>
                    <span data-filter=".Events">@lang('front-end.event_activities')</span>
                </li>
                <li>
                    <span data-filter=".Songs">@lang('front-end.songs')</span>
                </li>
               
            </ul>
            <div class="row isotope-grid">
				@foreach($video_detail as $k => $v)
					
				   <div class="<?php echo $v->type?> col-md-4 col-sm-6 col-xs-12 isotope-item">
					   <div class="image-card image-card-8">  
							<div class="image">
								<div class="video-responsive">
									<iframe src="//www.youtube.com/embed/<?php echo $v->url?>" width="420" height="315" allowfullscreen="" frameborder="1" class="image-box"></iframe>
								</div>
							</div>  
							<div class="content">
								<h4 style="font-size:16px;color:#FFD500;line-height:25px;font-family:mohanokor;">
									{{$v->short_desc}}
								</h4>                            
							</div>	
						</div>
				   </div>
				@endforeach  
            </div>
        </div>
    </div>
   
</section>


@endsection


@section('scripts')
<script>
    
</script>
@endsection