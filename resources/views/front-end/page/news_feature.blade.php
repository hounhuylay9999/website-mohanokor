@extends('front-end.layouts.app')
@section('title','News Feature')
@section('content')

<style>
        ul.no-list-style {
    list-style-image: url('{{ asset('upload/list.gif') }}');
    margin-left: 15px;
}
</style>
<section class="section p-t-70 p-b-40 bg-white">

    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
        <div class="loader"></div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <section class="section post-section-2 p-r-40">
                    <div class="post-header">
                        <h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">@lang('front-end.news_feature')</h3>
                        <hr class="hr-vision" style="margin-top:-60px;">
                    </div>
                    <div class="post-content">    
                        @foreach($news_detail as $key => $value)
                            <p class="text-block m-b-30"></p>
                            <h4 style="font-size:21px;color:#990000;line-height:30px;">{{$value->title}}</h4>
                            <p style="line-height:25px;text-align:justify;">{{$value->short_desc}}</p>
                            <ul class="no-list-style" style="margin-left:-20px;">							
                                <li><b>@lang('front-end.date'):</b>{{$value->event_date}}</li>							
                            </ul> 
                        @endforeach                       
                        
                    </div>
                </section>
            </div>
            <div class="col-md-5">
            <div class="m-b-50" style="margin-top:65px;"></div>
            <div class="row">
            <div class="m-b-50">
                <div class="gallery-box m-b-15">
                    @foreach($news_detail as $key => $value)
                        <div class="gallery-overlay">
                            <a class="fa fa-search-plus gallery-photo" href="{{ asset('upload/'.$value->news_detail_thumnail) }}"></a>
                        </div>
                        <img src="{{ asset('upload/'.$value->news_detail_thumnail) }}" class="image-box img-responsive"/>
                    @endforeach
                </div>				                    
            </div>                  
        </div>
    </div>
</section>


@endsection


@section('scripts')
<script>
    
</script>
@endsection