@extends('front-end.layouts.app')
@section('title','Management')
@section('content')

<script src="{{ asset('js/mnk_map/ios-orientationchange-fix.min.js')}}"></script> 


<script language="JavaScript" type="text/JavaScript">
    var posx;var posy;
    function capmouse(e){
    // captures the mouse position
    posx = 0; posy = 0;
    if (!e){var e = window.event;}
    if (e.pageX || e.pageY){
    posx = e.pageX;
    posy = e.pageY;
    }
    else if (e.clientX || e.clientY){
    posx = e.clientX;
    posy = e.clientY;
    }
    }
    function showP(){
    alert('X mouse is: '+posx+' Y mouse is: '+posy)
    }
    
    </script>
<style>
 area{
    cursor: pointer;
}
.pointer{
    cursor: pointer;
}
.imgs{
    z-index: -1000;
}
</style>
<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
        <div class="loader"></div>
    </div>
    <br>
    <div class="page-content p-b-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section class="section post-section-2 p-r-40">
                        <div class="post-header">
                            <h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">{{$management->title}}<div id="demo"></div></h3><div id="coord"></div>
							<hr class="hr-vision" style="margin-top:-60px;"><br>
                        </div>
                        <div class="post-content"> 
							 <div class="col-md-12" id="wrap-para">  
							 <div align="center">                     
							  <div class="w3-image">
                                
							  
                                @if(app()->getLocale() == 'en')
                                    <img src="{{ asset('upload/'.$management->management_thumnail_en) }}"  width="800" height="678" usemap="#powerpuffgirls"  id="image">
                                @elseif(app()->getLocale() == 'kh') 
                                    <img src="{{ asset('upload/'.$management->management_thumnail_kh) }}"  width="800" height="678" usemap="#powerpuffgirls"  id="image">
                                @endif 
								<map name="powerpuffgirls">
                                    {{-- <area shape="poly" coords="160,340,260,340,260,420,160,420,160,340"> --}}
                                    <area shape="poly" coords="400,340,500,340,500,420,400,420,400,340" href="{{route('management_role.index')}}?details=2"> 
                                    <area shape="poly" coords="600,340,700,340,700,420,600,420,600,340" href="{{route('management_role.index')}}?details=3"> 
                                    <area shape="poly" coords="770,340,870,340,870,420,770,420,770,340" href="{{route('management_role.index')}}?details=4"> 
                                    <area shape="poly" coords="900,340,1000,340,1000,420,900,420,900,340" href="{{route('management_role.index')}}?details=5"> 
                                    <area shape="poly" coords="15,470,90,470,90,550,15,550,15,470" href="{{route('management_role.index')}}?details=6"> 
                                    {{-- <area shape="poly" coords="80,470,160,470,160,550,80,550,80,470"> --}}
                                    {{-- <area shape="poly" coords="150,470,230,470,230,550,150,550,150,470"> --}}
                                    {{-- <area shape="poly" coords="215,470,310,470,310,550,215,550,215,470" href="{{route('management_role.index')}}?details=9"> --}}
                                    {{-- <area shape="poly" coords="280,470,395,470,395,550,280,550,280,470" href="{{route('management_role.index')}}?details=10"> --}}
                                    <area shape="poly" coords="330,470,395,470,395,550,330,550,330,470" href="{{route('management_role.index')}}?details=10">
                                    <area shape="poly" coords="345,470,470,470,470,550,345,550,345,470" href="{{route('management_role.index')}}?details=11">
                                    <area shape="poly" coords="410,470,550,470,550,550,410,550,410,470" href="{{route('management_role.index')}}?details=12">
                                    <area shape="poly" coords="470,470,650,470,650,550,475,550,470,470" href="{{route('management_role.index')}}?details=13">
                                    <area shape="poly" coords="530,470,740,470,740,550,530,550,530,470" href="{{route('management_role.index')}}?details=14">
                                    <area shape="poly" coords="590,470,820,470,820,550,590,550,590,470" href="{{route('management_role.index')}}?details=15">
                                    {{-- <area shape="poly" coords="650,470,900,470,900,550,650,550,650,470" href="{{route('management_role.index')}}?details=16"> --}}
                                    {{-- <area shape="poly" coords="710,470,990,470,990,550,710,550,710,470"> --}}
                                    <area shape="poly" coords="1000,470,1080,470,1080,550,1000,550,1000,470" href="{{route('management_role.index')}}?details=17">
								</map>
							  </div>
							 </div><br><br><br>
							</div>
                        </div>
                    </section>
					
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('js/mnk_map/jquery.min.js')}}"></script>
<script src="{{ asset('js/mnk_map/jquery.rwdImageMaps.min.js')}}"></script> 
@endsection
 @section('script')
<script>
    $(document).ready(function(e) {
        $('img[usemap]').rwdImageMaps();
    
    });
</script>
@endsection