@extends('front-end.layouts.app')
@section('title','Home')
@section('content')
<link href="{{ asset('css/css/main.css') }}" rel="stylesheet">

        @if(app()->getLocale() == 'en')
            <link href="{{asset('fonts/fonts/english_fonts.css')}}" rel="stylesheet">
        @elseif(app()->getLocale() == 'kh') 
            <link href="{{asset('fonts/fonts/khmer_fonts.css')}}" rel="stylesheet">
        @endif 

<style>
    .size{
        width: 400px;
        height: 250px;
    }
    
  
</style>
<section>
  <!-- START REVOLUTION SLIDER 5.0-->
    <div class="page-loader">
        <div class="loader"></div>
    </div>
    <div class="slider-1 slider-2"> 
        <div class="rev_slider" id="js-slider-2" style="display:none;" >
            <ul>
                @foreach($herobanner as $k => $v)
                    <li class="item-1 dark left" data-transition="fade">
                        <img class="rev-slidebg" src="{{ asset('upload/'.$v->herobanner_thumnail) }}" alt="#" />
                        <h3 class="tp-caption tp-resizeme caption-1" data-frames="[{&quot;delay&quot;:900,&quot;speed&quot;:500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:150px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;: &quot;wait&quot;, &quot;speed&quot;: 300, &quot;to&quot;: &quot;opacity: 0&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                        data-x="['left']" data-hoffset="['15', '15', '15', '15']" data-y="['center']" data-voffset="['-50']" data-width="['520', '480', '480', '480']">&nbsp;</h3>
                        <div class="tp-caption tp-resizeme caption-2" data-frames="[{&quot;delay&quot;:1400,&quot;speed&quot;:500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:150px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;: &quot;wait&quot;, &quot;speed&quot;: 300, &quot;to&quot;: &quot;opacity: 0&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                        data-x="['left']" data-hoffset="['15', '15', '15', '15']" data-y="['center']" data-voffset="['50', '50', '50', '50']" data-width="['520', '480', '480', '480']">&nbsp;</div>
                        <div class="tp-caption tp-resizeme caption-3" data-frames="[{&quot;delay&quot;:1900,&quot;speed&quot;:500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:150px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;: &quot;wait&quot;, &quot;speed&quot;: 300, &quot;to&quot;: &quot;opacity: 0&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                        data-responsive="on" data-x="['left']" data-hoffset="['15', '15', '15', '15']" data-y="['center']" data-voffset="['150', '150', '150', '150']" data-width="['770', '770', '770', '480']">
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</section>

<!-- section / start-->
<div class="section p-t-30 p-b-30 cta-section-1 cta-section-2" style="background-color: #990000; color:white;">
    <div class="container">
        <div class="block-left">
            <p class="text-block text-med-sm">@lang('front-end.What_is_most_important_for_you')</p>
        </div>
        <div class="block-right">
            <a class="au-btn au-btn-border au-btn-border-light" href="#info">@lang('front-end.detail')</a>
        </div>
    </div>
</div>

<section class="section bg-grey-light p-t-60 p-b-50">
    <div class="container">
        <div class="relative">
            <div class="p-b-10">
              <div class="heading-section heading-section-2 dark">
                  <h3>@lang('front-end.products_and_services')</h3>
              </div>
            </div>
            <div class="owl-carousel dark nav-style-2" data-carousel-margin="7" data-carousel-nav="true" data-carousel-loop="true" data-carousel-items="4">
                @foreach($service_product as $key => $value)
                    <div class="image-card image-card-3">
                        <div class="image image-box ">
                            @if(app()->getLocale() == 'en')
                                <img src="{{ asset('upload/'.$value->product_service_thumnail_en) }}"/>
                            @elseif(app()->getLocale() == 'kh') 
                                <img src="{{ asset('upload/'.$value->product_service_thumnail_kh) }}"/>
                            @endif 
                        </div>
                        <h3 class="title">
                                <a href="{{route('product_show.index')}}?type={{$value->category_id}}&main={{$value->product_id}}">
                                    <span><b>{{$value->title}}</b></span>
                                    <i class="fa fa-chevron-right"></i>
                                </a>
                        </h3>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<section class="section p-t-70 p-b-40 bg-white">
  <div class="container">
      <div class="p-b-10">
          <div class="heading-section heading-section-2 dark">
              <h3>@lang('front-end.news_event')</h3>
          </div>
      </div>
      <div class="row">
          @foreach($news_detail as $key => $value )
            <div class="col-md-4">
                <div class="image-card image-card-6 article_wrapper">
                <div class="image image-box">
                        <a> <img src="{{ asset('upload/'.$value->news_detail_thumnail) }}" alt="#"/></a>
                    </div>
                    <h3 class="name article_padding">
                        <a @if(Route::currentRouteName()=='news_feature.index') aria-current="page" @endif href="{{ route('news_feature.index') }}?detail={{$value->id}}">
                            
                            <?php
                                echo substr($value->title,0,60,)."...";
                            ?>
                        </a>
                    </h3>
                    <ul class="socials article_padding">
                        <li>
                            <a class="fa fa-calendar article_padding"></a>
                            <span>{{$value->event_date}}</span>
                        </li>                           
                    </ul>
                    <div class="content article_padding">
                        <p>
                            <?php
                                echo substr($value->short_desc,0,70,)."...";
                            ?>
                        </p>
                    </div>
                    
                    <div class="article_padding">
                        <a @if(Route::currentRouteName()=='news_feature.index') aria-current="page" @endif href="{{ route('news_feature.index') }}?detail={{$value->id}}" class="readmore"><span>@lang('front-end.read_more')</span></a>
                    </div>
                </div>
            </div>
          @endforeach
      </div>
  </div>

<div style="text-align:center;">
<a href="http://127.0.0.1:8000/news_event" class="btn btn-warningReadmore btn-outline readall"><span>@lang('front-end.read_more')</a>
</div>

</section>
<style>
.btn-warningReadmore {
color: #fff;
background-color: #FFD500;
border-color: #990000;
}
</style>

<section class="section bg-white p-t-65 p-b-65">
    <div class="container">
        <div class="heading-section heading-section-2 dark">
            <h3>@lang('front-end.more_information')</h3>
        </div>
     
        <div class="row">
           
            @foreach($more_information as $key => $value)
                <div class="col-sm-4">
                    <img class="img-responsive m-b-15 image-box" src="{{ asset('upload/'.$value->more_information_thumnail) }}">
                </div>
            @endforeach
           
        <div class="col-sm-4">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td scope="col" colspan=3 style="text-align:center;vertical-align: middle;background-color:#f9f9f9;font-size:14px;font-weight:bold;color:#333;">@lang('front-end.exchange')</td>
                    </tr>
                </thead>
                @foreach($exchange_rate as $key => $value)

                @endforeach
                <tbody >  
                    <tr>
                        <td colspan=3 style="text-align:center;vertical-align: middle;font-weight:bold;font-size:14px; background:#FFD500;color:#FFF;">@lang('front-end.khmer_riel'):{{$value->date_post}}</td>        
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">@lang('front-end.currency')</td>
                        <td style="vertical-align: middle;">@lang('front-end.bid')</td>
                        <td style="vertical-align: middle;">@lang('front-end.ask')</td>      
                    </tr>	
                    @foreach($exchange_rate as $key => $value)
                    <tr style="font-size: 12px">
                        
                        <td>{{$value->title}}</td>
                        <td>{{$value->buy}}</td>
                        <td>{{$value->sell}}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>

<!-- OUR PARTNER / start-->
<section class="section bg-grey-light p-t-70 p-b-70">
    <div class="container">
        <div class="relative">
            <div class="p-b-15">
                <div class="heading-section heading-section-2 dark">
                    <h3>@lang('front-end.partner')</h3>
                </div>
            </div>
            <center>
                <div class="owl-carousel dark nav-style-2" data-carousel-margin="30" data-carousel-nav="true" data-carousel-loop="false" data-carousel-items="5" data-carousel-autoplay="true">
                    @foreach($partner as $k => $v )
                        <div class="icon-box icon-box-3">
                        <div class="icon">                            
                            <img src="{{ asset('upload/'.$v->partner_thumnail) }}" alt="#" />
                        </div>
                        </div>
                    @endforeach
                </div>
            </center>
        </div>
    </div>
</section>

@endsection

@section('scripts')
<script>
  
</script>
@endsection