@extends('front-end.layouts.app')
@section('title','Vision')
@section('content')

<head>
	
	<link rel="stylesheet" type="text/css" href="{{ asset('css/vision/demo.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/vision/demo2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/vision/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/vision/style2.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/vision/style2.css')}}">
	<link href="{{ asset('css/vision/css')}}" rel="stylesheet" type="text/css">
    
</head>
<style>
	.vision{
		background-color: #fbd504;
	}
</style>
    <section class="section p-t-70 p-b-40 bg-white">
    	<div class="heading-page heading-page-1 heading-page-2">
      
		</div>
		<div class="page-loader">
			<div class="loader"></div>
		</div>
		
		<div class="container vision" style="padding:0;margin:0;">
            <div class="codrops-top"></div>
			<header></header>
			<section class="se-container">
				<div class="se-slope" >
					<article class="se-content">
						<h3>{{$mission->vision_title}}</h3>
						<p>{{$mission->vision_des}}</p>
					</article>
				</div>
				<div class="se-slope">
					<article class="se-content">
						<h3>{{$mission->mission_title}}</h3><br><br>					 
						<p>{{$mission->mission_des}}</p>
					</article>
				</div>
				<div class="se-slope" style="margin-top:-275px;">
					<article class="se-content">
						<h3>{{$mission->core_title}}</h3>
						<p>{{$mission->core_des}}</p>
					</article>
				</div>				
			</section>
        </div>

		<div class="container2" style="margin-top:-70px;">           
			<section class="se-container2">
				<div class="se-slope2 se-slope-black2">
					<article class="se-content2">
						<h3 style="font-size:24pt;font-weight:bold;">{{$mission->trust_title}}</h3>
						<p>{{$mission->trust_des}}</p>
					</article>
				</div>
				<div class="se-slope2 se-slope-pink2">
					<article class="se-content2">
						<h3 style="font-size:24pt;font-weight:bold;">{{$mission->exellence_title}}</h3>
						<p>{{$mission->exellence_des}}</p>
					</article>
				</div>
				<div class="se-slope2 se-slope-pink2">
					<article class="se-content2">
						<h3 style="font-size:24pt;font-weight:bold;">{{$mission->accountability_title}}</h3>
						<p>{{$mission->accountability_des}}</p>
					</article>
				</div>
				<div class="se-slope2 se-slope-black2">
					<article class="se-content2">
						<h3 style="font-size:24pt;font-weight:bold;">{{$mission->morality_title}}</h3>
						<p>{{$mission->morality_des}}</p>
					</article>
				</div>				
			</section>
        </div>
		<div style="margin-top:-40px;"></div>
    </section>

@endsection

@section('scripts')
<script>
  
</script>
@endsection