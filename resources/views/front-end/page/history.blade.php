@extends('front-end.layouts.app')
@section('title','History')
@section('content')



<section class="section p-t-70 p-b-40 bg-white">

    <div class="heading-page heading-page-1 heading-page-2">
        
    </div>
    <hr>
        <div class="page-loader">
            <div class="loader"></div>
        </div>
    <br>

    <div class="page-content p-b-50">
        <div class="container">
            <div class="row">
			    <div class="col-md-2"></div>
                    <div class="col-md-9">
                        <section class="section post-section-1 m-b-30 p-r-15">                       
                            <div class="post-content">
                                
                            <h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">{{$history->title}}</h3>
                            <hr class="hr-vision" style="margin-top:-60px;"><br>
                            @foreach($history_detail as $key => $value)
                                <br>
                                <h3 style="color:#990000;margin-top:-5px;font-size:14pt;font-weight:bold;">{{$value->title}}</h3>
                                <p class="text-block" style="line-height:28px;text-align:justify;">
                                    {{$value->short_desc}}
                                </p>
                            @endforeach

                            <div style="text-align:center;"><br></div>
                                <div class="row">
                                    <div>
                                        <div class="gallery-box">
                                            <div class="gallery-overlay">
                                                <a class="fa fa-search-plus gallery-photo" href="{{ asset('upload/'.$history->history_thumnail) }}"></a>
                                            </div>
                                            <img src="{{ asset('upload/'.$history->history_thumnail) }}" class="img-responsive image-box"/>
                                        </div>
                                    </div>
                                </div>            
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
</section>
    

@endsection


@section('scripts')
<script>
    
</script>
@endsection