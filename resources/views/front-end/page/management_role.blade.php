@extends('front-end.layouts.app')
@section('title','Management')
@section('content')

<style>
    /* table th{
        background: #990000;
        color: white;
    } */
    
    ul.no-list-style {
    list-style-image: url('{{ asset('upload/list.gif') }}');
    margin-left: 15px;
}
</style>

<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
        <div class="loader"></div>
    </div>
    <div class="page-content p-t-80 p-b-30">
        <div class="container">
            <div class="row">
                @foreach($management_role as $key => $value)
					<div class="col-md-4" style="background-color:#f9fafb;-webkit-box-shadow: 1px 2px 5px -1px rgba(130,130,130,1);-moz-box-shadow: 1px 2px 5px -1px rgba(130,130,130,1);box-shadow: 1px 2px 5px -1px rgba(130,130,130,1);">
						<div style="text-align:center;margin-bottom:10px;"><br><br><img src="{{ asset('upload/'.$value->management_detail_thumnail) }}" class="avatar"><br><br>
						{{-- <h4 class="text-block text-bold text-med-sm m-b-15 leader-name">{{$value->title}}</h4> --}}
						<p class="text-block m-b-20 leader-name">{{$value->title}}</p>
					 </div>
					 
					<p class="text-block m-b-30 leader-description">
						{!!$value->skill!!}
                     </p>
                </div>
                <div class="col-md-8" style="background-color:#ffffff;">
                    {!!$value->short_desc!!}					
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@endsection
 @section('script')
<script>
  
</script>
@endsection