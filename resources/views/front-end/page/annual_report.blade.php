@extends('front-end.layouts.app')
@section('title','Annual Report')
@section('content')

<section class="section p-t-70 p-b-40 bg-white">

    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
        <div class="loader"></div>
    </div>
    <br>
    
    <div class="section bg-white p-t-70 p-b-40">
        <div class="container" style="margin-top: -70px;">
            <ul class="project-tabs m-b-50 filter-tope-group" style="font-size:13pt;">
			    <h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">{{$annual_report->title}}</h3>
			    <hr class="hr-vision" style="margin-top:-60px;"><br><br>
            </ul>
            <div class="row isotope-grid" style="margin-top: -50px;">
                <div class="col-md-4 col-sm-6 col-xs-12 isotope-item"  style="position: absolute; left: 0%; top: 0px;">
                    <div class="gallery-box image-card image-card-8">   
                        <div class="image photo">
                            @if(app()->getLocale() == 'en')
                                <a href="{{ asset('images/Annual_Report2021 (English) 28-12-22-news.pdf') }}" target="_blank">
                                    <img class="img-responsive image-box" src="{{ asset('upload/Annual_Report2021 (English) 28-12-22_Page_01.jpg') }}" style="width:250px;height:330px"/>
                                </a>
                            @elseif(app()->getLocale() == 'kh') 
                                <a href="{{ asset('images/Annual_Report2021 (khmer) 28-12-22-new.pdf') }}" target="_blank">
                                    <img class="img-responsive image-box" src="{{ asset('upload/Annual_Report2021 (khmer) 28-12-22_Page_01.jpg') }}" style="width:250px;height:330px"/>
                                </a>
                            @endif 
                        </div>  
                        <div class="content">
                            <h4 style="font-size:20px;font-family:mohanokor; margin-top:20px;">
                                {{-- <a href="" style="color:#FFD500;"></a> --}}
                                {{-- <a href="{{ asset('images/Annual_Report2021 (khmer) 28-12-22-new.pdf') }}" target="_blank" style="color:black;">
                                    {{$annual_report_detail->title}}
                                </a>   --}}
                                @if(app()->getLocale() == 'en')
                                    <a href="{{ asset('images/Annual_Report2021 (English) 28-12-22-news.pdf') }}" target="_blank" style="color:black;">
                                        @lang('front-end.title_ennual')
                                    </a>
                                @elseif(app()->getLocale() == 'kh') 
                                    <a href="{{ asset('images/Annual_Report2021 (khmer) 28-12-22-new.pdf') }}" target="_blank" style="color:black;">
                                        @lang('front-end.title_ennual')
                                    </a>
                                @endif 
                            </h4>                    
                        </div>						
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('scripts')
<script>
  
</script>
@endsection