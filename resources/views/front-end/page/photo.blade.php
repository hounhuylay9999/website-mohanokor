@extends('front-end.layouts.app')
@section('title','Photo')
@section('content')

<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
        <div class="loader"></div>
    </div>
    
    <div class="section bg-white p-t-70 p-b-40">
        <div class="container" style="margin-top: -70px;">
            
            <ul class="project-tabs m-b-50 filter-tope-group" style="font-size:13pt;">
			    <h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">{{$photo->title}}</h3>
			    <hr class="hr-vision" style="margin-top:-60px;"><br><br>

                <li class="active">
					 <span data-filter=".HeadOffice, .Events">@lang('front-end.all')</span>
                </li>
                <li>
                    <span data-filter=".HeadOffice">@lang('front-end.head')</span>
                </li>
                <li>
                    <span data-filter=".Events">@lang('front-end.event_activities')</span>
                </li>
            </ul>
            <div class="row isotope-grid">
                @foreach($photo_detail as $k => $v)
                    <div class="<?php echo $v->type ?> col-md-4 col-sm-6 col-xs-12 isotope-item"  style="position: absolute; left: 0%; top: 0px;">
                    
                        <div class="gallery-box image-card image-card-8">  
                            <div class="image">
                                <a class="gallery-photo" href="{{ asset('upload/'.$v->photo_detail_thumnail) }}">
                                    <img class="img-responsive image-box" src="{{ asset('upload/'.$v->photo_detail_thumnail) }}" style="width:350px;height:230px"/>
                                </a>
                            </div>  
                            <div class="content">
                                <h4 style="font-size:20px;font-family:mohanokor;">
                                    <a href="" style="color:#FFD500;"></a>
                                </h4>                            
                            </div>						
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
   
</section>

@endsection

@section('scripts')
<script>
  
</script>
@endsection