@extends('front-end.layouts.app')
@section('title','Organization Chat')
@section('content')

<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
    </div>
    <hr>
    <div class="page-loader">
        <div class="loader"></div>
    </div>
    <br>
    
    <div class="page-content p-b-50">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section class="section post-section-1 m-b-50">
                        <div class="post-content">
                            <h4 class="text-block text-bold text-black text-med-large m-b-30 header-title-color" style="">{{$organization->title}}</h4>                           
                            <hr class="hr-vision" style="margin-top:-70px;">
                        </div>
                    </section>
                </div>
            </div>
			
			<div class="row">
                <div class="col-md-12">
                    <div class="m-b-50">
                        <div class="gallery-box m-b-15">
                            <div class="gallery-overlay">
                                @if(app()->getLocale() == 'en')
                                    <a class="fa fa-search-plus gallery-photo" href="{{ asset('upload/'.$organization->organization_thumnail_en) }}"></a>
                                @elseif(app()->getLocale() == 'kh') 
                                    <a class="fa fa-search-plus gallery-photo" href="{{ asset('upload/'.$organization->organization_thumnail_kh) }}"></a>
                                @endif 
                            </div>
                            @if(app()->getLocale() == 'en')
                                <img src="{{ asset('upload/'.$organization->organization_thumnail_en) }}" class="img-responsive" />
                            @elseif(app()->getLocale() == 'kh') 
                                <img src="{{ asset('upload/'.$organization->organization_thumnail_kh) }}" class="img-responsive" />
                            @endif 
                        </div>
                    </div>
                </div>               
            </div>
        </div>		
    </div>
</section>

@endsection

@section('scripts')
<script>
  
</script>
@endsection