@extends('front-end.layouts.app')
@section('title','Branches Network')
@section('content')

<script src="{{ asset('js/mnk_map/ios-orientationchange-fix.min.js')}}"></script> 
<script src="{{ asset('js/mnk_map/jquery.rwdImageMaps.min.js')}}"></script> 


<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
		<div class="loader"></div>
	</div>
    <div class="page-content p-b-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section class="section post-section-2 p-r-40">
                        <div class="post-header">
                            <h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">{{$branches_network_title->title}}</h3>
							<hr class="hr-vision" style="margin-top:-60px;"><br>
                        </div>
                        <div class="post-content">
                        
						
							 <div class="col-md-12" id="wrap-para">          
							 <div align="center">                     
							  <div class="w3-image">
                                @if(app()->getLocale() == 'en')
                                    <img src="images/branch_map_en.png"  usemap="#powerpuffgirls"  id="image-map">
                                @elseif(app()->getLocale() == 'kh') 
                                    <img src="images/branch_map.png"  usemap="#powerpuffgirls"  id="image-map">
                                @endif 
							  
							  <a class="btn battambang" data-html="true" data-content="<b>សាខា</b><br>បាត់ដំបង<p style='color:red;'>មហានគរ</p><hr>sdfs" data-placement="top" data-toggle="popover" data-trigger="focus"tabindex="0" ></a>
                              <map name="powerpuffgirls">
								<area shape="poly" coords="441,501,503,496,489,475,478,443,441,445,440,474" class="fadeandscale_open" onclick="area_click('14')" style="cursor:pointer;"><!--Prey Veng-->
								<area shape="poly" coords="60,190,171,189,153,165,130,164,129,143,125,139,103,137,91,140,87,154,87,165,68,170" class="fadeandscale_open" onclick="area_click('1')" style="cursor:pointer;"><!--Banteay Meanchey-->
								<area shape="poly" coords="98,289,163,295,158,256,147,256,145,227,107,227,106,250,98,261" class="fadeandscale_open" onclick="area_click('2')" style="cursor:pointer;"><!--Battambang-->
								<area shape="poly" coords="409,407,491,408,475,353,430,357,432,382,404,386" class="fadeandscale_open" onclick="area_click('3')" style="cursor:pointer;"><!-- Kampong Cham-->
								<area shape="poly" coords="299,409,377,407,364,377,358,362,353,345,316,349,308,372" class="fadeandscale_open" onclick="area_click('4')" style="cursor:pointer;"><!-- Kampong Chhnang -->
								<area shape="poly" coords="281,511,341,516,339,485,326,479,327,455,288,458,287,486" class="fadeandscale_open" onclick="area_click('5')" style="cursor:pointer;"><!-- Kampong Speu-->
								<area shape="poly" coords="350,260,462,260,462,350,350,350" class="fadeandscale_open" onclick="area_click('6')" style="cursor:pointer;"><!--Kampong Thom-->
								<area shape="poly" coords="256,606,310,592,305,570,312,547,262,546,267,576" class="fadeandscale_open" onclick="area_click('7')" style="cursor:pointer;"><!--Kampot-->
								<area shape="poly" coords="400,500,428,500,428,550,400,550" class="fadeandscale_open" onclick="area_click('8')" style="cursor:pointer;"><!--Kandal-->
								<area shape="poly" coords="137,516,203,519,204,486,191,459,160,459,147,476" class="fadeandscale_open" onclick="area_click('9')" style="cursor:pointer;"><!--Koh Kong-->
								<area shape="poly" coords="286,659,350,660,342,600,297,611" class="fadeandscale_open" onclick="area_click('23')" style="cursor:pointer;"><!--Kep-->
								<area shape="poly" coords="520,310,625,310,613,292,613,266,571,267,520,287" class="fadeandscale_open" onclick="area_click('10')" style="cursor:pointer;"><!-- Kratie-->							
								<area shape="poly" coords="690,320,748,317,751,287,742,284,737,261,707,263,702,286" class="fadeandscale_open" onclick="area_click('11')" style="cursor:pointer;"><!-- Mondulkiri-->	
								<area shape="poly" coords="139,108,147,115,148,119,189,126,234,121,224,103,205,102,214,84,203,75,167,77,164,97,139,103" class="fadeandscale_open" onclick="area_click('22')" style="cursor:pointer;">	<!-- Oddar Meanchey-->							
								<area shape="poly" coords="30,234,73,238,73,264,91,265,74,305,30,299,31,279,29,265,29,235" class="fadeandscale_open" onclick="area_click('24')" style="cursor:pointer;"><!-- Pailin-->
								<area shape="poly" coords="344,440,428,440,428,490,344,490" class="fadeandscale_open" onclick="area_click('12')" style="cursor:pointer;"><!-- Phnom Penh-->
								<area shape="poly" coords="179,642,240,649,258,610,238,607,234,579,198,581,198,603,167,610" class="fadeandscale_open" onclick="area_click('18')" style="cursor:pointer;"><!--Preah Sihanouk-->
								<area shape="poly" coords="340,100,450,100,450,170,450,340,170" class="fadeandscale_open" onclick="area_click('13')" style="cursor:pointer;"><!-- prash vihea-->
								<area shape="poly" coords="202,375,278,377,260,346,258,316,221,317,222,340,207,348,203,361" class="fadeandscale_open" onclick="area_click('15')" style="cursor:pointer;">	<!-- Pursat-->							
								<area shape="poly" coords="680,160,750,160,735,143,733,122,700,121,680,149" class="fadeandscale_open" onclick="area_click('16')" style="cursor:pointer;"><!--Ratanakiri-->
								<area shape="poly" coords="213,206,288,207,279,185,265,178,269,162,268,151,237,153,228,157,232,175,223,184" class="fadeandscale_open" onclick="area_click('17')" style="cursor:pointer;">	<!-- Siem Reap-->								
								<area shape="poly" coords="561,177,606,179,628,173,623,153,616,141,612,121,584,120,579,145" class="fadeandscale_open" onclick="area_click('19')" style="cursor:pointer;"> <!-- Stung Treng-->
								<area shape="poly" coords="489,512,531,510,540,532,564,553,563,572,484,570,497,533" class="fadeandscale_open" onclick="area_click('20')" style="cursor:pointer;">  <!-- Svay Rieng-->
								<area shape="poly" coords="359,610,411,616,408,585,403,556,364,557,366,580,362,600" class="fadeandscale_open" onclick="area_click('21')" style="cursor:pointer;">  <!-- Takeo-->
								<area shape="poly" coords="487,441,554,440,547,409,542,381,500,382,500,408" class="fadeandscale_open" onclick="area_click('25')" style="cursor:pointer;">			<!-- Tboung Khmum-->					
								</map>
							  </div>
							 </div>
							</div>
                        </div>
                    </section>
                    <div id="fadeandscale" class="well">
					
                        @foreach($province as $key => $value)
                            <div id="fade{{$value->id}}" style="display:none;" class="col-sm-12" style="padding-bottom:2px;">
                            
                                @foreach($branches_network as $key => $v)
                                
                                    @if($v->province_id==$value->id)
                                        <h4 style="color:#FFD500;font-family:contents;">{{$v->title}}</h4>
                                        <p style="font-family:contents;line-height:20px;">&nbsp;@lang('front-end.addresses'):&nbsp;{{$v->address}}</p>
                                        <p style="font-family:contents;line-height:20px;">&nbsp;@lang('front-end.phone') &nbsp;&nbsp;:&nbsp;(+855)&nbsp;{{$v->phone}}<br>
                                        <br>&nbsp;&nbsp;@lang('front-end.e_mail')&nbsp;&nbsp;:&nbsp;{{$v->e_mail}}</p>	
                                        <hr class="hr_style">
                                    @endif
                                
                                @endforeach
                            </div>
                        @endforeach

                    </div>
					
                </div>
            </div>
        </div>
    </div>

</section>
<script src="{{ asset('js/mnk_map/jquery.min.js')}}"></script>
@endsection
 @section('script')
<script>
    $(document).ready(function(e) {
        $('img[usemap]').rwdImageMaps();
    
    });
</script>
@endsection

    
    





