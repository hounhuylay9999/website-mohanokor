@extends('front-end.layouts.app')
@section('title','Complaint Form')
@section('content')
<head>
 
	<link rel="stylesheet" href="{{ asset('css/sweetalert/sweetalert.css')}}">
	<link rel="stylesheet" href="{{ asset('css/css/ApplicationForm/style.css')}}">
	<link rel="stylesheet" href="{{ asset('css/css/ApplicationForm/style_customer_complaint.css')}}">
	<script src="{{ asset('js/sweetalert/sweetalert.js')}}"></script>
    
</head>

<style>
    textarea {
        color: #000;
        width: 96%;
        padding: 6px 2%;
        margin: 0;
        resize: none;
        -webkit-appearance: none;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    textarea {
        border-color: rgb(169, 169, 169);
    }

    input[type=submit], input[type=button] {
        cursor: pointer;
        padding: 0 20px;
        background-color: #990000;
        height: 36px;
        color: #fff;
        border: none;
        border-radius: 3px;
        margin-top: 20px;
        font-weight:bold;
        -webkit-appearance: none;
    }

    ul.no-list-style {
        list-style-image: url('{{ asset('upload/list.gif') }}');
        margin-left: 15px;
    }
</style>

<script>
    
    function preventuploads(){
        var myfile=document.getElementById("userfile").value;
        var ext = myfile.split('.').pop();
            //var extension = myfile.substr( (myfile.lastIndexOf('.') +1) );
            if(ext=="pdf" || ext=="png" || ext=="jpg"){
                swal({title: "",text: "ឯកសារបានបញ្ជូនរួចហើយ",type: "warning"});
            }
            else{
                swal({title: "",text: "Please Upload: PDF, JPG, PNG!",type: "warning"});			
                document.getElementById("userfile").value="";
                return false;
            }	
        }
            
        //only khmer or english language
        function checklanguage(field,language){
                var sNewVal = "";
                var sFieldVal = field.value;
                for(var i = 0; i < sFieldVal.length; i++) {
                    var ch = sFieldVal.charAt(i);
                    var c = ch.charCodeAt(0);
            
            //English
            if(language=="English"){
            if(c < 0 || c > 255) {
                field.value="";
                swal({title: "",text: "Please Type English!",type: "warning"});
                //msg("Please Type English ");
                //msg(coffee);
                return false;}
            else {
                        sNewVal += ch;}   
        
            //Khmer 
        
            }else{
            if(c < 0 || c > 255 || c==32) {
            sNewVal += ch;}
            else {
                field.value="";
                swal({title: "",text: "Please Type Khmer",type: "warning"});    
                return false;  } } }  
        }
  </script>

<section class="section p-t-70 p-b-40 bg-white">
    <div class="heading-page heading-page-1 heading-page-2">
      
    </div>
    <hr>
    <div class="page-loader">
        <div class="loader"></div>
    </div>

    <div class="page-content p-b-70">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <section class="section post-section-2 p-r-40">
                        <div class="post-header">
                            <h3 class="text-block text-black text-bold text-med-large m-b-25 header-title-color">{{$complaint_form->title}}</h3>
                            <hr class="hr-vision" style="margin-top:-60px;"><br>
                        </div>
                        <div class="post-content">
                            <p class="text-block m-b-30 no-list-style" style="text-align:justify;">
                                {{-- <ul class="no-list-style"> --}}
                                   
                                        {!! $complaint_form->full_text !!}    
                                   
                                {{-- </ul> --}}
                                <ul class="no-list-style">
                                        <li><strong><a href="{{ asset('images/CustomerComplaintForm.pdf') }}" target="_blank">@lang('front-end.customer_complaint_forms')</a></strong></li>
                                </ul>
                                
                                     
                            </p> 
                        </div>
                    </section>
                    <br>
                <div class="col-md-12 col-sm-12" style="padding:0px;">					
                    <p style="font-size:15px; height:40px; margin-bottom:5px; padding:10px; font-weight:bold; border-radius: 5px;;color:#fff;/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffba34+0,ffe01c+100 */
                        background: #990000; /* Old browsers */
                        background: -moz-linear-gradient(top,  #990000 0%, #990000 100%); /* FF3.6-15 */
                        background: -webkit-linear-gradient(top,  #990000 0%,#990000 100%); /* Chrome10-25,Safari5.1-6 */
                        background: linear-gradient(to bottom,  #990000 0%,#990000 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#990000', endColorstr='#990000',GradientType=0 ); /* IE6-9 */">@lang('front-end.customer_complaint_form')</p>
                <div class="main_container">
                {{-- <form name="captchaform" class="register-form" id="captchaform" action="customer_complaint.php" method="post" enctype="multipart/form-data" route='complaint_form_detail.store'>							 --}}
                    {!! Form::open(array('route' => 'complaint_form_detail.store','method'=>'POST','class'=>'register-form','enctype'=>'multipart/form-data', 'onSubmit'=>"return preventuploads()")) !!}    
                    <table cellpadding="0" cellspacing="0" class="info" id="info">
                        <tr>
                            <th colspan="2" style="text-align:left;"></th>
                        </tr>
                        <tr>
                            <td colspan="2" style="background-color:#fff; padding-top:10px; padding-bottom:10px; color:red;"></td>
                        </tr>
                        <tr>
                            <td class="td_width" style="height:50px;">
                                <div class="form-group" style="margin-left:2px;">
                                    <label for="txtname"><i class="fa fa-user"></i></label>
                                    <input type="text" name="full_name" maxlength="150"  placeholder="Full Name" autocomplete="off" required autofocus />
                                </div>
                            </td><td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="td_width" style="height:50px;">
                                <div class="form-group" style="margin-left:2px;">
                                    <label for="txtaddress"><i class="fa fa-address-card"></i></label>
                                    <input type="text" name="address" id="txtaddress" maxlength="400"  placeholder="Address" autocomplete="off" required autofocus />
                                </div>
                            </td><td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="td_width" style="height:50px;">
                                <div class="form-group" style="margin-left:2px;">
                                    <label for="txttel"><i class="fa fa-phone"></i></label>
                                    <input type="text" name="phone" id="txttel" maxlength="50"  placeholder="Phone" autocomplete="off" required autofocus/>
                                </div>
                            </td><td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="td_width" style="height:50px;">
                                <div class="form-group" style="margin-left:2px;">
                                    <label for="txtemail"><i class="fa fa-envelope"></i></label>
                                    <input type="text" name="e_mail" id="txtemail" maxlength="150" value="" placeholder="E-mail" autocomplete="off" required autofocus/>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>			                     
                        <tr>
                            <td colspan="2" style="padding-top:10px; padding-bottom:20px;"><br /><strong>@lang('front-end.please_describe_on_the_problem')</strong><span style="color:red;font-weight:bold;font-size:17px;">*</span>
                                <br />
                                <textarea rows="5" name="txt1"  id="txtproblem" maxlength="1500" autocomplete="off"  required autofocus></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-top:10px; padding-bottom:20px;"><br /><strong>@lang('front-end.expected_result')</strong>
                                <br />
                                <textarea rows="5" name="txt2"  id="txtExpectedResult" maxlength="1500" autocomplete="off"  required autofocus></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-top:20px; padding-bottom:20px;">
                                <p class="text-block m-b-5"><b>@lang('front-end.document'):  <span style="font-family:Arials">(Image, PDF, Max: 5MB)</span></b></p>
                                <div class="form-group">
                                    <input  name="file" id="userfile" type="file" accept="application/pdf, image/*"><h4></h4>
                                </div>
                            </td>
                        </tr>
                        <tr style="margin-top: 20px;">
                            <td>
                                <div class="form-group form-button">
                                    <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1" style="background:#990000;">@lang('front-end.submit')</button>
                                </div>
                            </td>
                        </tr>
                        
                    </table>
                {!! Form::close() !!}
               
                    <ul style="margin-top: 30px;">
                        
                        {!! $complaint_form->sort_text !!} 
                    </ul>
                    
                    <br>    
                    <br>
                </div>
            </div>
        </div>
            <div class="col-md-3">
                <div class="m-b-50"><br></div>
                    <div class="row">
                        <div class="m-b-50 hidden-p">
                            <div class="gallery-box m-b-15">
                                <div class="gallery-overlay">
                                    <a class="fa fa-search-plus gallery-photo" href="{{ asset('upload/'.$complaint_form->complaint_form_thumnail) }}"></a>
                                </div>
                                <img class="img-responsive image-box" src="{{ asset('upload/'.$complaint_form->complaint_form_thumnail) }}">
                            </div>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
</section>
    

@endsection

