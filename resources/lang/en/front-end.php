<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'vision_mission'                    => 'Vision Mission & Core Value',
    'organization_chat'                 => 'Organizational Chat',
    'management'                        => 'Management Team',
    'photo'                             => 'Photo',
    'video'                             => 'Video',
    'news_event'                        => 'News & event',
    'home'                              => 'Home',
    'history'                           => 'History',
    'about_us'                          => 'About Us',
    'address'                           => 'Address',
    'product_service'                   => 'Products and Services',
    'credit'                            => 'Credit',
    'deposit'                           => 'Deposits',
    'business_loan'                     => 'Business Loan',
    'group_loan'                        => 'Group Loan',
    'housing_loan'                      => 'Housing Loan',
    'home_improvement_loan'             => 'Home Improvement Loan',
    'motorbike_loan'                    => 'Motorbike Loan',
    'car_loan'                          => 'Car Loan',
    'agriculture_loan'                  => 'Agriculture Loan',
    'fixed_deposit'                     => 'Fixed Deposit', 
    'general_saving'                    => 'General Saving', 
    'future_account'                    => 'Future Account', 
    'atm_card'                          => 'ATM Card', 
    'atm'                               => 'ATM', 
    'mohanokor_mobile'                  => 'MOHANOKOR Mobile',
    'mobile'                            => 'MOHANOKOR Mobile',
    'payroll'                           => 'Payroll',
    'payroll_service'                   => 'Payroll Service',
    'carerr'                            => 'Careers',
    'job_information'                   => 'Job Information',
    'e_form'                            => 'E-Form',
    'contact_us'                        => 'Contact Us',
    'branch_network'                    => 'Branches Network',
    'complain_form'                     => 'Complaint Form',
    'What_is_most_important_for_you'    => 'What is most important for you?',
    'history_detail'                    => 'History Detail',
    'detail'                            => 'Detail',
    'products_and_services'             => 'Products And Services',
    'news_event'                        => 'Featured News',
    'read_more'                         => 'Read More',
    'more_information'                  => 'More Information',
    'currency'                          => 'CURRENCY',
    'bid'                               => 'BID',
    'ask'                               => 'ASK',
    'exchange'                          => 'Exchange Rate',
    'khmer_riel'                        => 'Khmer Riel',
    'partner'                           => 'PARTNERS',
    'contact_us'                        => 'Contact Us',
    'website'                           => 'Website',
    'branches'                          => 'Branches',
    'useful_link'                       => 'Useful Links',
    'carerrs'                           => 'Job Announment',
    'link'                              => 'This may be the very useful and quick link for proper information',
    'social_network'                    => 'Social Network',
    'all'                               => 'All',
    'head'                              => 'Head Office',
    'event_activities'                  => 'Event Activities',
    'songs'                             => 'Songs',
    'position'                          => 'Position',
    'location'                          => 'Location',
    'close_date'                        => 'Close Date',
    'details'                           => 'Detail',
    'download'                          => 'Download',
    'click'                             => 'Please click the link below to download',
    'button'                            => 'application form',
    'application_form'                  => 'Application Form',
    'position_apply'                    => 'Position Applied',
    'required'                          => 'Required',
    'please_select_position'            => '__Please Select Position__',
    'document'                          => 'Document',
    'submit'                            => 'Submit',
    'customer_complaint_form'           => 'Customer Complaint Form',
    'expected_result'                   => '2. Expected result',
    'please_describe_on_the_problem'    => '1. Please Describe On The Problem',
    'news_feature'                      => 'News Feature',
    'date'                              => 'Date',
    'home'                              => 'Home',
    'number'                            => '(+855)',
    'annual_report'                     => 'Annual Report',
    'customer_complaint_forms'          => 'Download Customer Complaint Form',
    'title_ennual'                      => 'Annual Report 2021',
    'contact_information'               => 'Contact Information',
    'addresses'                         => 'Address',
    'phone'                             => 'Phone',
    'e_mail'                            => 'E-Mail',



















];