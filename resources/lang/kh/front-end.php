<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    
    'about'                             => 'អំពីយើង',
    'contact'                           => 'ទាក់ទង',
    'history'                           => 'ប្រវត្តិ',
    'about_us'                          => 'អំពីយើង',
    'product_service'                   => 'ផលិតផល និង សេវាកម្ម',
    'carerr'                            => 'ឱកាសការងារ',
    'carerrs'                           => 'ឱកាសការងារ',
    'contact_us'                        => 'ទំនាក់ទំនង',
    'vision_mission'                    => 'ទស្សនវិស័យ បេសកកម្ម និងតម្លៃរួម',
    'organization_chat'                 => 'រចនាសម្ព័ន្ធ',
    'management'                        => 'គណៈគ្រប់គ្រង',
    'photo'                             => 'រូបភាព',
    'news_event'                        => 'ព្រឹត្តិការណ៍ថ្មីៗ',
    'video'                             => 'វីដេអូ',
    'credit'                            => 'សេវាឥណទាន',
    'business_loan'                     => 'ឥណទានអាជីវកម្ម',
    'group_loan'                        => 'ឥណទានក្រុម',
    'agriculture_loan'                  => 'ឥណទានកសិកម្ម',
    'car_loan'                          => 'ឥណទានរថយន្ត',
    'motorbike_loan'                    => 'ឥណទានម៉ូតូ',
    'home_improvement_loan'             => 'ឥណទានកែលម្អគេហដ្ជាន',
    'housing_loan'                      => 'ឥណទានគេហដ្ជាន',
    'deposit'                           => 'សេវាបញ្ញើសន្សំ',
    'general_saving'                    => 'គណនីសន្សំទូទៅ',
    'fixed_deposit'                     => 'គណនីបញ្ញើមានកាលកំណត់',
    'future_account'                    => 'គណនីសន្សំថ្ងៃអនាគត',
    'atm'                               => 'សេវាអេធីអឹម',
    'atm_card'                          => 'បណ្ណអេធីអឹម',
    'atm'                               => 'ម៉ាសុីនអេធីអឹម',
    'mohanokor_mobile'                  => 'សេវាមហានគរ ម៉ូបាល',
    'mobile'                            => 'ម៉ូបាល',
    'payroll'                           => 'សេវាបើកប្រាក់បៀវត្ស',
    'job_information'                   => 'ព័ត៍មានអំពីការងារ',
    'e_form'                            => 'ដាក់ពាក្យអេឡិចត្រូនិក',
    'branch_network'                    => 'បណ្តាញប្រតិបត្តិការ',
    'complain_form'                     => 'ទម្រង់បណ្តឹងត្អូញត្អែរ',    
    'detail'                            => 'ព័ត៌មានលម្អិត',
    'What_is_most_important_for_you'    => 'តើ​សេវា​កម្ម​មួយ​ណា​ដែល​ជា​ជម្រើស​របស់​លោក​អ្នក!',
    'products_and_services'             => 'ផលិតផល និង សេវាកម្ម',
    'read_more'                         => 'អានបន្ត',
    'more_information'                  => 'ព័ត៌​មាន​បន្ថែម​',
    'currency'                          => 'រូបិយប័ណ្ណ',
    'bid'                               => 'ទិញ',
    'ask'                               => 'លក់',
    'exchange'                          => 'អត្រាប្តូរប្រាក់',
    'khmer_riel'                        => 'គិតជាប្រាក់រៀល',
    'partner'                           => 'ដៃគូសហការ',
    'contact_us'                        => 'អាសយដ្ឋាន',
    'website'                           => 'គេហទំព័រ',
    'branches'                          => 'សាខា',
    'useful_link'                       => 'តំណភ្ជាប់មានប្រយោជន៍',
    'link'                              => 'នេះគីជាតំណភ្ជាប់ដែលមានភាពងាយស្រួល និងឆាប់រហ័ស',
    'social_network'                    => 'បណ្តាញសង្គម',
    'all'                               => 'ទាំងអស់',
    'head'                              => 'ការិយាល័យ',
    'event_activities'                  => 'ព្រឹត្តិការណ៍',
    'songs'                             => 'ចម្រៀង',
    'position'                          => 'មុខដំណែង',
    'location'                          => 'ទីកន្លែងការងារ',
    'close_date'                        => 'ថ្ងៃផុតកំណត់',
    'details'                           => 'ទាញយក',
    'download'                          => 'ទាញយក',
    'click'                             => 'សូមចុចលើប៊ូតុងខាងក្រោមដើម្បី',
    'button'                            => 'ទាញយកពាក្យសុំបម្រើការងារ',
    'application_form'                  => 'ពាក្យសុំបម្រើការងារ',
    'position_apply'                    => 'មុខតំណែងស្នើសុំ',
    'required'                          => 'តម្រូវអោយមាន',
    'please_select_position'            => 'សូមជ្រើសរើសមុខតំណែង',
    'document'                          => 'ឯកសារភ្ជាប់',
    'submit'                            => 'បញ្ជូនទិន្នន័យ',
    'customer_complaint_form'           => 'ទម្រង់បណ្តឹងត្អូញត្អែររបស់អតិថិជន',
    'expected_result'                   => '២. លទ្ធផលរំពឹងទុក',
    'please_describe_on_the_problem'    => '១. សូម​ពិពណ៌នា​ពី​បញ្ហា​ដែល​បាន​ជួប​ប្រទះ',
    'news_feature'                      => 'ព័ត៍មានលំអិត',
    'date'                              => 'កាលបរិច្ជេទ',
    'home'                              => 'ទំព័រដើម',
    'number'                            => '(+៨៥៥)',
    'annual_report'                     => 'របាយការណ៏ប្រចាំឆ្នាំ',
    'customer_complaint_forms'          => 'ទាញយកទម្រង់​បណ្តឹង​ត្អូញ​ត្អែរ',
    'title_ennual'                      => 'របាយការណ៏ប្រចាំឆ្នាំ ២០២១',
    'contact_information'               => 'ទំនាក់ទំនងព័ត៌មាន',
    'addresses'                         => 'អាសយដ្ឋាន',
    'phone'                             => 'ទូរស័ព្ទ',
    'e_mail'                            => 'អុីម៉ែល',









    






];